/*
 ============================================================================
 Name        : examples.cpp
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Starts a client (and a server during selfcheck) and connects to
               a server. Checks whether all examples were instanciated with their
               profile specific childs.
 ============================================================================
 */
#include <test_ProfileServer.h>
/* Custom main function is needed to interprete the comand line args */
int
main(int argc, char **argv) {
    /* Parse the arguments and set up environment */
    ::testing::AddGlobalTestEnvironment(new ComplianceEnv(argc, argv));

    /* Instanciate tests */
    // Clear nodes set from commandline
    ::ComplianceEnv::nodes.clear();
    ::ComplianceEnv::profiles.clear();

    // Define nodes to test (analog to createControlComponentsForAllProfiles)
    for(UA_UInt16 componentNo = 1; componentNo <= C3_PROFILES_COUNT; componentNo++) {
        addNodeToTests(C3_PROFILES[componentNo - 1],
                       UA_NODEID_NUMERIC(NS_APPLICATION, componentNo));
    }
    // TODO check example types in NS_DSTYPES

    /* Run tests */
    registerComplianceTests();
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
/*
 ============================================================================
 Name        : interfaces.cpp
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Starts a client (and a server with internalServer) and connects
               to a server. Checks whether all control component interfaces
               were instanciated with their childs.
 ============================================================================
 */
#include <test_ProfileServer.h>
/* Custom main function is needed to interprete the comand line args */
int
main(int argc, char **argv) {
    /* Parse the arguments and set up environment */
    ::testing::AddGlobalTestEnvironment(new ComplianceEnv(argc, argv));
    // Clear nodes set from commandline
    ::ComplianceEnv::nodes.clear();
    ::ComplianceEnv::profiles.clear();
    // Define nodes to test
    C3_Profile profile = C3_PROFILE_UNKNOWN;
    profile.optional = true;
    // A value greater than max value is ANY but none specific.
    profile.SI = (C3_SI_FACET)(C3_SI_FACET_MAXVALUE + 1);
    addNodeToTests(
        profile, UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCINTERFACETYPES +
                                                    NS2FACTOR_PROFILES * C3_SI_UNKNOWN));
    profile.SI = C3_SI_CMD;
    addNodeToTests(profile,
                   UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCINTERFACETYPES +
                                                      NS2FACTOR_PROFILES * C3_SI_CMD));
    profile.SI = C3_SI_OPERATIONS;
    addNodeToTests(profile, UA_NODEID_NUMERIC(NS_PROFILES,
                                              NS2OFFSET_CCINTERFACETYPES +
                                                  NS2FACTOR_PROFILES * C3_SI_OPERATIONS));
#ifdef CCPUA_ENABLE_PACKML
    profile.SI = C3_SI_PACKML;
    addNodeToTests(profile,
                   UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCINTERFACETYPES +
                                                      NS2FACTOR_PROFILES * C3_SI_PACKML));
#endif
    // Overwrite test filter
    std::ostringstream filter;
    filter << "-*Behaviour*:*" << NS2FACTOR_PROFILES * C3_SI_CMD << "/*.ANY*:*"
           << NS2FACTOR_PROFILES * C3_SI_OPERATIONS << "/*.ANY*:*"
#ifdef CCPUA_ENABLE_PACKML
           << NS2FACTOR_PROFILES * C3_SI_PACKML << "/*.ANY*"
#endif
           ;
    ::testing::GTEST_FLAG(filter) = filter.str();

    /* Run tests */
    registerComplianceTests();
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
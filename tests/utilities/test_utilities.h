/*
 ============================================================================
 Name        : test_utilities.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Common includes and definitions for tests
 ============================================================================
 */
#ifndef TEST_UTILITIES_H
#define TEST_UTILITIES_H

/* open62541 Includes */
extern "C" {
#ifdef UA_ENABLE_AMALGAMATION
#include <open62541.h>  //Not tested yet
#else
#include <open62541/client.h>
#include <open62541/client_config_default.h>
#include <open62541/client_highlevel.h>
#endif /* open62541 Includes */
#ifdef CCPUA_ENABLE_PACKML
#include <profile_packML.h>
#endif
#include <profile_utilities.h>

#include <C3_Profiles.h>
}
#include <gtest/gtest.h>

std::string
nodeIdToString(const UA_NodeId *nodeId);

std::ostream &
operator<<(std::ostream &os, const UA_NodeId nodeId);

std::ostream &
operator<<(std::ostream &os, const UA_String string);

void
nodeIdFromString(std::string nodeIdString, std::vector<UA_NodeId> *nodes);

/* Additional Assert and Expect checks */
#define ASSERT_STATUS(CODE)                                                              \
    ASSERT_EQ(CODE, UA_STATUSCODE_GOOD) << "Bad statuscode: " << UA_StatusCode_name(CODE)
#define EXPECT_STATUS(CODE)                                                              \
    EXPECT_EQ(CODE, UA_STATUSCODE_GOOD) << "Bad statuscode: " << UA_StatusCode_name(CODE)
// TODO use AssertionResult or ASSERT_PRED(...) to print IDS instead?
#define ASSERT_EQ_NODEID(ID1, ID2)                                                       \
    ASSERT_TRUE(UA_NodeId_equal(ID1, ID2))                                               \
        << " ID1: " << nodeIdToString(ID1) << " ID2: " << nodeIdToString(ID2)
#define EXPECT_EQ_NODEID(ID1, ID2)                                                       \
    EXPECT_TRUE(UA_NodeId_equal(ID1, ID2))                                               \
        << " ID1: " << nodeIdToString(ID1) << " ID2: " << nodeIdToString(ID2)

#define ASSERT_EQ_ARRAY(array1, array1Size, array2, array2Size)                          \
    ASSERT_EQ(array1Size, array2Size);                                                   \
    for(size_t i; i < (size_t)array1Size; i++) {                                         \
        ASSERT_EQ(array1[i], array2[i]) << ". Value " << i << "not equal.";              \
    }
#define EXPECT_EQ_ARRAY(array1, array1Size, array2, array2Size)                          \
    EXPECT_EQ(array1Size, array2Size);                                                   \
    if(array1Size == array2Size) {                                                       \
        for(size_t i; i < (size_t)array1Size; i++) {                                     \
            EXPECT_EQ(array1[i], array2[i]) << ". Value " << i << "not equal.";          \
        }                                                                                \
    }

/*
 * Simple browsing functions
 * Containing fatal failure asserts --> need to return void.
 * Use SCOPED_TRACE("Testname"); in TEST{ } environments to get proper error line
 * information or use DOC_... macros from test_ProfileServer
 */
// TODO change to functions of ProfileServerTest class --> client, nsIdx can be left out
void
translatePath(UA_Client *client, UA_UInt16 nsIdx, UA_NodeId startingId,
              const char *path[], size_t pathSize, UA_NodeId *outNodeId);
void
translatePath(UA_Client *client, UA_UInt16 nsIdx, UA_NodeId startingId, const char *name,
              UA_NodeId *outNodeId);
void
hasChildOfType(UA_Client *client, UA_NodeId startingId, UA_NodeId typeId,
               UA_NodeId *outNodeId);
void
hasReferenceToNode(UA_Client *client, UA_NodeId startingId, UA_NodeId referenceTypeId,
                   UA_NodeId targetId);

/*
 * Implemented checks in checkVariableAttributes:
 * UA_NodeId dataType;
 * UA_Byte accessLevel; default = UA_ACCESSLEVEL_READ
 * UA_Int32 valueRank; default = UA_VALUERANK_SCALAR
 * size_t arrayDimensionsSize; default = 0
 * UA_UInt32 *arrayDimensions; default = NULL
 *
 * Not implemented checks:
 * UA_Double minimumSamplingInterval;
 * UA_Boolean historizing;
 * UA_ValueSource valueSource;
 *  union {
 *      struct {
 *          UA_DataValue value;
 *          UA_ValueCallback callback;
 *      } data;
 *      UA_DataSource dataSource;
 * } value;
 */
void
checkVariableAttributes(UA_Client *client, UA_NodeId nodeId, UA_UInt32 dataTypeId);
void
checkVariableAttributes(UA_Client *client, UA_NodeId nodeId, UA_UInt32 dataTypeId,
                        UA_Byte accessLevel);
void
checkVariableAttributes(UA_Client *client, UA_NodeId nodeId, UA_UInt32 dataTypeId,
                        UA_Byte accessLevel, UA_Int32 valueRank);
void
checkVariableAttributes(UA_Client *client, UA_NodeId nodeId, UA_UInt32 dataTypeId,
                        UA_Byte accessLevel, UA_Int32 valueRank,
                        UA_UInt32 *arrayDimensions, size_t arrayDimensionsSize);

/* Checks on variable values */
void
checkValueString(UA_Client *client, UA_NodeId nodeId, const char *value);
void
checkValueInStringArray(UA_Client *client, UA_NodeId nodeId, const char *value);

/* Writing of variable values */
void
writeString(UA_Client *client, UA_NodeId nodeId, const char *value);

/* Checks on method attributes
 * Implemented checks in checkMethodAttributes
 *    UA_Boolean executable;
 *
 * Not implemented checks:
 *    UA_LocalizedText displayName;
 *    UA_LocalizedText description;
 *    UA_UInt32 writeMask;
 *    UA_UInt32 userWriteMask;
 *    UA_Boolean userExecutable;
 */
void
checkMethodAttributes(UA_Client *client, UA_NodeId nodeId,
                      UA_MethodAttributes expectation);
void
checkMethodExecutable(UA_Client *client, UA_NodeId nodeId);

/* Invokation of methods */
void
callMethod(UA_Client *client, UA_NodeId methodId, UA_NodeId parentId);
void
callMethod(UA_Client *client, UA_NodeId methodId, UA_NodeId parentId,
           UA_StatusCode expectedResult);

/* CC functions */
const char *
getUsername();
std::string
concatCMD(const char *order);

void
tryToCallOrder(UA_Client *client, UA_UInt16 nsIdx, UA_NodeId ccId, const char *order,
               C3_SI_FACET si, const char *targetState, const char *targetStateValue);

#endif /* TEST_UTILITIES_H */
#include <test_complianceEnv.h>  // for ComplianceEnv::server
#include <test_utilities.h>

#include <regex>

#include <C3_Occupation.h>  // for occupation of components

std::string
nodeIdToString(const UA_NodeId *nodeId) {
    UA_String nodeIdStringUa = UA_STRING_NULL;
    if(UA_NodeId_print(nodeId, &nodeIdStringUa) == UA_STATUSCODE_GOOD) {
        std::string nodeIdstr =
            std::string((char *)nodeIdStringUa.data, nodeIdStringUa.length);
        UA_String_clear(&nodeIdStringUa);
        return nodeIdstr;
    }
    return NULL;
}

// Pretty printer for node ids
std::ostream &
operator<<(std::ostream &os, const UA_NodeId nodeId) {
    return os << nodeIdToString(&nodeId);
}
// Pretty printer for UA_String
std::ostream &
operator<<(std::ostream &os, const UA_String string) {
    os.write((const char *)string.data, string.length);
    return os;
}

/*
 * Parse a node id from its string represenation. Inverse of UA_NodeId_print or
 * nodeItToString respectively. Returns UA_NODEID_NULL in case of an error. NodeIdString
 * needs to have syntax: [ns=index;][i|s|g|b]=id
 * //TODO: Use new UA_NodeId_parse() function from open62541
 */
const static std::regex
    nodeIdRegex("(?:ns=(\\d+);)?(?:(?:i\\=(\\d+))|(?:([sgb])=\"(.*?)\"))");

void
parseNodeIdFromRegex(std::smatch nodeIdRegexMatch, UA_NodeId *id) {
    // parse namespace index
    if(nodeIdRegexMatch.length(1) > 0)
        id->namespaceIndex = (UA_UInt16)stoul(nodeIdRegexMatch[1]);
    if(nodeIdRegexMatch.length(2) > 0) {
        // parse numeric index
        id->identifierType = UA_NODEIDTYPE_NUMERIC;
        id->identifier.numeric = (UA_UInt32)stoul(nodeIdRegexMatch[2]);
    } else if(nodeIdRegexMatch.str(3)[0] == 's') {
        // parse string index
        id->identifierType = UA_NODEIDTYPE_STRING;
        id->identifier.string = UA_String_fromChars(nodeIdRegexMatch.str(4).c_str());
    }
}

UA_NodeId
nodeIdFromString(std::string nodeIdString) {
    std::smatch nodeIdRegexMatch;
    if(std::regex_search(nodeIdString, nodeIdRegexMatch, nodeIdRegex)) {
        UA_NodeId id = UA_NODEID_NULL;
        parseNodeIdFromRegex(nodeIdRegexMatch, &id);
        return id;
    } else
        return UA_NODEID_NULL;
}
void
nodeIdFromString(std::string nodeIdString, std::vector<UA_NodeId> *nodes) {
    // Count and allocate space for node ids
    auto idsBegin =
        std::sregex_iterator(nodeIdString.begin(), nodeIdString.end(), nodeIdRegex);
    auto idsEnd = std::sregex_iterator();
    size_t count = (size_t)std::distance(idsBegin, idsEnd);
    if(count == 0)
        return;
    // parse every nodeId which was found by the regex
    for(std::sregex_iterator i = idsBegin; i != idsEnd; ++i) {
        UA_NodeId id = UA_NODEID_NULL;
        parseNodeIdFromRegex(*i, &id);
        if(!UA_NodeId_isNull(&id)) {
            nodes->push_back(id);
        }
    }
}

// TODO rewrite utility functions, so that memory is freed on exceptions (ASSERT / EXPECT)
/* The server or client pointer may be NULL. If the server is given it is used for faster
 * testing. */
void
translatePath(UA_Client *client, UA_UInt16 nsIdx, UA_NodeId startingId,
              const char *path[], size_t pathSize, UA_NodeId *outNodeId) {
    UA_Server *server = ComplianceEnv::server;
    ASSERT_TRUE(server != NULL || client != NULL);
    /* Setup browsepath */
    UA_BrowsePath bp;
    UA_BrowsePath_init(&bp);
    UA_NodeId_copy(&startingId, &bp.startingNode);
    bp.relativePath.elements = (UA_RelativePathElement *)UA_Array_new(
        pathSize, &UA_TYPES[UA_TYPES_RELATIVEPATHELEMENT]);
    bp.relativePath.elementsSize = pathSize;
    for(size_t i = 0; i < pathSize; i++) {
        UA_RelativePathElement *elem = &bp.relativePath.elements[i];
        // profilerequirement: Only a hierarchical reference is needed
        elem->referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_HIERARCHICALREFERENCES);
        elem->targetName = UA_QUALIFIEDNAME_ALLOC(nsIdx, path[i]);
        elem->includeSubtypes =
            UA_TRUE;  // This allows HAS_CHILD, HAS_PROPERTY, ORGANIZES, ...
    }
    UA_BrowsePathResult bpr;
    if(server != NULL) {
        /* Setup and send request with server*/
        bpr = UA_Server_translateBrowsePathToNodeIds(server, &bp);
    } else {
        /* Setup and send request with client*/
        UA_TranslateBrowsePathsToNodeIdsRequest request;
        UA_TranslateBrowsePathsToNodeIdsRequest_init(&request);
        request.browsePaths = &bp;
        request.browsePathsSize = 1;
        UA_TranslateBrowsePathsToNodeIdsResponse response =
            UA_Client_Service_translateBrowsePathsToNodeIds(client, request);
        ASSERT_STATUS(response.responseHeader.serviceResult);
        ASSERT_EQ(response.resultsSize, 1);
        /* Copy result and free ressources */
        UA_BrowsePathResult_copy(&response.results[0], &bpr);
        UA_TranslateBrowsePathsToNodeIdsResponse_clear(&response);
    }
    UA_BrowsePath_clear(&bp);

    /* Evaluate result */
    ASSERT_STATUS(bpr.statusCode) << ". Didn't find browsepath to last child: "
                                  << path[pathSize - 1];  // TODO print whole path
    ASSERT_EQ(bpr.targetsSize, 1);
    if(outNodeId) {
        UA_NodeId_copy(&bpr.targets->targetId.nodeId, outNodeId);
    }
    UA_BrowsePathResult_clear(&bpr);
}

/* The server or client pointer may be NULL. If the server is given it is used for faster
 * testing. */
static void
hasReference(UA_Client *client, UA_NodeId startingId, UA_NodeId referenceTypeId,
             UA_BrowseResult *outBrowseResult) {
    UA_Server *server = ComplianceEnv::server;
    ASSERT_TRUE(server != NULL || client != NULL);
    /* Setup browserequest */
    UA_BrowseDescription bd;
    UA_BrowseDescription_init(&bd);
    UA_NodeId_copy(&startingId, &bd.nodeId);
    UA_NodeId_copy(&referenceTypeId, &bd.referenceTypeId);
    bd.includeSubtypes = UA_TRUE;
    bd.browseDirection = UA_BROWSEDIRECTION_FORWARD;
    bd.resultMask = UA_BROWSERESULTMASK_ALL;
    if(server != NULL) {
        /* Setup and send request with server*/
        *outBrowseResult = UA_Server_browse(server, 0, &bd);
    } else {
        /* Setup and send request with client*/
        UA_BrowseRequest request;
        UA_BrowseRequest_init(&request);
        request.nodesToBrowse = &bd;
        request.nodesToBrowseSize = 1;
        UA_BrowseResponse response = UA_Client_Service_browse(client, request);
        ASSERT_STATUS(response.responseHeader.serviceResult);
        ASSERT_EQ(response.resultsSize, 1);
        /* Copy result and free ressources */
        UA_BrowseResult_copy(&response.results[0], outBrowseResult);
        UA_BrowseResponse_clear(&response);
    }

    /* Evaluate result */
    ASSERT_STATUS(outBrowseResult->statusCode)
        << ". Error browsing for " << referenceTypeId << " from " << startingId;
    ASSERT_GT(outBrowseResult->referencesSize, 0)
        << " No reference of type " << referenceTypeId << " from " << startingId;
}

/* The server or client pointer may be NULL. If the server is given it is used for faster
 * testing. */
void
hasChildOfType(UA_Client *client, UA_NodeId startingId, UA_NodeId typeId,
               UA_NodeId *outNodeId) {
    UA_BrowseResult br;
    UA_BrowseResult_init(&br);
    hasReference(client, startingId,
                 UA_NODEID_NUMERIC(0, UA_NS0ID_HIERARCHICALREFERENCES), &br);
    /* Check for Type */
    for(int i = 0; i < br.referencesSize; ++i) {
        if(UA_NodeId_equal(&br.references[i].typeDefinition.nodeId, &typeId)) {
            if(outNodeId) {
                UA_NodeId_copy(&br.references[i].nodeId.nodeId, outNodeId);
            }
            UA_BrowseResult_clear(&br);
            return;
        }
    }
    UA_BrowseResult_clear(&br);
    FAIL() << "Didn't find child of type: " << typeId << " under " << startingId;
}
/* The server or client pointer may be NULL. If the server is given it is used for faster
 * testing. */
void
hasReferenceToNode(UA_Client *client, UA_NodeId startingId, UA_NodeId referenceTypeId,
                   UA_NodeId targetId) {
    UA_BrowseResult br;
    UA_BrowseResult_init(&br);
    hasReference(client, startingId, referenceTypeId, &br);
    /* Check for Target node */
    for(int i = 0; i < br.referencesSize; ++i) {
        if(UA_NodeId_equal(&br.references[i].nodeId.nodeId, &targetId)) {
            UA_BrowseResult_clear(&br);
            return;
        }
    }
    UA_BrowseResult_clear(&br);
    FAIL() << " No reference of type " << referenceTypeId << " from " << startingId
           << " to " << targetId;
}

void
translatePath(UA_Client *client, UA_UInt16 nsIdx, UA_NodeId startingId, const char *name,
              UA_NodeId *outNodeId) {
    const char *path[1] = {name};
    translatePath(client, nsIdx, startingId, path, 1, outNodeId);
}

static void
checkNodeClass(UA_Server *server, UA_Client *client, UA_NodeId nodeId,
               UA_NodeClass expectation) {
    UA_NodeClass nodeClass;
    UA_StatusCode retval =
        (server == NULL) ? UA_Client_readNodeClassAttribute(client, nodeId, &nodeClass)
                         : UA_Server_readNodeClass(server, nodeId, &nodeClass);
    ASSERT_STATUS(retval) << ". Could't read nodeclass attribute for node: " << nodeId;
    EXPECT_EQ(nodeClass, expectation);
}

void
checkVariableAttributes(UA_Client *client, UA_NodeId nodeId, UA_UInt32 dataTypeId) {
    checkVariableAttributes(client, nodeId, dataTypeId, UA_ACCESSLEVELMASK_READ);
}
void
checkVariableAttributes(UA_Client *client, UA_NodeId nodeId, UA_UInt32 dataTypeId,
                        UA_Byte accessLevel) {
    checkVariableAttributes(client, nodeId, dataTypeId, accessLevel, UA_VALUERANK_SCALAR);
}
void
checkVariableAttributes(UA_Client *client, UA_NodeId nodeId, UA_UInt32 dataTypeId,
                        UA_Byte accessLevel, UA_Int32 valueRank) {
    checkVariableAttributes(client, nodeId, dataTypeId, UA_ACCESSLEVELMASK_READ,
                            valueRank, NULL, 0);
}
void
checkVariableAttributes(UA_Client *client, UA_NodeId nodeId, UA_UInt32 dataTypeId,
                        UA_Byte accessLevel, UA_Int32 valueRank,
                        UA_UInt32 *arrayDimensions, size_t arrayDimensionsSize) {
    UA_Server *server = ComplianceEnv::server;
    ASSERT_TRUE(server != NULL || client != NULL);
    // Check node class of variable
    checkNodeClass(server, client, nodeId, UA_NODECLASS_VARIABLE);

    // Check data type attribute
    // TODO Only checks against exact datatype (no subtypes checked)
    UA_NodeId dataTypeIdServer;
    UA_NodeId dataTypeIdTarget = UA_NODEID_NUMERIC(0, dataTypeId);
    UA_StatusCode retval =
        (server == NULL)
            ? UA_Client_readDataTypeAttribute(client, nodeId, &dataTypeIdServer)
            : UA_Server_readDataType(server, nodeId, &dataTypeIdServer);
    ASSERT_STATUS(retval) << ". Could't read datatype attribute for node: " << nodeId;
    EXPECT_EQ_NODEID(&dataTypeIdServer, &dataTypeIdTarget)
        << ". Datatype id not equal for node: " << nodeId;
    UA_NodeId_clear(&dataTypeIdServer);

    // Check access level
    UA_Byte accessLevelServer;
    retval = (server == NULL)
                 ? UA_Client_readAccessLevelAttribute(client, nodeId, &accessLevelServer)
                 : UA_Server_readAccessLevel(server, nodeId, &accessLevelServer);
    ASSERT_STATUS(retval) << ". Could't read acesslevel attribute for node: " << nodeId;
    EXPECT_EQ(accessLevel, (accessLevelServer & accessLevel));

    // Check value rank attribute
    UA_Int32 valueRankServer;
    retval = (server == NULL)
                 ? UA_Client_readValueRankAttribute(client, nodeId, &valueRankServer)
                 : UA_Server_readValueRank(server, nodeId, &valueRankServer);
    ASSERT_STATUS(retval) << ". Could't read valuerank attribute for node: " << nodeId;
    EXPECT_TRUE(valueRank == valueRankServer || valueRankServer == UA_VALUERANK_ANY);

    // Check array dimensions
    UA_UInt32 *arrayDimensionsServer = NULL;
    size_t arrayDimensionsSizeServer = 0;
    if(true) {  //(server == NULL) //FIXME server implementation yields to error (always
                // reads arrayDimensionsSize 0)
        retval = UA_Client_readArrayDimensionsAttribute(
            client, nodeId, &arrayDimensionsSizeServer, &arrayDimensionsServer);
    } else {
        UA_Variant arrayDimensionsVariantServer;
        UA_Variant_init(&arrayDimensionsVariantServer);
        retval =
            UA_Server_readArrayDimensions(server, nodeId, &arrayDimensionsVariantServer);
        arrayDimensionsServer = arrayDimensionsVariantServer.arrayDimensions;
        arrayDimensionsSizeServer = arrayDimensionsVariantServer.arrayDimensionsSize;
    }
    ASSERT_STATUS(retval) << ". Could't read array dimensions attribute for node: "
                          << nodeId;
    EXPECT_EQ(arrayDimensionsSize, arrayDimensionsSizeServer);
    if(arrayDimensions != NULL) {
        EXPECT_EQ_ARRAY(arrayDimensions, arrayDimensionsSize, arrayDimensionsServer,
                        arrayDimensionsSizeServer);
    }
    if(arrayDimensionsServer)
        UA_Array_delete(arrayDimensionsServer, arrayDimensionsSizeServer,
                        &UA_TYPES[UA_TYPES_UINT32]);
}

void
checkValueString(UA_Client *client, UA_NodeId nodeId, const char *value) {
    UA_Server *server = ComplianceEnv::server;
    ASSERT_TRUE(server != NULL || client != NULL);
    UA_Variant valueServer;
    UA_Variant_init(&valueServer);
    UA_StatusCode retval =
        (server == NULL) ? UA_Client_readValueAttribute(client, nodeId, &valueServer)
                         : UA_Server_readValue(server, nodeId, &valueServer);
    ASSERT_STATUS(retval) << ". Could't read value attribute for node: " << nodeId;
    EXPECT_EQ(valueServer.type, &UA_TYPES[UA_TYPES_STRING]);
    EXPECT_TRUE(UA_Variant_isScalar(&valueServer));
    std::string valueServerString;
    valueServerString.append((char *)((UA_String *)valueServer.data)->data,
                             ((UA_String *)valueServer.data)->length);
    EXPECT_STREQ(value, valueServerString.c_str());
    UA_Variant_clear(&valueServer);
}

void
checkValueInStringArray(UA_Client *client, UA_NodeId nodeId, const char *value) {
    UA_Server *server = ComplianceEnv::server;
    ASSERT_TRUE(server != NULL || client != NULL);
    // read value from server
    UA_Variant valueServer;
    UA_Variant_init(&valueServer);
    UA_StatusCode retval =
        (server == NULL) ? UA_Client_readValueAttribute(client, nodeId, &valueServer)
                         : UA_Server_readValue(server, nodeId, &valueServer);
    ASSERT_STATUS(retval) << ". Could't read value attribute for node: " << nodeId;
    EXPECT_EQ(valueServer.type, &UA_TYPES[UA_TYPES_STRING]);
    EXPECT_FALSE(UA_Variant_isScalar(&valueServer));
    EXPECT_LT(valueServer.arrayDimensionsSize, 2);
    EXPECT_GT(valueServer.arrayLength, 0);

    // Check value in array
    UA_String stringUa = UA_STRING((char *)value);
    for(size_t i = 0; i < valueServer.arrayLength; i++) {
        if(UA_String_equal(&((UA_String *)valueServer.data)[i], &stringUa)) {
            UA_Variant_clear(&valueServer);
            return;
        }
    }
    UA_Variant_clear(&valueServer);
    FAIL() << "String '" << value << "' not found in string array of node:" << nodeId;
}

void
writeString(UA_Client *client, UA_NodeId nodeId, const char *value) {
    UA_Server *server = ComplianceEnv::server;
    UA_String valueUa = UA_STRING((char *)value);
    UA_Variant valueVariant;
    UA_Variant_setScalar(&valueVariant, &valueUa, &UA_TYPES[UA_TYPES_STRING]);
    UA_StatusCode retval =
        (server == NULL) ? UA_Client_writeValueAttribute(client, nodeId, &valueVariant)
                         : UA_Server_writeValue(server, nodeId, valueVariant);
    EXPECT_STATUS(retval) << ". Couldn't write string to nodeId: " << nodeId;
}

void
checkMethodAttributes(UA_Client *client, UA_NodeId nodeId,
                      UA_MethodAttributes expectation) {
    // TODO add other attribute checks and add same function syntax for variables
    UA_Server *server = ComplianceEnv::server;
    ASSERT_TRUE(server != NULL || client != NULL);
    UA_StatusCode result = UA_STATUSCODE_GOOD;
    // Check node class of variable
    checkNodeClass(server, client, nodeId, UA_NODECLASS_METHOD);

    // Check executable
    if(expectation.specifiedAttributes & UA_NODEATTRIBUTESMASK_EXECUTABLE) {
        UA_Boolean executable;
        result = (server != NULL)
                     ? UA_Server_readExecutable(server, nodeId, &executable)
                     : UA_Client_readExecutableAttribute(client, nodeId, &executable);
        ASSERT_STATUS(result) << ". Couldn't read executable attribute of " << nodeId;
        EXPECT_EQ(expectation.executable, executable);
    }
}

void
checkMethodExecutable(UA_Client *client, UA_NodeId nodeId) {
    UA_MethodAttributes attr;
    attr.specifiedAttributes = UA_NODEATTRIBUTESMASK_EXECUTABLE;
    attr.executable = UA_TRUE;
    checkMethodAttributes(client, nodeId, attr);
}

void
callMethod(UA_Client *client, UA_NodeId methodId, UA_NodeId parentId,
           UA_StatusCode expectedResult) {
    UA_Server *server = ComplianceEnv::server;
    ASSERT_TRUE(server != NULL || client != NULL);
    UA_StatusCode result = UA_STATUSCODE_GOOD;

    /* Don't allow serverside call to use correct client authentification */
    // if(server != NULL) {
    //     UA_CallMethodRequest request;
    //     UA_CallMethodRequest_init(&request);
    //     request.methodId = methodId;
    //     request.objectId = parentId;
    //     UA_CallMethodResult response = UA_Server_call(server, &request);
    //     result = response.statusCode;
    // } else {
    result = UA_Client_call(client, parentId, methodId, 0, NULL, 0, NULL);
    // }
    if(result != expectedResult && result != UA_STATUSCODE_GOOD)
        ADD_FAILURE() << "Bad statuscode: " << UA_StatusCode_name(result)
                      << ". Couldn't call method (" << methodId
                      << ") for object: " << parentId;
}

void
callMethod(UA_Client *client, UA_NodeId methodId, UA_NodeId parentId) {
    callMethod(client, methodId, parentId, UA_STATUSCODE_GOOD);
}

static UA_NodeId
translateBrowsenameToNodeId(UA_Client *client, UA_NodeId nodeId, const char *browsename,
                            UA_UInt16 nsIdx) {
    UA_NodeId result = UA_NODEID_NULL;
    /* Setup browsepath */
    UA_BrowsePath bp;
    UA_BrowsePath_init(&bp);
    UA_NodeId_copy(&nodeId, &bp.startingNode);
    UA_RelativePathElement bpElem;
    UA_RelativePathElement_init(&bpElem);
    bpElem.referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_HIERARCHICALREFERENCES);
    bpElem.targetName = UA_QUALIFIEDNAME(nsIdx, (char *)browsename);
    bpElem.includeSubtypes =
        UA_TRUE;  // This allows HAS_CHILD, HAS_PROPERTY, ORGANIZES, ...
    bp.relativePath.elements = &bpElem;
    bp.relativePath.elementsSize = 1;

    /* Setup and send request with client*/
    UA_TranslateBrowsePathsToNodeIdsRequest request;
    UA_TranslateBrowsePathsToNodeIdsRequest_init(&request);
    request.browsePaths = &bp;
    request.browsePathsSize = 1;
    UA_TranslateBrowsePathsToNodeIdsResponse response =
        UA_Client_Service_translateBrowsePathsToNodeIds(client, request);
    if(response.responseHeader.serviceResult == UA_STATUSCODE_GOOD &&
       response.resultsSize == 1 &&
       response.results[0].statusCode == UA_STATUSCODE_GOOD &&
       response.results[0].targetsSize == 1) {
        UA_NodeId_copy(&response.results[0].targets->targetId.nodeId, &result);
    }
    UA_TranslateBrowsePathsToNodeIdsResponse_clear(&response);
    return result;
}

const char *
getUsername() {
    return (ComplianceEnv::logins.size() > 0)
               ? std::get<0>(ComplianceEnv::logins[0]).c_str()
               : "Anonymous";
}

std::string
concatCMD(const char *order) {
    return std::string(getUsername()) + ";" + std::string(order) + ";";
}

void
tryToCallOrder(UA_Client *client, UA_UInt16 nsIdx, UA_NodeId ccId, const char *order,
               C3_SI_FACET si, const char *targetState, const char *targetStateValue) {
    // Check if state already ok
    UA_NodeId statusId = translateBrowsenameToNodeId(client, ccId, "STATUS", nsIdx);
    UA_NodeId stateId = translateBrowsenameToNodeId(client, statusId, targetState, nsIdx);

    UA_Variant valueServer;
    UA_Variant_init(&valueServer);
    UA_StatusCode retval = UA_Client_readValueAttribute(client, stateId, &valueServer);
    UA_String targetStateValueUa = UA_STRING((char *)targetStateValue);
    if(retval == UA_STATUSCODE_GOOD && valueServer.type == &UA_TYPES[UA_TYPES_STRING] &&
       UA_Variant_isScalar(&valueServer) &&
       UA_String_equal((UA_String *)valueServer.data, &targetStateValueUa)) {
        UA_Variant_clear(&valueServer);
        return;
    }
    UA_Variant_clear(&valueServer);

    /* Do SI:CMD specific occupation */
    if(si & (C3_SI_CMD)) {
        /* Browse to cmd id */
        UA_NodeId cmdId = translateBrowsenameToNodeId(client, ccId, "CMD", nsIdx);
        /* Try to occupy component via CMD interface */

        UA_String cmd = UA_STRING_ALLOC(concatCMD(order).c_str());
        UA_Variant valueVariant;
        UA_Variant_setScalar(&valueVariant, &cmd, &UA_TYPES[UA_TYPES_STRING]);
        UA_Client_writeValueAttribute(client, cmdId, &valueVariant);
        sleep(ComplianceEnv::behaviourTimeout);
        UA_String_clear(&cmd);
        UA_NodeId_clear(&cmdId);
    }
    /* Do SI:OPERATIONS specific occupation */
    else if(si & (C3_SI_OPERATIONS)) {
        /* Browse to OCCUPY method */
        UA_NodeId operationsId =
            translateBrowsenameToNodeId(client, ccId, "OPERATIONS", nsIdx);
        UA_NodeId methodId =
            translateBrowsenameToNodeId(client, operationsId, order, nsIdx);
        /* Try to occupy component via OPERATIONS interface */
        UA_Client_call(client, operationsId, methodId, 0, NULL, 0, NULL);
        sleep(ComplianceEnv::behaviourTimeout);
    }
}
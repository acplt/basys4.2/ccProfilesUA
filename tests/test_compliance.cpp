/*
 ============================================================================
 Name        : compliance.cpp
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Starts a client (and a server during selfcheck) and connects to
               a server. Instanciates all possible tests, which may be filtered
               by --gtest_filter= parameter of google test framework.
               Registration of tests is controlled by cmdline parameters.
 ============================================================================
 */
#include <test_ProfileServer.h>
/* Custom main function is needed to interprete the comand line args */
int
main(int argc, char **argv) {
    /* Parse the arguments and set up environment */
    ::testing::AddGlobalTestEnvironment(new ComplianceEnv(argc, argv));
    /* Instanciate tests */
    registerComplianceTests();
    /* Run tests */
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
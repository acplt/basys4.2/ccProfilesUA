extern "C" {
#include <profile_examples.h>

#include <controlComponent/cc_instanciation.h>
#include <controlComponent/cc_connector.h>
}
#include <test_complianceEnv.h>

/* Parse arguments from a main function to setup server url and node id variables*/
static void
usage(void) {
    // TODO add possibility to read information from config
    std::cout
        << "Usage: testname "                        // TODO get filename as param
        << "[--server opc.tcp://<host>:<port>] "     // set external / internal server url
        << "[--user <username1> <password> [...]] "  // authentification for the client(s)
        << "--nodes [ns=<index>;]i|s|g|b=<id>[, ...] "  // get node ids to check
        << "[--namespace <URIofBaSysNamespace>]"        // namespace for translate node
                                                        // ids
        << "[--profile <profile>] "                     // test for profile
        << "[--selfcheck] "                             // use internal server
        << "[--timeout time]"  // timeout for behaviour tests in seconds
        << "[--gtest ...]"     // used in ::testing::InitGoogleTest(&argc, argv) starting
                               // with --gtest;
        << std::endl;
}

static void
usage(std::string errorMsg) {
    std::cerr << "Error: " << errorMsg << std::endl;
    usage();
}

/* Static variables of compliance test environment */
int ComplianceEnv::setupResult = -1;
std::vector<UA_NodeId> ComplianceEnv::nodes;
const char *ComplianceEnv::nsUriProfiles = NS_PROFILES_URI;
std::vector<C3_Profile> ComplianceEnv::profiles;
std::string ComplianceEnv::serverUrl = std::string("opc.tcp://localhost:4840");
std::vector<std::tuple<std::string, std::string>> ComplianceEnv::logins;
bool ComplianceEnv::internalServer = false;
UA_Server *ComplianceEnv::server = NULL;
void *ComplianceEnv::cc_executionLoopContext;
UA_UInt64 cc_executionLoopId = 0;
UA_Boolean ComplianceEnv::running;
THREAD_HANDLE ComplianceEnv::serverThread;
unsigned int ComplianceEnv::behaviourTimeout = 0;

#ifndef WIN32
void *
ComplianceEnv::serverloop(void *)
#else
DWORD WINAPI
ComplianceEnv::serverloop(LPVOID lpParam)
#endif
{
    while(running)
        UA_Server_run_iterate(server, true);
    return 0;
}

int
ComplianceEnv::parseMainArgs(int argc, char **argv) {
    /* Parse the arguments and set up environment */
    // TODO use regex or CLI library instead: http://tclap.sf.net/ or
    // http://optionparser.sourceforge.net/index.html
    C3_Profile profile = C3_PROFILE_UNKNOWN;
    for(int argpos = 1; argpos < argc; argpos++) {
        if(strcmp(argv[argpos], "--help") == 0 || strcmp(argv[argpos], "-h") == 0) {
            usage();
            return EXIT_SUCCESS;
        }
        // ignore google test parameters
        if(strncmp(argv[argpos], "--gtest", 7) == 0) {
            continue;
        }
        // parse server url
        if(strcmp(argv[argpos], "--server") == 0) {
            argpos++;
            if(argpos >= argc) {
                usage("parameter --server given, but no url specified!");
                return EXIT_FAILURE;
            }
            serverUrl = std::string(argv[argpos]);
            continue;
        }
        // parse username and password
        if(strcmp(argv[argpos], "--user") == 0) {
            if(argpos + 2 >= argc) {
                usage("parameter --user given, but no username and password specified!");
                return EXIT_FAILURE;
            }
            logins.push_back({argv[argpos + 1], argv[argpos + 2]});
            argpos += 2;
            continue;
        }
        // check whether internal server (profile_server code) should be used
        if(strcmp(argv[argpos], "--selfcheck") == 0) {
            internalServer = true;
            continue;
        }
        // check which nodes should be tested
        if(strcmp(argv[argpos], "--nodes") == 0) {
            argpos++;
            if(argpos >= argc) {
                usage("parameter --nodes given, but no nodes specified!");
                return EXIT_FAILURE;
            }
            nodeIdFromString(std::string(argv[argpos]), &nodes);
            if(nodes.size() == 0) {
                usage(std::string("could not parse any node id out of: ") + argv[argpos]);
                return EXIT_FAILURE;
            }
            continue;
        }
        // also allow parameter nodes=
        if(strncmp(argv[argpos], "--nodes=", 8) == 0) {
            if(strlen(argv[argpos]) <= 8) {
                usage("parameter --nodes= given, but no nodes specified!");
                return EXIT_FAILURE;
            }
            nodeIdFromString(std::string(&argv[argpos][8]), &nodes);
            if(nodes.size() == 0) {
                usage(std::string("could not parse any node id out of: ") +
                      &argv[argpos][8]);
                return EXIT_FAILURE;
            }
            continue;
        }
        // parse namespace uri
        if(strcmp(argv[argpos], "--namespace") == 0) {
            argpos++;
            if(argpos >= argc) {
                usage("parameter --namespace given, but no URI specified!");
                return EXIT_FAILURE;
            }
            nsUriProfiles = argv[argpos];
            continue;
        }
        // check Profile value
        if(strcmp(argv[argpos], "--profile") == 0) {
            argpos++;
            if(argpos >= argc) {
                usage("parameter --profile given, but no value specified!");
                return EXIT_FAILURE;
            }
            profile = C3_Profile_parse(argv[argpos]);
            if(!C3_Profile_isEqual(profile, C3_PROFILE_NULL)) {
                continue;
            }
            // check for special values --> readSIValue from nodes
            // if(strcmp(argv[argpos], "readSIValue")) {
            //     // TODO Set new static variable "readSIPROFILE" --> try to read
            //     C3_Profile/SI
            //     // value for tests
            //     continue;
            // }
            usage(
                std::string("parameter --profile given, but no valid profile specified! "
                            "Available profiles are: BASYSDEMO, BASYS, PACKML, FULL, "
                            "FULLOPTIONAL. Or "
                            "custom profiles in the format 'SI,OC,EM,ES,OM,optional', "
                            "e.g. '4,6,6,2,2,0' for BASYS profile. Given value: ") +
                argv[argpos]);
            return EXIT_FAILURE;
        }
        // parse behaviour timeout
        if(strcmp(argv[argpos], "--timeout") == 0) {
            argpos++;
            if(argpos >= argc) {
                usage("parameter --timeout given, but no time specified!");
                return EXIT_FAILURE;
            }
            behaviourTimeout = strtoul(argv[argpos], NULL, 10);
            continue;
        }
        // Unknown option
        usage(std::string("unknown command line option: ") + argv[argpos]);
        return EXIT_FAILURE;
    }

    // Clear other bits, if NONE bit is set
    if(profile.SI & C3_SI_NONE)
        profile.SI = C3_SI_NONE;
    std::fill_n(std::back_inserter(profiles), nodes.size(), profile);

    // Check that node ids are set
    if(nodes.size() == 0) {
        usage("no node ids specified via --nodes parameter!");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void
ComplianceEnv::SetUp() {
    ASSERT_EQ(setupResult, EXIT_SUCCESS) << "Error in commandline arguments.";
    // TODO check for all implemented facet values
    /* for(C3_Profile profile : profiles) {
        if(profile.SI & C3_SI_SERVICES)
            FAIL() << "Profile tests for C3_SI_SERVICES not implemented.";
        if(profile.SI & C3_SI_MTP)
            FAIL() << "Profile tests for C3_SI_MTP not implemented.";
    } */
    // TODO try to read profile values of nodes (C3_Profile OPC UA variable) and WARN if
    // their value is different or copy their value to profiles, if profiles value is
    // UNKNOWN. --> Could also be done in register

    // Print out infos about nodes
    std::cout << "Node ids that will be tested: " << std::endl;
    for(size_t i = 0; i < nodes.size(); ++i) {
        char *profileString = C3_Profile_print(profiles[i]);
        std::cout << "\tNode no. " << i + 1 << "\t" << nodes[i] << "\t" << profileString
                  << std::endl;
        free(profileString);
    }

    /* Create test server with default config and start it*/
    if(internalServer) {
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                    "Starting internal opcua server.");
        /* Set server config */
        server = UA_Server_new();
        // TODO make Loglevel a cli parameter
        setServerConfig(server, "test", "test_compliance", 4840, UA_LOGLEVEL_WARNING);
        // Create types and parent folder for dummy CCs
        createControlComponentEnvironment(server, nsUriProfiles, "myCompany/consortium");
        // Create all dummy CCs
        createControlComponentsForAllProfiles(server, EXAMPLE_NONE,
                                              true);  // TODO add return and check status
        /* Run the control components */
        cc_executionLoopContext = cc_executionLoopContext_new(server);
        UA_Server_addRepeatedCallback(server, cc_executionLoop, cc_executionLoopContext,
                                      100, &cc_executionLoopId);
        
        /* Start a thread for the server */
        UA_StatusCode retval = UA_Server_run_startup(server);
        ASSERT_STATUS(retval);
        running = UA_TRUE;
        THREAD_CREATE(serverThread, serverloop);
        /* Set default login defined in UA_ServerConfig_setDefault(...) */
        if(logins.size() == 0) {
            logins.push_back({"user1", "password"});
            logins.push_back({"user2", "password1"});
        }
    }
}

void
ComplianceEnv::TearDown() {
    if(internalServer) {
        /* Stop server*/
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                    "Regular shutdown of internal opcua server.");
        running = UA_FALSE;
        THREAD_JOIN(serverThread);
        UA_Server_run_shutdown(server);
        UA_Server_removeCallback(server, cc_executionLoopId);
        cc_executionLoopContext_delete(cc_executionLoopContext);
        clearControlComponentEnvironment(server);
        UA_Server_delete(server);
        server = NULL;
    }
}
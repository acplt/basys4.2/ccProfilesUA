#include <test_ProfileServer.h>

TEST_COMPLIANCE(OM, ANY) {
    DOC_TEST("Checks for any opration mode profile")
    /* Check that status variable is present */
    UA_NodeId statusId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);

    /* Check status childs*/
    DOC_RULE("Control component has hierarchical reference to OPMODE string variable")
    UA_NodeId statusVarId;
    translatePath(client, nsIdxProfiles, statusId, "OPMODE", &statusVarId);
    checkVariableAttributes(client, statusVarId, UA_NS0ID_STRING);

    /* Free ressources */
    UA_NodeId_clear(&statusVarId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(OM, DISABLED_CONTI_Optional) {
    DOC_TEST("Checks for optional features of OM profile CONTI")
    SI_SPECIFIC(CMD) {
        UA_NodeId cmdolistId;
        translatePath(client, nsIdxProfiles, nodeId, "CMDOLIST", &cmdolistId);
        DOC_RULE("CMDOLIST string array contains orders: CONTI, STARTUP, SHUTDOWN")
        checkValueInStringArray(client, cmdolistId, "CONTI");
        checkValueInStringArray(client, cmdolistId, "STARTUP");
        checkValueInStringArray(client, cmdolistId, "SHUTDOWN");
        UA_NodeId_clear(&cmdolistId);
    }
}

void
OM::registerTests(C3_Profile profile, UA_NodeId nodeId) {
    if(profile.OM != C3_OM_NONE) {
        REGISTER_TEST_COMPLIANCE(OM, ANY);
    }
    if(profile.OM == C3_OM_CONTI || profile.OM == C3_OM_UNKNOWN) {
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(OM, DISABLED_CONTI_Optional);
    }
};
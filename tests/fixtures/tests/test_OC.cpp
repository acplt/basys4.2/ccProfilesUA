extern "C" {
#include <C3_Occupation.h>  //to get C3_OC_... state machine values for behaviour checks
}

#include <test_ProfileServer.h>

TEST_COMPLIANCE(OC, ANY) {
    DOC_TEST("Checks for any occupation profile")
    /* Check that status variable is present */
    UA_NodeId statusId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);

    /* Check status childs*/
    DOC_RULE("STATUS has hierarchical reference to OCCST string variable")
    UA_NodeId statusVarId;
    translatePath(client, nsIdxProfiles, statusId, "OCCST", &statusVarId);
    checkVariableAttributes(client, statusVarId, UA_NS0ID_STRING);
    DOC_RULE("STATUS has hierarchical reference to OCCUPIER string variable")
    UA_NodeId_clear(&statusVarId);
    translatePath(client, nsIdxProfiles, statusId, "OCCUPIER", &statusVarId);
    checkVariableAttributes(client, statusVarId, UA_NS0ID_STRING);

    /* Free ressources */
    UA_NodeId_clear(&statusVarId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(OC, ANY_Optional) {
    DOC_TEST("Checks for optional features of any occupation profile")
    /* Check that status variable is present */
    UA_NodeId statusId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);

    /* Check status childs*/
    DOC_RULE("STATUS has hierarchical reference to OCCLAST string variable")
    UA_NodeId statusVarId;
    translatePath(client, nsIdxProfiles, statusId, "OCCLAST", &statusVarId);
    checkVariableAttributes(client, statusVarId, UA_NS0ID_STRING);

    /* Free ressources */
    UA_NodeId_clear(&statusVarId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(OC, OCCUPIED) {
    DOC_TEST("Checks for OC profile OCCUPIED")
    SI_SPECIFIC(OPERATIONS) {
        DOC_RULE("OPERATIONS has hierarchical reference to FREE and OCCUPY method")
        UA_NodeId methodId;
        const char *freePath[2] = {"OPERATIONS", "FREE"};
        translatePath(client, nsIdxProfiles, nodeId, freePath, 2, &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        const char *occupyPath[2] = {"OPERATIONS", "OCCUPY"};
        translatePath(client, nsIdxProfiles, nodeId, occupyPath, 2, &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
    }
}

TEST_COMPLIANCE(OC, OCCUPIED_Behaviour) {
    DOC_TEST("Checks for the behaviour in OC profile OCCUPIED")
    /* Get required status ids */
    UA_NodeId statusId;
    UA_NodeId occstId;
    UA_NodeId occupierId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);
    translatePath(client, nsIdxProfiles, statusId, "OCCST", &occstId);
    translatePath(client, nsIdxProfiles, statusId, "OCCUPIER", &occupierId);

    /* Do SI:CMD specific tests */
    SI_SPECIFIC(CMD) {
        /* Get specific node ids */
        UA_NodeId cmdId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "CMD", &cmdId);

        // Initial state has to be FREE
        tryToCallOrder(client, nsIdxProfiles, nodeId, "FREE", profile.SI, "OCCST",
                       "FREE");
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_FREE]);
        checkValueString(client, occupierId, C3_OC_OCCUPIER_FREE);

        DOC_RULE("Control component can be occupied in FREE. OCCST: FREE --> OCCUPIED")
        // Try to occupy component
        writeString(client, cmdId,
                    concatCMD(C3_OC_ORDERSTRING[C3_OC_ORDER_OCCUPY]).c_str());
        // Check whether occupied
        sleep(ComplianceEnv::behaviourTimeout);
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_OCCUPIED]);
        checkValueString(client, occupierId, getUsername());

        DOC_RULE("Control component stays occupied if occupier sends occupy order.")
        // Try to occupy component again
        writeString(client, cmdId,
                    concatCMD(C3_OC_ORDERSTRING[C3_OC_ORDER_OCCUPY]).c_str());
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether still occupied
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_OCCUPIED]);
        checkValueString(client, occupierId, getUsername());

        DOC_RULE("Control component can't be occupied by different sender.")
        // Try to occupy component with wrong ID
        writeString(client, cmdId, "WrongTestID;OCCUPY;");
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether occupied by old occupier
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_OCCUPIED]);
        checkValueString(client, occupierId, getUsername());

        DOC_RULE("Control component can't be freed by different sender.")
        // Try to free component with wrong ID
        writeString(client, cmdId, "WrongTestID;FREE;");
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether occupied by old occupier
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_OCCUPIED]);
        checkValueString(client, occupierId, getUsername());

        DOC_RULE("Control component can be freed by occupier. OCCST: OCCUPIED --> FREE")
        // Try to free component
        writeString(client, cmdId,
                    concatCMD(C3_OC_ORDERSTRING[C3_OC_ORDER_FREE]).c_str());
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether freed
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_FREE]);
        checkValueString(client, occupierId, C3_OC_OCCUPIER_FREE);

        /* Free ressources */
        UA_NodeId_clear(&cmdId);
    }

    /* Do SI:OPERATIONS specific tests */
    SI_SPECIFIC(OPERATIONS) {
        // Initial state has to be FREE
        tryToCallOrder(client, nsIdxProfiles, nodeId, "FREE", profile.SI, "OCCST",
                       "FREE");
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_FREE]);
        checkValueString(client, occupierId, C3_OC_OCCUPIER_FREE);

        /* Get specific node ids */
        UA_NodeId operationsId = UA_NODEID_NULL;
        UA_NodeId occupyId = UA_NODEID_NULL;
        UA_NodeId freeId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "OPERATIONS", &operationsId);
        translatePath(client, nsIdxProfiles, operationsId,
                      C3_OC_ORDERSTRING[C3_OC_ORDER_OCCUPY], &occupyId);
        translatePath(client, nsIdxProfiles, operationsId,
                      C3_OC_ORDERSTRING[C3_OC_ORDER_FREE], &freeId);
        const char *sender =
            (logins.size() > 0) ? std::get<0>(logins[0]).c_str() : C3_OC_OCCUPIER_ANONYM;
        // We need at least one login to have different senders //TODO move to utilities
        EXPECT_GT(logins.size(), 0)
            << "No login (username, password) given to open a second client.";
        UA_Client *client2 = UA_Client_new();
        UA_StatusCode retval = UA_ClientConfig_setDefault(UA_Client_getConfig(client2));
        if(logins.size() > 1)  // Try to connect with second login
            retval = UA_Client_connectUsername(client2, ComplianceEnv::serverUrl.c_str(),
                                               std::get<0>(logins[1]).c_str(),
                                               std::get<1>(logins[1]).c_str());
        else  // Try to connect second client as Anonymous.
            retval = UA_Client_connect(client2, ComplianceEnv::serverUrl.c_str());
        EXPECT_STATUS(retval) << "Couldn't connect a second client.";

        DOC_RULE("Control component can be occupied in FREE. OCCST: FREE --> OCCUPIED")
        // Try to occupy component
        callMethod(client, occupyId, operationsId);
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether occupied
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_OCCUPIED]);
        checkValueString(client, occupierId, sender);

        DOC_RULE("Control component stays occupied if occupier sends occupy order.")
        // Try to occupy component again
        callMethod(client, occupyId, operationsId, UA_STATUSCODE_GOODNODATA);
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether still occupied
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_OCCUPIED]);
        checkValueString(client, occupierId, sender);

        DOC_RULE("Control component can't be occupied by different sender.")
        // Try to occupy component with wrong ID
        callMethod(client2, occupyId, operationsId, UA_STATUSCODE_BADUSERACCESSDENIED);
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether occupied by old occupier
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_OCCUPIED]);
        checkValueString(client, occupierId, sender);

        DOC_RULE("Control component can't be freed by different sender.")
        // Try to free component with wrong ID
        callMethod(client2, freeId, operationsId, UA_STATUSCODE_BADUSERACCESSDENIED);
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether occupied by old occupier
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_OCCUPIED]);
        checkValueString(client, occupierId, sender);

        DOC_RULE("Control component can be freed by occupier. OCCST: OCCUPIED --> FREE")
        // Try to free component
        callMethod(client, freeId, operationsId);
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether freed
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_FREE]);
        checkValueString(client, occupierId, C3_OC_OCCUPIER_FREE);

        /* Free ressources */
        UA_NodeId_clear(&operationsId);
        UA_NodeId_clear(&occupyId);
        UA_NodeId_clear(&freeId);
        UA_Client_disconnect(client2);
        UA_Client_delete(client2);
    }

    /* Free common ressources */
    UA_NodeId_clear(&occupierId);
    UA_NodeId_clear(&occstId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(OC, OCCUPIED_Optional) {
    DOC_TEST("Checks for optional features of OC profile OCCUPIED")
    SI_SPECIFIC(CMD) {
        UA_NodeId cmdolistId;
        translatePath(client, nsIdxProfiles, nodeId, "CMDOLIST", &cmdolistId);
        DOC_RULE("CMDOLIST string array contains orders: FREE, OCCUPY")
        checkValueInStringArray(client, cmdolistId, "FREE");
        checkValueInStringArray(client, cmdolistId, "OCCUPY");
        UA_NodeId_clear(&cmdolistId);
    }
}

TEST_COMPLIANCE(OC, PRIO) {
    DOC_TEST("Checks for OC profile PRIO")
    SI_SPECIFIC(OPERATIONS) {
        DOC_RULE("OPERATIONS has hierarchical reference to FREE and PRIO method")
        UA_NodeId methodId;
        const char *freePath[2] = {"OPERATIONS", "FREE"};
        translatePath(client, nsIdxProfiles, nodeId, freePath, 2, &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        const char *occupyPath[2] = {"OPERATIONS", "PRIO"};
        translatePath(client, nsIdxProfiles, nodeId, occupyPath, 2, &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
    }
}

TEST_COMPLIANCE(OC, PRIO_Behaviour) {
    DOC_TEST("Checks for the behaviour in OC profile PRIO")
    /* Get required status ids */
    UA_NodeId statusId;
    UA_NodeId occstId;
    UA_NodeId occupierId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);
    translatePath(client, nsIdxProfiles, statusId, "OCCST", &occstId);
    translatePath(client, nsIdxProfiles, statusId, "OCCUPIER", &occupierId);

    /* Do SI:CMD specific tests */
    SI_SPECIFIC(CMD) {
        /* Get specific node ids */
        UA_NodeId cmdId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "CMD", &cmdId);

        // Initial state has to be FREE
        tryToCallOrder(client, nsIdxProfiles, nodeId, "FREE", profile.SI, "OCCST",
                       "FREE");
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_FREE]);
        checkValueString(client, occupierId, C3_OC_OCCUPIER_FREE);

        DOC_RULE("Control component can be occupied in FREE. OCCST: FREE --> PRIO")
        // Try to occupy component
        writeString(client, cmdId,
                    concatCMD(C3_OC_ORDERSTRING[C3_OC_ORDER_PRIO]).c_str());
        // Check whether occupied
        sleep(ComplianceEnv::behaviourTimeout);
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_PRIO]);
        checkValueString(client, occupierId, getUsername());

        DOC_RULE("Control component stays occupied if occupier sends occupy order.")
        // Try to occupy component again
        writeString(client, cmdId,
                    concatCMD(C3_OC_ORDERSTRING[C3_OC_ORDER_PRIO]).c_str());
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether still occupied
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_PRIO]);
        checkValueString(client, occupierId, getUsername());

        DOC_RULE("Control component can't be occupied by different sender.")
        // Try to occupy component with wrong ID
        writeString(client, cmdId, "WrongTestID;PRIO;");
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether occupied by old occupier
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_PRIO]);
        checkValueString(client, occupierId, getUsername());

        DOC_RULE("Control component can't be freed by different sender.")
        // Try to free component with wrong ID
        writeString(client, cmdId, "WrongTestID;FREE;");
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether occupied by old occupier
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_PRIO]);
        checkValueString(client, occupierId, getUsername());

        DOC_RULE("Control component can be freed by occupier. OCCST: PRIO --> FREE")
        // Try to free component
        writeString(client, cmdId,
                    concatCMD(C3_OC_ORDERSTRING[C3_OC_ORDER_FREE]).c_str());
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether freed
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_FREE]);
        checkValueString(client, occupierId, C3_OC_OCCUPIER_FREE);

        /* Free ressources */
        UA_NodeId_clear(&cmdId);
    }

    /* Do SI:OPERATIONS specific tests */
    SI_SPECIFIC(OPERATIONS) {
        // Initial state has to be FREE
        tryToCallOrder(client, nsIdxProfiles, nodeId, "FREE", profile.SI, "OCCST",
                       "FREE");
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_FREE]);
        checkValueString(client, occupierId, C3_OC_OCCUPIER_FREE);

        /* Get specific node ids */
        UA_NodeId operationsId = UA_NODEID_NULL;
        UA_NodeId occupyId = UA_NODEID_NULL;
        UA_NodeId freeId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "OPERATIONS", &operationsId);
        translatePath(client, nsIdxProfiles, operationsId,
                      C3_OC_ORDERSTRING[C3_OC_ORDER_PRIO], &occupyId);
        translatePath(client, nsIdxProfiles, operationsId,
                      C3_OC_ORDERSTRING[C3_OC_ORDER_FREE], &freeId);
        const char *sender =
            (logins.size() > 0) ? std::get<0>(logins[0]).c_str() : C3_OC_OCCUPIER_ANONYM;
        // We need at least one login to have different senders //TODO move to utilities
        EXPECT_GT(logins.size(), 0)
            << "No login (username, password) given to open a second client.";
        UA_Client *client2 = UA_Client_new();
        UA_StatusCode retval = UA_ClientConfig_setDefault(UA_Client_getConfig(client2));
        if(logins.size() > 1)  // Try to connect with second login
            retval = UA_Client_connectUsername(client2, ComplianceEnv::serverUrl.c_str(),
                                               std::get<0>(logins[1]).c_str(),
                                               std::get<1>(logins[1]).c_str());
        else  // Try to connect second client as Anonymous.
            retval = UA_Client_connect(client2, ComplianceEnv::serverUrl.c_str());
        EXPECT_STATUS(retval) << "Couldn't connect a second client.";

        DOC_RULE("Control component can be occupied in FREE. OCCST: FREE --> PRIO")
        // Try to occupy component
        callMethod(client, occupyId, operationsId);
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether occupied
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_PRIO]);
        checkValueString(client, occupierId, sender);

        DOC_RULE("Control component stays occupied if occupier sends occupy order.")
        // Try to occupy component again
        callMethod(client, occupyId, operationsId, UA_STATUSCODE_GOODNODATA);
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether still occupied
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_PRIO]);
        checkValueString(client, occupierId, sender);

        DOC_RULE("Control component can't be occupied by different sender.")
        // Try to occupy component with wrong ID
        callMethod(client2, occupyId, operationsId, UA_STATUSCODE_BADUSERACCESSDENIED);
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether occupied by old occupier
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_PRIO]);
        checkValueString(client, occupierId, sender);

        DOC_RULE("Control component can't be freed by different sender.")
        // Try to free component with wrong ID
        callMethod(client2, freeId, operationsId, UA_STATUSCODE_BADUSERACCESSDENIED);
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether occupied by old occupier
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_PRIO]);
        checkValueString(client, occupierId, sender);

        DOC_RULE("Control component can be freed by occupier. OCCST: PRIO --> FREE")
        // Try to free component
        callMethod(client, freeId, operationsId);
        sleep(ComplianceEnv::behaviourTimeout);
        // Check whether freed
        checkValueString(client, occstId, C3_OC_STATESTRING[C3_OC_STATE_FREE]);
        checkValueString(client, occupierId, C3_OC_OCCUPIER_FREE);

        /* Free ressources */
        UA_NodeId_clear(&operationsId);
        UA_NodeId_clear(&occupyId);
        UA_NodeId_clear(&freeId);
        UA_Client_disconnect(client2);
        UA_Client_delete(client2);
    }

    /* Free common ressources */
    UA_NodeId_clear(&occupierId);
    UA_NodeId_clear(&occstId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(OC, PRIO_Optional) {
    DOC_TEST("Checks for optional features of OC profile PRIO")
    SI_SPECIFIC(CMD) {
        UA_NodeId cmdolistId;
        translatePath(client, nsIdxProfiles, nodeId, "CMDOLIST", &cmdolistId);
        DOC_RULE("CMDOLIST string array contains orders: PRIO")
        checkValueInStringArray(client, cmdolistId, "PRIO");
        UA_NodeId_clear(&cmdolistId);
    }
}

// TODO add checks for CrossProfile values OCCUPY <-> PRIO change
void
OC::registerTests(C3_Profile profile, UA_NodeId nodeId) {
    if(profile.OC != C3_OC_NONE) {
        REGISTER_TEST_COMPLIANCE(OC, ANY);
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(OC, ANY_Optional);
    }
    if(profile.OC & C3_OC_OCCUPIED || profile.OC == C3_OC_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(OC, OCCUPIED);
        REGISTER_TEST_COMPLIANCE(OC, OCCUPIED_Behaviour);
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(OC, OCCUPIED_Optional);
    }
    if(profile.OC & C3_OC_PRIO || profile.OC == C3_OC_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(OC, PRIO);
        REGISTER_TEST_COMPLIANCE(OC, PRIO_Behaviour);
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(OC, PRIO_Optional);
    }
};
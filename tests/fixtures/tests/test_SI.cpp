#include <test_ProfileServer.h>

/*
 * Tests for the SI Profile itself
 */
TEST_COMPLIANCE(SI, ANY) {
    DOC_TEST("Checks for any service interface profile")

    // Check that nodeId parameter is set
    if(UA_NodeId_equal(&nodeId, &UA_NODEID_NULL))
        FAIL() << "NodeId not set.";
    /* Check that node exists */
    UA_LocalizedText displayname;
    UA_StatusCode retval =
        UA_Client_readDisplayNameAttribute(client, nodeId, &displayname);
    ASSERT_STATUS(retval) << ". Didn't find ControlComponent.";
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                "Found control component with display name: %.*s",
                (int)displayname.text.length, displayname.text.data);
    UA_LocalizedText_clear(&displayname);

    /* Check that status variable is present */
    DOC_RULE("Control component has hierarchical reference to STATUS variable / object")
    UA_NodeId statusId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);

    /* Check status childs*/
    // DISCUSS: may also be moved to OM ANY profile check
    UA_NodeId statusVarId;
    DOC_RULE("STATUS has hierarchical reference to WORKST string variable")
    translatePath(client, nsIdxProfiles, statusId, "WORKST", &statusVarId);
    checkVariableAttributes(client, statusVarId, UA_NS0ID_STRING);

    /* Free ressources */
    UA_NodeId_clear(&statusVarId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(SI, ANY_Optional) {
    DOC_TEST("Checks for optional features of any service interface profile")
    /* Check that status variable is present */
    UA_NodeId statusId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);

    /* Check status childs*/
    // DISCUSS: may also be moved to OM ANY profile check
    UA_NodeId statusVarId;
    DOC_RULE("STATUS has hierarchical reference to ER string variable")
    translatePath(client, nsIdxProfiles, statusId, "ER", &statusVarId);
    checkVariableAttributes(client, statusVarId, UA_NS0ID_STRING);
    UA_NodeId_clear(&statusVarId);
    DOC_RULE("STATUS has hierarchical reference to ERLAST string variable")
    translatePath(client, nsIdxProfiles, statusId, "ERLAST", &statusVarId);
    checkVariableAttributes(client, statusVarId, UA_NS0ID_STRING);

    /* TODO Check optional characteristics of STATUS variable --> move to own test */
    // HASTYPEDEFINITON to basys state variabel type
    // HASPROPERTY reference
    // Datatype

    /* Free ressources */
    UA_NodeId_clear(&statusVarId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(SI, CMD) {
    DOC_TEST("Check command input variable")
    /* Check that CMD variable is present */
    DOC_RULE(
        "Control component has hierarchical reference to writable CMD string variable")
    UA_NodeId cmdId;
    translatePath(client, nsIdxProfiles, nodeId, "CMD", &cmdId);
    checkVariableAttributes(client, cmdId, UA_NS0ID_STRING,
                            UA_ACCESSLEVELMASK_READ & UA_ACCESSLEVELMASK_WRITE);
    UA_NodeId_clear(&cmdId);
}

TEST_COMPLIANCE(SI, CMD_Behaviour) {
    DOC_TEST("Check command input behaviour")
    /* Check that CMD variable is present */
    DOC_RULE("CMD input syntax test")
    UA_NodeId cmdId;
    translatePath(client, nsIdxProfiles, nodeId, "CMD", &cmdId);
    // TODO write wrong test values like 'test' 'test,;;' to CMD and check output:
    UA_NodeId_clear(&cmdId);
}

TEST_COMPLIANCE(SI, CMD_Optional) {
    DOC_TEST("Check optional variables for command input")
    /* Check control component childs*/
    DOC_RULE(
        "Control component has hierarchical reference to CMDOLIST string array variable")
    UA_NodeId cmdVarId;
    translatePath(client, nsIdxProfiles, nodeId, "CMDOLIST", &cmdVarId);
    checkVariableAttributes(client, cmdVarId, UA_NS0ID_STRING, UA_ACCESSLEVELMASK_READ,
                            UA_VALUERANK_ONE_DIMENSION, NULL, 1);

    UA_NodeId_clear(&cmdVarId);

    DOC_RULE(
        "Control component has hierarchical reference to CMDOPREF string array variable")
    translatePath(client, nsIdxProfiles, nodeId, "CMDOPREF", &cmdVarId);
    checkVariableAttributes(client, cmdVarId, UA_NS0ID_STRING, UA_ACCESSLEVELMASK_READ,
                            UA_VALUERANK_ONE_DIMENSION, NULL, 1);
    UA_NodeId_clear(&cmdVarId);

    DOC_RULE("Control component has hierarchical reference to CMDLAST string variable")
    translatePath(client, nsIdxProfiles, nodeId, "CMDLAST", &cmdVarId);
    checkVariableAttributes(client, cmdVarId, UA_NS0ID_STRING);
    UA_NodeId_clear(&cmdVarId);
}

TEST_COMPLIANCE(SI, OPERATIONS) {
    DOC_TEST("Check operations interface")
    DOC_RULE("Control component has hierarchical reference to OPERATIONS container")
    translatePath(client, nsIdxProfiles, nodeId, "OPERATIONS", NULL);
}
#ifdef CCPUA_ENABLE_PACKML
TEST_COMPLIANCE(SI, PACKML) {
    DOC_TEST("Check PACKML interface")

    DOC_RULE("Control component has hierarchical reference to object node of type "
             "PackMLBaseObjectType")
    UA_NodeId packMLBaseObjectTypeId = UA_NODEID_NULL;
    translatePath(client, nsIdxPackML, UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                  "PackMLBaseObjectType", &packMLBaseObjectTypeId);
    UA_NodeId baseId = UA_NODEID_NULL;
    hasChildOfType(client, nodeId, packMLBaseObjectTypeId, &baseId);
    UA_NodeId_clear(&packMLBaseObjectTypeId);

    // check for reference only in instances (not in instance declarations)
    UA_NodeClass ccIdNodeClass;
    UA_Client_readNodeClassAttribute(client, nodeId, &ccIdNodeClass);
    if(ccIdNodeClass == UA_NODECLASS_OBJECT || ccIdNodeClass == UA_NODECLASS_VARIABLE) {
        DOC_RULE("PackMLObjects folder has component reference to PackMLBaseObject of "
                 "control component")
        UA_NodeId packMLFolderId = UA_NODEID_NULL;
        translatePath(client, nsIdxPackML, UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                      "PackMLObjects", &packMLFolderId);
        hasReferenceToNode(client, packMLFolderId,
                           UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT), baseId);
        UA_NodeId_clear(&packMLFolderId);
    }

    DOC_RULE("PackMLBaseObject has mandatory childs")
    translatePath(client, nsIdxPackML, baseId, "Admin", NULL);
    translatePath(client, nsIdxPackML, baseId, "BaseStateMachine", NULL);
    UA_NodeId setMethodId = UA_NODEID_NULL;
    translatePath(client, nsIdxPackML, baseId, "SetMachSpeed", &setMethodId);
    translatePath(client, 0, setMethodId, "InputArguments", NULL);
    checkMethodExecutable(client, setMethodId);
    UA_NodeId_clear(&setMethodId);
    translatePath(client, nsIdxPackML, baseId, "SetParameter", &setMethodId);
    translatePath(client, 0, setMethodId, "InputArguments", NULL);
    checkMethodExecutable(client, setMethodId);
    UA_NodeId_clear(&setMethodId);
    translatePath(client, nsIdxPackML, baseId, "SetProduct", &setMethodId);
    translatePath(client, 0, setMethodId, "InputArguments", NULL);
    checkMethodExecutable(client, setMethodId);
    UA_NodeId_clear(&setMethodId);
    translatePath(client, nsIdxPackML, baseId, "SetUnitMode", &setMethodId);
    translatePath(client, 0, setMethodId, "InputArguments", NULL);
    checkMethodExecutable(client, setMethodId);
    UA_NodeId_clear(&setMethodId);
    UA_NodeId packMLStatusId = UA_NODEID_NULL;
    translatePath(client, nsIdxPackML, baseId, "Status", &packMLStatusId);

    DOC_RULE("PackMLBaseObject/Status has mandatory childs")
    UA_NodeId analogItemId = UA_NODEID_NULL;
    translatePath(client, nsIdxPackML, packMLStatusId, "CurMachSpeed", &analogItemId);
    translatePath(client, 0, analogItemId, "EURange", NULL);
    UA_NodeId_clear(&analogItemId);
    translatePath(client, nsIdxPackML, packMLStatusId, "EquipmentBlocked", NULL);
    translatePath(client, nsIdxPackML, packMLStatusId, "EquipmentStarved", NULL);
    translatePath(client, nsIdxPackML, packMLStatusId, "MachSpeed", &analogItemId);
    translatePath(client, 0, analogItemId, "EURange", NULL);
    UA_NodeId_clear(&analogItemId);
    translatePath(client, nsIdxPackML, packMLStatusId, "UnitModeCurrent", NULL);
    translatePath(client, nsIdxPackML, packMLStatusId, "UnitSupportedModes", NULL);
    UA_NodeId_clear(&packMLStatusId);

    DOC_RULE("PackMLBaseObject has state machine hierarchy with mandatory childs")
    const char *pathCurrentStateId[2] = {"CurrentState", "Id"};
    UA_NodeId stateMachineId = UA_NODEID_NULL;
    translatePath(client, nsIdxPackML, baseId, "BaseStateMachine", &stateMachineId);
    translatePath(client, 0, stateMachineId, "AvailableStates", NULL);
    translatePath(client, 0, stateMachineId, "AvailableTransitions", NULL);
    translatePath(client, 0, stateMachineId, pathCurrentStateId, 2, NULL);
    translatePath(client, nsIdxPackML, stateMachineId, "MachineState", &stateMachineId);
    translatePath(client, 0, stateMachineId, "AvailableStates", NULL);
    translatePath(client, 0, stateMachineId, "AvailableTransitions", NULL);
    translatePath(client, 0, stateMachineId, pathCurrentStateId, 2, NULL);
    translatePath(client, nsIdxPackML, stateMachineId, "ExecuteState", &stateMachineId);
    translatePath(client, 0, stateMachineId, "AvailableStates", NULL);
    translatePath(client, 0, stateMachineId, "AvailableTransitions", NULL);
    translatePath(client, 0, stateMachineId, pathCurrentStateId, 2, NULL);
    UA_NodeId_clear(&stateMachineId);

    /* Free common ressources */
    UA_NodeId_clear(&baseId);
}
#endif

void
SI::registerTests(C3_Profile profile, UA_NodeId nodeId) {
    if(profile.SI != C3_SI_NONE) {
        REGISTER_TEST_COMPLIANCE(SI, ANY)
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(SI, ANY_Optional)
    }
    if(profile.SI & C3_SI_CMD || profile.SI == C3_SI_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(SI, CMD)
        REGISTER_TEST_COMPLIANCE(SI, CMD_Behaviour)
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(SI, CMD_Optional)
    }
    if(profile.SI & C3_SI_OPERATIONS || profile.SI == C3_SI_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(SI, OPERATIONS)
    }
#ifdef CCPUA_ENABLE_PACKML
    if(profile.SI & C3_SI_PACKML || profile.SI == C3_SI_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(SI, PACKML)
    }
#endif
}
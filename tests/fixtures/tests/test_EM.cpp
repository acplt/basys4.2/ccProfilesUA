extern "C" {
#include <C3_ExecutionMode.h>  //to get C3_EM_... state machine values for behaviour checks
}

#include <test_ProfileServer.h>

TEST_COMPLIANCE(EM, ANY) {
    DOC_TEST("Checks for any execution mode profile")
    /* Check that status variable is present */
    UA_NodeId statusId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);

    /* Check status childs*/
    DOC_RULE("Control component has hierarchical reference to EXMODE string variable")
    UA_NodeId statusVarId;
    translatePath(client, nsIdxProfiles, statusId, "EXMODE", &statusVarId);
    checkVariableAttributes(client, statusVarId, UA_NS0ID_STRING);

    /* Free ressources */
    UA_NodeId_clear(&statusVarId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(EM, AUTO) {
    DOC_TEST("Checks for EM profile AUTO")
    SI_SPECIFIC(OPERATIONS) {
        DOC_RULE("OPERATIONS has hierarchical reference to AUTO method")
        UA_NodeId methodId;
        const char *path[2] = {"OPERATIONS", "AUTO"};
        translatePath(client, nsIdxProfiles, nodeId, path, 2, &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
    }
}

TEST_COMPLIANCE(EM, AUTO_Behaviour) {
    DOC_TEST("Checks for the behaviour in EM profile AUTO")
    /* Get required status ids */
    UA_NodeId statusId;
    UA_NodeId emstId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);
    translatePath(client, nsIdxProfiles, statusId, "EXMODE", &emstId);

    /* Do SI:CMD specific tests */
    SI_SPECIFIC(CMD) {
        // Initial state has to be AUTO
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_AUTO]);

        /* Get specific node ids */
        UA_NodeId cmdId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "CMD", &cmdId);

        DOC_RULE("Control component stays in AUTO mode if AUTO mode is ordered again.")
        // Try to occupy component again
        writeString(client, cmdId,
                    concatCMD(C3_EM_ORDERSTRING[C3_EM_ORDER_AUTO]).c_str());
        // Check whether still occupied
        sleep(ComplianceEnv::behaviourTimeout);
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_AUTO]);

        /* Free ressources */
        UA_NodeId_clear(&cmdId);
    }

    /* Do SI:OPERATIONS specific tests */
    SI_SPECIFIC(OPERATIONS) {
        // Initial state has to be AUTO
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_AUTO]);

        /* Get specific node ids */
        UA_NodeId operationsId = UA_NODEID_NULL;
        UA_NodeId methodId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "OPERATIONS", &operationsId);
        translatePath(client, nsIdxProfiles, operationsId,
                      C3_EM_ORDERSTRING[C3_EM_ORDER_AUTO], &methodId);

        DOC_RULE("Control component stays in AUTO mode if AUTO mode is ordered again.")
        // Try to occupy component again
        callMethod(client, methodId, operationsId, UA_STATUSCODE_GOODNODATA);
        // Check whether still occupied
        sleep(ComplianceEnv::behaviourTimeout);
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_AUTO]);

        /* Free ressources */
        UA_NodeId_clear(&operationsId);
        UA_NodeId_clear(&methodId);
    }

    /* Free common ressources */
    UA_NodeId_clear(&emstId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(EM, AUTO_Optional) {
    DOC_TEST("Checks for optional features of EM profile AUTO")
    SI_SPECIFIC(CMD) {
        UA_NodeId cmdolistId;
        translatePath(client, nsIdxProfiles, nodeId, "CMDOLIST", &cmdolistId);
        DOC_RULE("CMDOLIST string array contains orders: AUTO")
        checkValueInStringArray(client, cmdolistId, "AUTO");
        UA_NodeId_clear(&cmdolistId);
    }
}

TEST_COMPLIANCE(EM, SEMIAUTO) {
    DOC_TEST("Checks for EM profile SEMIAUTO")
    SI_SPECIFIC(OPERATIONS) {
        DOC_RULE("OPERATIONS has hierarchical reference to SEMIAUTO method")
        UA_NodeId methodId;
        const char *path[2] = {"OPERATIONS", "SEMIAUTO"};
        translatePath(client, nsIdxProfiles, nodeId, path, 2, &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
    }
}

TEST_COMPLIANCE(EM, SEMIAUTO_Behaviour) {
    DOC_TEST("Checks for the behaviour in EM profile SEMIAUTO")
    /* Get required status ids */
    UA_NodeId statusId;
    UA_NodeId emstId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);
    translatePath(client, nsIdxProfiles, statusId, "EXMODE", &emstId);

    tryToCallOrder(client, nsIdxProfiles, nodeId, "SEMIAUTO", profile.SI, "EXMODE",
                   "SEMIAUTO");

    /* Do SI:CMD specific tests */
    SI_SPECIFIC(CMD) {
        // Initial state has to be SEMIAUTO
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_SEMIAUTO]);

        /* Get specific node ids */
        UA_NodeId cmdId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "CMD", &cmdId);

        DOC_RULE(
            "Control component stays in SEMIAUTO mode if SEMIAUTO mode is ordered again.")
        // Try to occupy component again
        writeString(client, cmdId,
                    concatCMD(C3_EM_ORDERSTRING[C3_EM_ORDER_SEMIAUTO]).c_str());
        // Check whether still occupied
        sleep(ComplianceEnv::behaviourTimeout);
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_SEMIAUTO]);

        /* Free ressources */
        UA_NodeId_clear(&cmdId);
    }

    /* Do SI:OPERATIONS specific tests */
    SI_SPECIFIC(OPERATIONS) {
        // Initial state has to be SEMIAUTO
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_SEMIAUTO]);

        /* Get specific node ids */
        UA_NodeId operationsId = UA_NODEID_NULL;
        UA_NodeId methodId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "OPERATIONS", &operationsId);
        translatePath(client, nsIdxProfiles, operationsId,
                      C3_EM_ORDERSTRING[C3_EM_ORDER_SEMIAUTO], &methodId);

        DOC_RULE(
            "Control component stays in SEMIAUTO mode if SEMIAUTO mode is ordered again.")
        // Try to occupy component again
        callMethod(client, methodId, operationsId, UA_STATUSCODE_GOODNODATA);
        // Check whether still occupied
        sleep(ComplianceEnv::behaviourTimeout);
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_SEMIAUTO]);

        /* Free ressources */
        UA_NodeId_clear(&operationsId);
        UA_NodeId_clear(&methodId);
    }

    /* Free common ressources */
    UA_NodeId_clear(&emstId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(EM, SEMIAUTO_Optional) {
    DOC_TEST("Checks for optional features of EM profile SEMIAUTO")
    SI_SPECIFIC(CMD) {
        UA_NodeId cmdolistId;
        translatePath(client, nsIdxProfiles, nodeId, "CMDOLIST", &cmdolistId);
        DOC_RULE("CMDOLIST string array contains orders: SEMIAUTO")
        checkValueInStringArray(client, cmdolistId, "SEMIAUTO");
        UA_NodeId_clear(&cmdolistId);
    }
}

TEST_COMPLIANCE(EM, MANUAL) {
    DOC_TEST("Checks for EM profile MANUAL")
    SI_SPECIFIC(OPERATIONS) {
        DOC_RULE("OPERATIONS has hierarchical reference to MANUAL method")
        UA_NodeId methodId;
        const char *path[2] = {"OPERATIONS", "MANUAL"};
        translatePath(client, nsIdxProfiles, nodeId, path, 2, &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
    }
}

TEST_COMPLIANCE(EM, MANUAL_Behaviour) {
    DOC_TEST("Checks for the behaviour in EM profile MANUAL")
    /* Get required status ids */
    UA_NodeId statusId;
    UA_NodeId emstId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);
    translatePath(client, nsIdxProfiles, statusId, "EXMODE", &emstId);

    tryToCallOrder(client, nsIdxProfiles, nodeId, "MANUAL", profile.SI, "EXMODE",
                   "MANUAL");

    /* Do SI:CMD specific tests */
    SI_SPECIFIC(CMD) {
        // Initial state has to be MANUAL
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_MANUAL]);

        /* Get specific node ids */
        UA_NodeId cmdId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "CMD", &cmdId);

        DOC_RULE(
            "Control component stays in MANUAL mode if MANUAL mode is ordered again.")
        // Try to occupy component again
        writeString(client, cmdId,
                    concatCMD(C3_EM_ORDERSTRING[C3_EM_ORDER_MANUAL]).c_str());
        // Check whether still occupied
        sleep(ComplianceEnv::behaviourTimeout);
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_MANUAL]);

        /* Free ressources */
        UA_NodeId_clear(&cmdId);
    }

    /* Do SI:OPERATIONS specific tests */
    SI_SPECIFIC(OPERATIONS) {
        // Initial state has to be MANUAL
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_MANUAL]);

        /* Get specific node ids */
        UA_NodeId operationsId = UA_NODEID_NULL;
        UA_NodeId methodId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "OPERATIONS", &operationsId);
        translatePath(client, nsIdxProfiles, operationsId,
                      C3_EM_ORDERSTRING[C3_EM_ORDER_MANUAL], &methodId);

        DOC_RULE(
            "Control component stays in MANUAL mode if MANUAL mode is ordered again.")
        // Try to occupy component again
        callMethod(client, methodId, operationsId, UA_STATUSCODE_GOODNODATA);
        // Check whether still occupied
        sleep(ComplianceEnv::behaviourTimeout);
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_MANUAL]);

        /* Free ressources */
        UA_NodeId_clear(&operationsId);
        UA_NodeId_clear(&methodId);
    }

    /* Free common ressources */
    UA_NodeId_clear(&emstId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(EM, MANUAL_Optional) {
    DOC_TEST("Checks for optional features of EM profile MANUAL")
    SI_SPECIFIC(CMD) {
        UA_NodeId cmdolistId;
        translatePath(client, nsIdxProfiles, nodeId, "CMDOLIST", &cmdolistId);
        DOC_RULE("CMDOLIST string array contains orders: MANUAL")
        checkValueInStringArray(client, cmdolistId, "MANUAL");
        UA_NodeId_clear(&cmdolistId);
    }
}

TEST_COMPLIANCE(EM, SIMULATE) {
    DOC_TEST("Checks for EM profile SIMULATE")
    SI_SPECIFIC(OPERATIONS) {
        DOC_RULE("OPERATIONS has hierarchical reference to SIMULATE method")
        UA_NodeId methodId;
        const char *path[2] = {"OPERATIONS", "SIMULATE"};
        translatePath(client, nsIdxProfiles, nodeId, path, 2, &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
    }
}

TEST_COMPLIANCE(EM, SIMULATE_Behaviour) {
    DOC_TEST("Checks for the behaviour in EM profile SIMULATE")
    /* Get required status ids */
    UA_NodeId statusId;
    UA_NodeId emstId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);
    translatePath(client, nsIdxProfiles, statusId, "EXMODE", &emstId);

    tryToCallOrder(client, nsIdxProfiles, nodeId, "SIMULATE", profile.SI, "EXMODE",
                   "SIMULATE");

    /* Do SI:CMD specific tests */
    SI_SPECIFIC(CMD) {
        // Initial state has to be SIMULATE
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_SIMULATE]);

        /* Get specific node ids */
        UA_NodeId cmdId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "CMD", &cmdId);

        DOC_RULE(
            "Control component stays in SIMULATE mode if SIMULATE mode is ordered again.")
        // Try to occupy component again
        writeString(client, cmdId,
                    concatCMD(C3_EM_ORDERSTRING[C3_EM_ORDER_SIMULATE]).c_str());
        // Check whether still occupied
        sleep(ComplianceEnv::behaviourTimeout);
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_SIMULATE]);

        /* Free ressources */
        UA_NodeId_clear(&cmdId);
    }

    /* Do SI:OPERATIONS specific tests */
    SI_SPECIFIC(OPERATIONS) {
        // Initial state has to be SIMULATE
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_SIMULATE]);

        /* Get specific node ids */
        UA_NodeId operationsId = UA_NODEID_NULL;
        UA_NodeId methodId = UA_NODEID_NULL;
        translatePath(client, nsIdxProfiles, nodeId, "OPERATIONS", &operationsId);
        translatePath(client, nsIdxProfiles, operationsId,
                      C3_EM_ORDERSTRING[C3_EM_ORDER_SIMULATE], &methodId);

        DOC_RULE(
            "Control component stays in SIMULATE mode if SIMULATE mode is ordered again.")
        // Try to occupy component again
        callMethod(client, methodId, operationsId, UA_STATUSCODE_GOODNODATA);
        // Check whether still occupied
        sleep(ComplianceEnv::behaviourTimeout);
        checkValueString(client, emstId, C3_EM_STATESTRING[C3_EM_STATE_SIMULATE]);

        /* Free ressources */
        UA_NodeId_clear(&operationsId);
        UA_NodeId_clear(&methodId);
    }

    /* Free common ressources */
    UA_NodeId_clear(&emstId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(EM, SIMULATE_Optional) {
    DOC_TEST("Checks for optional features of EM profile SIMULATE")
    SI_SPECIFIC(CMD) {
        UA_NodeId cmdolistId;
        translatePath(client, nsIdxProfiles, nodeId, "CMDOLIST", &cmdolistId);
        DOC_RULE("CMDOLIST string array contains orders: SIMULATE")
        checkValueInStringArray(client, cmdolistId, "SIMULATE");
        UA_NodeId_clear(&cmdolistId);
    }
}

void
EM::registerTests(C3_Profile profile, UA_NodeId nodeId) {
    if(profile.EM != C3_EM_NONE) {
        REGISTER_TEST_COMPLIANCE(EM, ANY);
    }
    if(profile.EM & C3_EM_AUTO || profile.EM == C3_EM_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(EM, AUTO);
        REGISTER_TEST_COMPLIANCE(EM, AUTO_Behaviour);
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(EM, AUTO_Optional);
    }
    if(profile.EM & C3_EM_MANUAL || profile.EM == C3_EM_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(EM, MANUAL);
        REGISTER_TEST_COMPLIANCE(EM, MANUAL_Behaviour);
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(EM, MANUAL_Optional);
    }
    if(profile.EM & C3_EM_SEMIAUTO || profile.EM == C3_EM_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(EM, SEMIAUTO);
        REGISTER_TEST_COMPLIANCE(EM, SEMIAUTO_Behaviour);
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(EM, SEMIAUTO_Optional);
    }
    if(profile.EM & C3_EM_SIMULATE || profile.EM == C3_EM_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(EM, SIMULATE);
        REGISTER_TEST_COMPLIANCE(EM, SIMULATE_Behaviour);
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(EM, SIMULATE_Optional);
    }
};
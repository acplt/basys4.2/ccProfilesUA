#include <test_ProfileServer.h>

TEST_COMPLIANCE(ES, ANY) {
    DOC_TEST("Checks for any execution state profile")
    /* Check that status variable is present */
    UA_NodeId statusId;
    translatePath(client, nsIdxProfiles, nodeId, "STATUS", &statusId);

    /* Check status childs*/
    DOC_RULE("Control component has hierarchical reference to EXST string variable")
    UA_NodeId statusVarId;
    translatePath(client, nsIdxProfiles, statusId, "EXST", &statusVarId);
    checkVariableAttributes(client, statusVarId, UA_NS0ID_STRING);

    /* Free ressources */
    UA_NodeId_clear(&statusVarId);
    UA_NodeId_clear(&statusId);
}

TEST_COMPLIANCE(ES, BASYS) {
    DOC_TEST("Checks for ES profile BASYS")
    SI_SPECIFIC(OPERATIONS) {
        DOC_RULE("OPERATIONS has hierarchical reference to START, STOP, RESET, ABORT and "
                 "CLEAR method")
        UA_NodeId methodId;
        UA_NodeId objectId;
        translatePath(client, nsIdxProfiles, nodeId, "OPERATIONS", &objectId);

        translatePath(client, nsIdxProfiles, objectId, "START", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "STOP", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "RESET", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "ABORT", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "CLEAR", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);

        UA_NodeId_clear(&objectId);
    }
}

TEST_COMPLIANCE(ES, BASYS_Optional) {
    DOC_TEST("Checks for optional features of ES profile BASYS")
    SI_SPECIFIC(CMD) {
        UA_NodeId cmdolistId;
        translatePath(client, nsIdxProfiles, nodeId, "CMDOLIST", &cmdolistId);
        DOC_RULE(
            "CMDOLIST string array contains orders: START, STOP, RESET, ABORT and CLEAR")
        checkValueInStringArray(client, cmdolistId, "START");
        checkValueInStringArray(client, cmdolistId, "STOP");
        checkValueInStringArray(client, cmdolistId, "RESET");
        checkValueInStringArray(client, cmdolistId, "ABORT");
        checkValueInStringArray(client, cmdolistId, "CLEAR");
        UA_NodeId_clear(&cmdolistId);
    }
}

TEST_COMPLIANCE(ES, PACKML) {
    DOC_TEST("Checks for ES profile PACKML")
    SI_SPECIFIC(OPERATIONS) {
        DOC_RULE("OPERATIONS has hierarchical reference to START, STOP, RESET, HOLD, "
                 "UNHOLD, SUSPEND, UNSUSPEND, ABORT and "
                 "CLEAR method")
        UA_NodeId methodId;
        UA_NodeId objectId;
        translatePath(client, nsIdxProfiles, nodeId, "OPERATIONS", &objectId);

        translatePath(client, nsIdxProfiles, objectId, "START", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "STOP", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "RESET", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "HOLD", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "UNHOLD", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "SUSPEND", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "UNSUSPEND", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "ABORT", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);
        translatePath(client, nsIdxProfiles, objectId, "CLEAR", &methodId);
        checkMethodExecutable(client, methodId);
        UA_NodeId_clear(&methodId);

        UA_NodeId_clear(&objectId);
    }
}

TEST_COMPLIANCE(ES, PACKML_Optional) {
    DOC_TEST("Checks for optional features of ES profile PACKML")
    SI_SPECIFIC(CMD) {
        UA_NodeId cmdolistId;
        translatePath(client, nsIdxProfiles, nodeId, "CMDOLIST", &cmdolistId);
        DOC_RULE("CMDOLIST string array contains orders: START, STOP, RESET, HOLD, "
                 "UNHOLD, SUSPEND, UNSUSPEND, ABORT and CLEAR")
        checkValueInStringArray(client, cmdolistId, "START");
        checkValueInStringArray(client, cmdolistId, "STOP");
        checkValueInStringArray(client, cmdolistId, "RESET");
        checkValueInStringArray(client, cmdolistId, "HOLD");
        checkValueInStringArray(client, cmdolistId, "UNHOLD");
        checkValueInStringArray(client, cmdolistId, "SUSPEND");
        checkValueInStringArray(client, cmdolistId, "UNSUSPEND");
        checkValueInStringArray(client, cmdolistId, "ABORT");
        checkValueInStringArray(client, cmdolistId, "CLEAR");
        UA_NodeId_clear(&cmdolistId);
    }
}

void
ES::registerTests(C3_Profile profile, UA_NodeId nodeId) {
    if(profile.ES != C3_ES_NONE) {
        REGISTER_TEST_COMPLIANCE(ES, ANY);
    }
    if(profile.ES == C3_ES_BASYS || profile.ES == C3_ES_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(ES, BASYS);
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(ES, BASYS_Optional);
    }
    if(profile.ES == C3_ES_PACKML || profile.ES == C3_ES_UNKNOWN) {
        REGISTER_TEST_COMPLIANCE(ES, PACKML);
        if(profile.optional)
            REGISTER_TEST_COMPLIANCE(ES, PACKML_Optional);
    }
};
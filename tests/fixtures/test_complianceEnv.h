/*
 ============================================================================
 Name        : test_complianceEnv.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Definition of the compliance test environment for ClientServer
               and ProfileServer test fixtures.
 ============================================================================
 */
#ifndef TEST_COMPLIANCEENV_H
#define TEST_COMPLIANCEENV_H

#include <test_utilities.h>

extern "C" {
#include <../open62541/tests/testing-plugins/thread_wrapper.h>
}

// TODO create own namespace for compliance tests: namespace ct
/*
 * Environment, that is setup and teared down for every compliance test.
 * It holds all parsed command line options and static variables for the tests.
 */
class ComplianceEnv : public ::testing::Environment {
  private:
    int parseMainArgs(int argc, char **argv);
    static int setupResult;
    // internalServer
    static bool internalServer;
    static UA_Boolean running;
    static THREAD_HANDLE serverThread;
    THREAD_CALLBACK(serverloop);
    static void *cc_executionLoopContext;
    UA_UInt64 cc_executionLoopId;

  public:
    static std::string serverUrl;
    // TODO protect usernames and passwords
    static std::vector<std::tuple<std::string, std::string>> logins;
    static const char *nsUriProfiles;
    static std::vector<UA_NodeId> nodes;  // Defines the nodes that should be tested
    static std::vector<C3_Profile>
        profiles;  // Defines a profile to test for each node id
    // TODO add statusBeforeTest vector --> Add ControlComponentStatus struct to CCs
    static unsigned int behaviourTimeout;
    // internal Server
    static UA_Server *server;

    ComplianceEnv(int argc, char **argv) {
        // Parse main arguments (nodes needed as test parameter and hence need to be set
        // before SetUp is called.)
        setupResult = parseMainArgs(argc, argv);
    };
    ~ComplianceEnv() {
        for(UA_NodeId id : nodes) {
            UA_NodeId_clear(&id);
        }
    }
    void
    SetUp();
    void
    TearDown();
};

#endif /* TEST_COMPLIANCEENV_H */
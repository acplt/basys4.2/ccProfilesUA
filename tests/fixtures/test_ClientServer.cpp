#include <test_ClientServer.h>

UA_Client *ClientServerTest::client = NULL;
std::vector<std::tuple<std::string, std::string>> ClientServerTest::logins;

/* Settig up a generic server and client as a test fixture */
void
ClientServerTest::SetUpTestCase() {
    UA_StatusCode retval = UA_STATUSCODE_GOOD;
    logins = ComplianceEnv::logins;
    /* Create test client with default config */
    client = UA_Client_new();
    UA_ClientConfig *config = UA_Client_getConfig(client);
    //TODO make Loglevel a cli parameter
    config->logger = UA_Log_Stdout_withLevel(UA_LOGLEVEL_WARNING);
    retval = UA_ClientConfig_setDefault(UA_Client_getConfig(client));
    ASSERT_STATUS(retval);
    /* Connect client */
    if(logins.size() > 0)  // Connect with first login
        retval = UA_Client_connectUsername(client, ComplianceEnv::serverUrl.c_str(),
                                           std::get<0>(logins[0]).c_str(),
                                           std::get<1>(logins[0]).c_str());
    else  // Connect as anonymous
        retval = UA_Client_connect(client, ComplianceEnv::serverUrl.c_str());
    ASSERT_STATUS(retval);
}
void
ClientServerTest::TearDownTestCase() {
    /* Disconnect client */
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                "Regular disconnect of opcua client.");
    UA_Client_disconnect(client);
    UA_Client_delete(client);
    client = NULL;
}
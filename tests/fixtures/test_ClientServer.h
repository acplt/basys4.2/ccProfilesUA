/*
 ============================================================================
 Name        : test_ClientServer.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Abstract fixtures for OPC UA client server tests.
 ============================================================================
 */
#ifndef TEST_CLIENTSERVER_H
#define TEST_CLIENTSERVER_H

#include <test_complianceEnv.h>

/*
 * Abstract fixture that can establish a connection between an internal or external OPC UA
 * server and client
 */
class ClientServerTest : public testing::Test {
  protected:
    static UA_Client *client;
    static std::vector<std::tuple<std::string, std::string>> logins;

  public:
    static void
    SetUpTestCase();
    static void
    TearDownTestCase();
};

#endif /* TEST_CLIENTSERVER_H */
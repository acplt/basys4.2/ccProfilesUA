
#include <test_ProfileServer.h>

UA_UInt16 ProfileServerTest::nsIdxProfiles;
UA_UInt16 ProfileServerTest::nsIdxPackML;

/* Setup and Teardown for all ProfileServerTests */
void
ProfileServerTest::SetUpTestCase() {
    ClientServerTest::SetUpTestCase();
    UA_String nsUri = UA_STRING((char *)ComplianceEnv::nsUriProfiles);
    UA_StatusCode retval = UA_Client_NamespaceGetIndex(client, &nsUri, &nsIdxProfiles);
    ASSERT_STATUS(retval) << "Profiles namespace indes (" << ComplianceEnv::nsUriProfiles
                          << ") wasn't found.";
    // Check if PACKML index is needed and search for it if necessary
    nsIdxPackML = UA_UINT16_MAX;
#ifdef CCPUA_ENABLE_PACKML
    for(size_t i = 0; i < ComplianceEnv::nodes.size(); ++i) {
        if(ComplianceEnv::profiles[i].SI & C3_SI_PACKML) {
            UA_String nsUriPackML = UA_STRING((char *)NS_PACKML_URI);
            retval = UA_Client_NamespaceGetIndex(client, &nsUriPackML, &nsIdxPackML);
            ASSERT_STATUS(retval)
                << " PackML namespace index (" << NS_PACKML_URI
                << ") wasn't found for SI:PACKML of node " << ComplianceEnv::nodes[i];
            break;
        }
    }
#endif

    // TODO add cmdline flag for behaviour / static checks, so
    // that control component is only changed from tests if desired

    // Loop over nodes and try to set control component to default states if node is an
    // instance
    UA_NodeClass ccIdNodeClass;
    // TODO get old states and save in array --> Reset in TearDown
    for(size_t i = 0; i < ComplianceEnv::nodes.size(); ++i) {
        UA_Client_readNodeClassAttribute(client, ComplianceEnv::nodes[i], &ccIdNodeClass);
        if(!(ccIdNodeClass == UA_NODECLASS_OBJECT ||
             ccIdNodeClass == UA_NODECLASS_VARIABLE))
            continue;
        tryToCallOrder(client, nsIdxProfiles, ComplianceEnv::nodes[i], "OCCUPY",
                       ComplianceEnv::profiles[i].SI, "OCCST", "OCCUPIED");
        tryToCallOrder(client, nsIdxProfiles, ComplianceEnv::nodes[i], "STOP",
                       ComplianceEnv::profiles[i].SI, "EXST", "STOPPED");
        tryToCallOrder(client, nsIdxProfiles, ComplianceEnv::nodes[i], "AUTO",
                       ComplianceEnv::profiles[i].SI, "EXMODE", "AUTO");
    }
}

void
ProfileServerTest::TearDownTestCase() {
    // TODO Loop over nodes and try to reset control component to old states
    UA_NodeClass ccIdNodeClass;
    for(size_t i = 0; i < ComplianceEnv::nodes.size(); ++i) {
        UA_Client_readNodeClassAttribute(client, ComplianceEnv::nodes[i], &ccIdNodeClass);
        if(!(ccIdNodeClass == UA_NODECLASS_OBJECT ||
             ccIdNodeClass == UA_NODECLASS_VARIABLE))
            continue;
        tryToCallOrder(client, nsIdxProfiles, ComplianceEnv::nodes[i], "STOP",
                       ComplianceEnv::profiles[i].SI, "EXST", "STOPPED");
        tryToCallOrder(client, nsIdxProfiles, ComplianceEnv::nodes[i], "AUTO",
                       ComplianceEnv::profiles[i].SI, "EXMODE", "AUTO");
        tryToCallOrder(client, nsIdxProfiles, ComplianceEnv::nodes[i], "FREE",
                       ComplianceEnv::profiles[i].SI, "OCCST", "FREE");
    }
    ClientServerTest::TearDownTestCase();
}

void
registerComplianceTests() {
    // Loop over node-profile combinations and register tests
    for(size_t i = 0; i < ComplianceEnv::nodes.size(); ++i) {
        SI::registerTests(ComplianceEnv::profiles[i], ComplianceEnv::nodes[i]);
        OC::registerTests(ComplianceEnv::profiles[i], ComplianceEnv::nodes[i]);
        EM::registerTests(ComplianceEnv::profiles[i], ComplianceEnv::nodes[i]);
        ES::registerTests(ComplianceEnv::profiles[i], ComplianceEnv::nodes[i]);
        OM::registerTests(ComplianceEnv::profiles[i], ComplianceEnv::nodes[i]);
    }
}

void
addNodeToTests(C3_Profile profile, UA_NodeId nodeId) {
    // Fill profiles if uneven till size equals nodes.size
    while(ComplianceEnv::profiles.size() < ComplianceEnv::nodes.size()) {
        ComplianceEnv::profiles.push_back(ComplianceEnv::profiles.back());
    }
    // Add node / profile combination to test
    std::cout << "Adding tests for node-profile combination:\t" << nodeIdToString(&nodeId)
              << "\t" << C3_Profile_print(profile) << std::endl;

    ComplianceEnv::profiles.push_back(profile);
    ComplianceEnv::nodes.push_back(nodeId);
}
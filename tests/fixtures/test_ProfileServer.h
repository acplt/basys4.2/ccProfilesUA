/*
 ============================================================================
 Name        : test_ProfileServer.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Abstract and profile specific fixtures for profile tests.
 ============================================================================
 */
#ifndef TEST_PROFILESERVER_H
#define TEST_PROFILESERVER_H

#include <test_ClientServer.h>
#include <test_complianceEnv.h>

/*
 * A test fixture with server and client connected (extends ClientServerTest), but a
 * profile_server as example may be used as server if internalServer is set to true. This
 * is the base class for other profile tests, as the test parameters are the same.
 * @param profile --> service interface profile to test against
 * @param nodeId --> root node id of the control component test node
 */
class ProfileServerTest : public ClientServerTest {
  protected:
    C3_Profile profile;
    UA_NodeId nodeId;
    static UA_UInt16 nsIdxProfiles;
    static UA_UInt16 nsIdxPackML;

  public:
    ProfileServerTest(C3_Profile profile, UA_NodeId nodeId)
        : profile(profile), nodeId(nodeId){};
    static void
    SetUpTestCase();
    static void
    TearDownTestCase();
};

/*
 * Test fixtures for specific profiles, which are implemented in test_PROFILENAME.cpp
 * sources. Each Profilename has an own class, so that gtest_filter=*C3_PROFILENAME * can
 * be easily applied.
 */
#define FIXTURE_COMPLIANCE(profileName)                                                  \
    class profileName : public ProfileServerTest {                                       \
      public:                                                                            \
        profileName(C3_Profile profile, UA_NodeId nodeId)                                \
            : ProfileServerTest(profile, nodeId){};                                      \
        static void                                                                      \
        registerTests(C3_Profile profile, UA_NodeId nodeId);                             \
    };
FIXTURE_COMPLIANCE(SI);
FIXTURE_COMPLIANCE(OC);
FIXTURE_COMPLIANCE(EM);
FIXTURE_COMPLIANCE(ES);
FIXTURE_COMPLIANCE(OM);

/* Macros to define and register the compliance tests */
#define GENERATE_TESTNAME(profile, name) TEST_##profile##_##name
#define TEST_COMPLIANCE(profileName, testName)                                           \
    class GENERATE_TESTNAME(profileName, testName) : public profileName {                \
        void                                                                             \
        TestBody() override;                                                             \
                                                                                         \
      public:                                                                            \
        GENERATE_TESTNAME(profileName, testName)                                         \
        (C3_Profile profile, UA_NodeId nodeId) : profileName(profile, nodeId){};         \
    };                                                                                   \
    void GENERATE_TESTNAME(profileName, testName)::TestBody()
/* Macro to register a single COMPLIANCE Test */
// requires variable profile.SI and nodeId
// TODO use description as typeinfo parameter e.g. "Service Interface Profile Tests"
// TODO use parameter description? ("SI: " + std::string(C3_SI_TOSTRING(si)) + ", " +
// nodeIdToString(&nodeId)).c_str()
#define REGISTER_TEST_COMPLIANCE(profileName, testName)                                  \
    ::testing::RegisterTest(                                                             \
        (nodeIdToString(&nodeId) + "/" + #profileName).c_str(), #testName, nullptr,      \
        nullptr, __FILE__, __LINE__, [=]() -> profileName * {                            \
            return new GENERATE_TESTNAME(profileName, testName)(profile, nodeId);        \
        });
/* Registers all compliance tests for all nodeIds */
// TODO add comand line parameter for --optional, --behaviour to register only specific
// tests
void
registerComplianceTests();
/* Add a profile-nodeId combination to the test list.
 * Tests need to be registered with registerComplianceTests afterwards.
 */
void
addNodeToTests(C3_Profile profile, UA_NodeId nodeId);

/*
 * Macros for test and test rule documentation.
 * DOC_TEST should be used to initialize every compliance suite test.
 * DOC_RULE should be used to describe rules for each profile test.
 * The DOC_... defines make shure everything is printed out and saved to the logs.
 */
// TODO create second logger to set loglevels and outputs for userland independently and
// make it a cli parameter
#define DOC_TEST(text)                                                                   \
    RecordProperty("description", text);                                                 \
    RecordProperty("profile", C3_SI_TOSTRING(profile.SI));                               \
    RecordProperty("nodeId", nodeIdToString(&nodeId));                                   \
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Test: %s", text);               \
    SCOPED_TRACE(text);

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define DOC_RULE(text)                                                                   \
    RecordProperty("rule-" TOSTRING(__LINE__), text);                                    \
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Check: %s", text);              \
    SCOPED_TRACE(text);
/*
 * Start a region which is specific to an SI profile.
 * Needs braces: SI_SPECIFIC(CMD) { <specific test code> }.
 * Should be placed in a ProfileServerTest class as it reads the "profile" variable.
 */
#define SI_SPECIFIC(siProfile)                                                           \
    if(profile.SI & C3_SI_##siProfile || profile.SI == C3_SI_UNKNOWN)

#endif /* TEST_PROFILESERVER_H */
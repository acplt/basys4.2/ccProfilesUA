#include <open62541/plugin/accesscontrol_default.h>

#include <profile_utilities.h>

UA_UInt16 NS_PROFILES = 2;
UA_UInt16 NS_DSTYPES = 3; 

UA_StatusCode
markMandatory(UA_Server *server, UA_NodeId id, bool mandatory,
              bool useCustomModellingRule) {
    UA_StatusCode result = UA_STATUSCODE_GOOD;
    if(useCustomModellingRule) {
        result = UA_Server_addReference(
            server, id, UA_NODEID_NUMERIC(0, UA_NS0ID_HASMODELLINGRULE),
            UA_EXPANDEDNODEID_NUMERIC(NS_PROFILES,
                                      NS2OFFSET_CCMODELLINGRULES + (mandatory ? 1 : 2)),
            UA_TRUE);
    }
    result |= UA_Server_addReference(
        server, id, UA_NODEID_NUMERIC(0, UA_NS0ID_HASMODELLINGRULE),
        UA_EXPANDEDNODEID_NUMERIC(0, (!mandatory || useCustomModellingRule)
                                         ? UA_NS0ID_MODELLINGRULE_OPTIONAL
                                         : UA_NS0ID_MODELLINGRULE_MANDATORY),
        UA_TRUE);
    return result;
}

UA_StatusCode
markInterface(UA_Server *server, UA_NodeId id, C3_SI_FACET profile) {
    return UA_Server_addReference(
        server, id, UA_NODEID_NUMERIC(0, UA_NS0ID_HASINTERFACE),
        UA_EXPANDEDNODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCINTERFACETYPES +
                                                   NS2FACTOR_PROFILES * profile),
        UA_TRUE);
}

UA_StatusCode
markFacetModellingRule(UA_Server *server, UA_NodeId id, FACET_GROUP facetGroupNo,
                       int *facetValues, int facetValuesSize) {
    UA_StatusCode result = UA_STATUSCODE_GOOD;
    for(int i = 0; i < facetValuesSize; ++i) {
        // Calculate index for bitmask facet values
        int facetValueNo = 0;
        if(facetValues[i] > 0 && facetGroupNo < 3) {
            while(!(facetValues[i] & (1 << facetValueNo++)))
                ;
        } else
            facetValueNo = facetValues[i];
        // Add reference to modelling rule
        result = UA_Server_addReference(
            server, id, UA_NODEID_NUMERIC(0, UA_NS0ID_HASMODELLINGRULE),
            UA_EXPANDEDNODEID_NUMERIC(
                NS_PROFILES, NS2OFFSET_CCMODELLINGRULES +
                                 (facetGroupNo + 1) * NS2FACTOR_PROFILES + facetValueNo),
            UA_TRUE);
    }
    return result;
}

static UA_StatusCode
addProperty(UA_Server *server, char *browsename, char *displayname, char *description,
            UA_NodeId id, UA_NodeId parentId, UA_VariableAttributes vAttr,
            UA_Boolean writeable) {
    vAttr.displayName = UA_LOCALIZEDTEXT("en-US", displayname);
    vAttr.description = UA_LOCALIZEDTEXT("en-US", description);
    if(writeable) {
        vAttr.writeMask = UA_WRITEMASK_VALUEFORVARIABLETYPE;
        vAttr.accessLevel |= UA_ACCESSLEVELMASK_WRITE;
    }
    return UA_Server_addVariableNode(
        server, id, parentId, UA_NODEID_NUMERIC(0, UA_NS0ID_HASPROPERTY),
        UA_QUALIFIEDNAME(id.namespaceIndex, browsename),
        UA_NODEID_NUMERIC(0, UA_NS0ID_PROPERTYTYPE), vAttr, NULL, NULL);
}

UA_StatusCode
addStringProperty(UA_Server *server, char *browsename, char *displayname,
                  char *description, UA_NodeId id, UA_NodeId parentId, const char *value,
                  UA_Boolean writeable) {
    UA_VariableAttributes vAttr = UA_VariableAttributes_default;
    vAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    vAttr.valueRank = UA_VALUERANK_SCALAR;
    UA_String classVar = UA_STRING_ALLOC(value);
    UA_Variant_setScalar(&vAttr.value, &classVar, &UA_TYPES[UA_TYPES_STRING]);
    addProperty(server, browsename, displayname, description, id, parentId, vAttr,
                writeable);
    UA_String_clear(&classVar);
}

UA_StatusCode
addStringArrayProperty(UA_Server *server, char *browsename, char *displayname,
                       char *description, UA_NodeId id, UA_NodeId parentId,
                       const char **value, size_t arraySize, UA_Boolean writeable) {
    UA_VariableAttributes vAttr = UA_VariableAttributes_default;
    vAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    UA_UInt32 arrayDims = 0;
    vAttr.valueRank = UA_VALUERANK_ONE_DIMENSION;
    vAttr.arrayDimensions = &arrayDims;
    vAttr.arrayDimensionsSize = 1;
    /* Copy array values */
    UA_String* stringArray = UA_Array_new(arraySize, &UA_TYPES[UA_TYPES_STRING]);
    for(size_t i = 0; i < arraySize; i++) {
        stringArray[i] = UA_STRING((char *)value[i]);
    }
    UA_Variant_setArray(&vAttr.value, stringArray, arraySize, &UA_TYPES[UA_TYPES_STRING]);
    addProperty(server, browsename, displayname, description, id, parentId, vAttr,
                writeable);
    UA_free(stringArray);
}

UA_StatusCode
addVariableCallback(UA_Server *server, UA_NodeId id, void *read, void *write) {
    UA_ValueCallback callback;
    callback.onRead = read;
    callback.onWrite = write;
    return UA_Server_setVariableNode_valueCallback(server, id, callback);
}

void
appendStringArray(UA_Server *server, UA_NodeId id, char **values, size_t valuesSize) {
    /* Read, append and write the string array */
    UA_Variant stringArray;
    if(UA_Server_readValue(server, id, &stringArray) != UA_STATUSCODE_GOOD) {
        UA_NODEID_WRAP(
            &id, UA_LOG_ERROR(
                     UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Error appending string array for property (" UA_PRINTF_STRING_FORMAT
                     "): Couldn't reading property value.",
                     UA_PRINTF_STRING_DATA(nodeIdStr)))
        return;
    }
    if(stringArray.type != &UA_TYPES[UA_TYPES_STRING]) {
        UA_NODEID_WRAP(
            &id, UA_LOG_ERROR(
                     UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Error appending string array for property (" UA_PRINTF_STRING_FORMAT
                     "): Wrong datatype %s.",
                     UA_PRINTF_STRING_DATA(nodeIdStr), stringArray.type->typeName))
        return;
    }
    stringArray.data = UA_realloc(
        stringArray.data, (stringArray.arrayLength + valuesSize) * sizeof(UA_String));
    // Add new values
    for(size_t i = 0; i < valuesSize; i++) {
        ((UA_String *)stringArray.data)[stringArray.arrayLength + i] =
            UA_STRING(values[i]);
    }
    if(UA_Server_writeValue(server, id, stringArray) != UA_STATUSCODE_GOOD) {
        UA_NODEID_WRAP(
            &id, UA_LOG_ERROR(
                     UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Error appending string array for property (" UA_PRINTF_STRING_FORMAT
                     "): Couldn't write property value.",
                     UA_PRINTF_STRING_DATA(nodeIdStr)))
        UA_Variant_clear(&stringArray);
        return;
    }
    UA_Variant_clear(&stringArray);
}

UA_StatusCode
addFolder(UA_Server *server, char *browsename, char *displayname, char *description,
          UA_NodeId id, UA_NodeId parentId, UA_Boolean asComponent) {
    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    oAttr.description = UA_LOCALIZEDTEXT("en-US", description);
    oAttr.displayName = UA_LOCALIZEDTEXT("en-US", displayname);
    UA_StatusCode result = UA_Server_addObjectNode(
        server, id, parentId,
        UA_NODEID_NUMERIC(0, asComponent ? UA_NS0ID_HASCOMPONENT : UA_NS0ID_ORGANIZES),
        UA_QUALIFIEDNAME(id.namespaceIndex, browsename),
        UA_NODEID_NUMERIC(0, UA_NS0ID_FOLDERTYPE), oAttr, NULL, NULL);
    return result;
}

UA_StatusCode
addObjectType(UA_Server *server, char *browsename, char *displayname, char *description,
              UA_NodeId id, UA_NodeId parentId) {
    UA_ObjectTypeAttributes otAttr = UA_ObjectTypeAttributes_default;
    otAttr.description = UA_LOCALIZEDTEXT("en-US", description);
    otAttr.displayName = UA_LOCALIZEDTEXT("en-US", displayname);
    UA_StatusCode result = UA_Server_addObjectTypeNode(
        server, id, parentId, UA_NODEID_NUMERIC(0, UA_NS0ID_HASSUBTYPE),
        UA_QUALIFIEDNAME(id.namespaceIndex, browsename), otAttr, NULL, NULL);
    return result;
}

/*
 * Utility functions for browsing and writing values
 */

UA_StatusCode
getParent(UA_Server *server, const UA_NodeId *nodeId, UA_NodeId *parentId,
          UA_QualifiedName *parentBrowseName) {
    UA_StatusCode result = UA_STATUSCODE_GOOD;
    UA_BrowseDescription bd;
    bd.browseDirection = UA_BROWSEDIRECTION_INVERSE;
    bd.includeSubtypes = UA_TRUE;
    bd.nodeClassMask = UA_NODECLASS_UNSPECIFIED;
    bd.nodeId = *nodeId;
    bd.referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_HIERARCHICALREFERENCES);
    bd.resultMask = UA_BROWSERESULTMASK_BROWSENAME;
    UA_BrowseResult br = UA_Server_browse(server, 1, &bd);
    result = br.statusCode;
    if(result != UA_STATUSCODE_GOOD || br.referencesSize != 1) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Couldn't find parent: %s with referencesSize %i",
                     UA_StatusCode_name(br.statusCode), (int)br.referencesSize);
    } else {
        if(parentId)
            UA_NodeId_copy(&br.references->nodeId.nodeId, parentId);
        if(parentBrowseName)
            UA_QualifiedName_copy(&br.references->browseName, parentBrowseName);
    }
    UA_BrowseResult_clear(&br);
    return result;
}

UA_StatusCode
getChildByBrowsename(UA_Server *server, const UA_NodeId parentId,
                     const UA_QualifiedName childBrowseName, UA_NodeId *childIdOut) {
    UA_BrowsePathResult bpr =
        UA_Server_browseSimplifiedBrowsePath(server, parentId, 1, &childBrowseName);
    UA_StatusCode result = bpr.statusCode;
    if(result != UA_STATUSCODE_GOOD || bpr.targetsSize < 1) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Error browsing for %i:" UA_PRINTF_STRING_FORMAT
                     " returned: %s with targetSize %i",
                     childBrowseName.namespaceIndex,
                     UA_PRINTF_STRING_DATA(childBrowseName.name),
                     UA_StatusCode_name(result), (int)bpr.targetsSize);
        UA_BrowsePathResult_clear(&bpr);
        return result;
    }
    // Copy target nodeId
    if(childIdOut)
        result = UA_NodeId_copy(&bpr.targets[0].targetId.nodeId, childIdOut);
    UA_BrowsePathResult_clear(&bpr);
    return result;
}

UA_StatusCode
serverWriteValueByBrowsename(UA_Server *server, UA_NodeId startingNode,
                             const UA_QualifiedName browseName, UA_Variant value) {
    // Browse for nodeId
    UA_NodeId targetId = UA_NODEID_NULL;
    UA_StatusCode result =
        getChildByBrowsename(server, startingNode, browseName, &targetId);
    // write value
    if(result == UA_STATUSCODE_GOOD) {
        result = UA_Server_writeValue(server, targetId, value);
        UA_NodeId_clear(&targetId);
    }
    return result;
}

/*
 * Callback for all method calls, will log to stdout
 */
static UA_StatusCode
methodCallback(UA_Server *server, const UA_NodeId *sessionId, void *sessionHandle,
               const UA_NodeId *methodId, void *methodContext, const UA_NodeId *objectId,
               void *objectContext, size_t inputSize, const UA_Variant *input,
               size_t outputSize, UA_Variant *output) {
    /* Convert node Ids to strings. */
    UA_String methodIdStr = UA_STRING_NULL;
    UA_NodeId_print(methodId, &methodIdStr);
    UA_String objectIdStr = UA_STRING_NULL;
    UA_NodeId_print(objectId, &objectIdStr);
    UA_QualifiedName methodName;
    UA_Server_readBrowseName(server, *methodId, &methodName);
    UA_QualifiedName objectName;
    UA_Server_readBrowseName(server, *objectId, &objectName);
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                "Method %.*s for object %.*s was called:\tmethodId: %.*s,\tobjectId: "
                "%.*s,\tinputSize: %i,\toutputSize: %i",
                (int)methodName.name.length, methodName.name.data,
                (int)objectName.name.length, objectName.name.data,
                (int)methodIdStr.length, methodIdStr.data, (int)objectIdStr.length,
                objectIdStr.data, (int)inputSize, (int)outputSize);

    UA_String_clear(&methodIdStr);
    UA_String_clear(&objectIdStr);
    UA_QualifiedName_clear(&methodName);
    UA_QualifiedName_clear(&objectName);
    return UA_STATUSCODE_BADNOTIMPLEMENTED;
}

UA_StatusCode
addMethod(UA_Server *server, char *browsename, char *displayname, char *description,
          UA_NodeId id, UA_NodeId parentId, UA_MethodCallback callback) {
    UA_MethodAttributes methodAttr = UA_MethodAttributes_default;
    methodAttr.description = UA_LOCALIZEDTEXT("en-US", description);
    methodAttr.displayName = UA_LOCALIZEDTEXT("en-US", displayname);
    methodAttr.executable = true;
    methodAttr.userExecutable = true;
    UA_StatusCode result = UA_Server_addMethodNode(
        server, id, parentId, UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
        UA_QUALIFIEDNAME(id.namespaceIndex, browsename), methodAttr,
        callback ? callback : &methodCallback, 0, NULL, 0, NULL, NULL, NULL);
    return result;
}

/* Instanciate optional childs based on modelling rules.
 * Can be overwritten by CREATE_ALL_OPTIONAL_NODES;
 * The desired profile is checked via a global variable (requestedExampleProfile).
 * //TODO find better mechanism to define desired profile per node
 */
UA_Boolean
createOptionalChildCallback(UA_Server *server, const UA_NodeId *sessionId,
                            void *sessionContext, const UA_NodeId *sourceNodeId,
                            const UA_NodeId *targetParentNodeId,
                            const UA_NodeId *referenceTypeId) {
    /* Browse for modelling rules */
    UA_BrowseDescription bd;
    bd.browseDirection = UA_BROWSEDIRECTION_FORWARD;
    bd.includeSubtypes = UA_FALSE;
    bd.nodeClassMask = UA_NODECLASS_OBJECT;
    bd.nodeId = *sourceNodeId;
    bd.referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_HASMODELLINGRULE);
    bd.resultMask = UA_BROWSERESULTMASK_BROWSENAME;
    UA_BrowseResult br = UA_Server_browse(server, 0, &bd);
    if(br.statusCode != UA_STATUSCODE_GOOD) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Couldn't find modelling rules by browsing: %s",
                     UA_StatusCode_name(br.statusCode));
        UA_BrowseResult_clear(&br);
        return UA_FALSE;
    } else if(br.referencesSize <= 0) {
        return UA_FALSE;
    }

    /* Evaluate modelling rules */
    int facetValueAndId;
    FACET_GROUP facetGroupNo;
    int facetValue;
    bool childIsRequired = false;      // A modelling rule exists in NS2 that is requested
    bool childIsOptionalByCC = false;  // modelling rule "2:CCOptional"
    bool childIsOptionalByNS0 = false;  // modelling rule "0:Optional"
    for(int i = 0; i < br.referencesSize; ++i) {
        if(br.references[i].nodeId.nodeId.namespaceIndex == 0 &&
           br.references[i].nodeId.nodeId.identifier.numeric ==
               UA_NS0ID_MODELLINGRULE_OPTIONAL) {
            childIsOptionalByNS0 = true;
            continue;
        }
        /* Calculate profile values from modelling rule references */
        // ignore other modelling rules --> could be realised via special
        // modellingRuleType --> use in browserequest
        if(br.references[i].nodeId.nodeId.namespaceIndex != NS_PROFILES)
            continue;

        facetValueAndId = (br.references[i].nodeId.nodeId.identifier.numeric -
                           NS2OFFSET_CCMODELLINGRULES);
        // Check optional (2) or mandatory (1) regarding interface specification
        if(facetValueAndId == 1 || facetValueAndId == 2) {
            // break for-loop only if a node is optional and optional nodes are requested
            // otherwise check for further modelling rules
            if(facetValueAndId == 2) {
                childIsOptionalByCC = true;
            }
            continue;
        }

        // Skip further checks of modelling rules, if it is already required
        if(childIsRequired)
            continue;

        // Let the numeric magic happen: check for facet group and facet value by
        // utilizing the node id value, which is generated in profile_interface.c :
        // UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCMODELLINGRULES + (facetGroupNo + 1)
        // * NS2FACTOR_PROFILES + i)
        facetValue = facetValueAndId % NS2FACTOR_PROFILES;
        facetGroupNo = (FACET_GROUP)(facetValueAndId / NS2FACTOR_PROFILES - 1);
        // handle bitmasks
        if(facetGroupNo < 3)
            facetValue = 1 << (facetValue - 1);

        /* Check against requested profile
         * Don't instanciate child if requested profile facet value is NONE.
         * Instanciate child if facet value is UNKOWN, but child is required in a facet
         * group.
         */
        int requiredFacetValue =
            C3_Profile_toFacetValue(requestedExampleProfile, facetGroupNo);
        // handle bitmasks
        if(facetGroupNo < 3)
            childIsRequired =
                !(requiredFacetValue & 1) &&
                (requiredFacetValue == 0 || requiredFacetValue & facetValue);
        else
            childIsRequired =
                requiredFacetValue != 1 &&
                (requiredFacetValue == 0 || requiredFacetValue == facetValue);
    }
    UA_BrowseResult_clear(&br);
    // Check for optional custom rule:
    // Child is not required, if child is CCOptional but optional is not requested.
    if(childIsOptionalByCC && !requestedExampleProfile.optional)
        childIsRequired = false;

    // Check if child is optional by NS0 modelling rule and should be created
    // This is used for instance declaration in the type system
    if(childIsOptionalByNS0 && CreateAllOptionalNodes)
        childIsRequired = true;

    return childIsRequired;
}

/* Overwrite default activateSession and closeSession callbacks from AccessControl to
 * write the userid to the sessionContext as UA_String.
 * The username is needed by the CC_Operation callbacks as sender id
 *
 * Code is taken from ua_accesscontrol_default.c plugin.
 */
typedef struct {
    UA_Boolean allowAnonymous;
    size_t usernamePasswordLoginSize;
    UA_UsernamePasswordLogin *usernamePasswordLogin;
} AccessControlContext;

UA_String CC_ANONYMOUSPOLICY = UA_STRING_STATIC("open62541-anonymous-policy");
UA_String CC_USERNAMEPOLICY = UA_STRING_STATIC("open62541-username-policy");

static UA_StatusCode
activateSession_CC(UA_Server *server, UA_AccessControl *ac,
                   const UA_EndpointDescription *endpointDescription,
                   const UA_ByteString *secureChannelRemoteCertificate,
                   const UA_NodeId *sessionId,
                   const UA_ExtensionObject *userIdentityToken, void **sessionContext) {
    AccessControlContext *context = (AccessControlContext *)ac->context;

    /* The empty token is interpreted as anonymous */
    if(userIdentityToken->encoding == UA_EXTENSIONOBJECT_ENCODED_NOBODY) {
        if(!context->allowAnonymous)
            return UA_STATUSCODE_BADIDENTITYTOKENINVALID;

        /* No userdata atm */
        *sessionContext = NULL;
        return UA_STATUSCODE_GOOD;
    }

    /* Could the token be decoded? */
    if(userIdentityToken->encoding < UA_EXTENSIONOBJECT_DECODED)
        return UA_STATUSCODE_BADIDENTITYTOKENINVALID;

    /* Anonymous login */
    if(userIdentityToken->content.decoded.type ==
       &UA_TYPES[UA_TYPES_ANONYMOUSIDENTITYTOKEN]) {
        if(!context->allowAnonymous)
            return UA_STATUSCODE_BADIDENTITYTOKENINVALID;

        const UA_AnonymousIdentityToken *token =
            (UA_AnonymousIdentityToken *)userIdentityToken->content.decoded.data;

        /* Compatibility notice: Siemens OPC Scout v10 provides an empty
         * policyId. This is not compliant. For compatibility, assume that empty
         * policyId == ANONYMOUS_POLICY */
        if(token->policyId.data &&
           !UA_String_equal(&token->policyId, &CC_ANONYMOUSPOLICY))
            return UA_STATUSCODE_BADIDENTITYTOKENINVALID;

        /* No userdata atm */
        *sessionContext = NULL;
        return UA_STATUSCODE_GOOD;
    }

    /* Username and password */
    if(userIdentityToken->content.decoded.type ==
       &UA_TYPES[UA_TYPES_USERNAMEIDENTITYTOKEN]) {
        const UA_UserNameIdentityToken *userToken =
            (UA_UserNameIdentityToken *)userIdentityToken->content.decoded.data;

        if(!UA_String_equal(&userToken->policyId, &CC_USERNAMEPOLICY))
            return UA_STATUSCODE_BADIDENTITYTOKENINVALID;

        /* The userToken has been decrypted by the server before forwarding
         * it to the plugin. This information can be used here. */
        /* if(userToken->encryptionAlgorithm.length > 0) {} */

        /* Empty username and password */
        if(userToken->userName.length == 0 && userToken->password.length == 0)
            return UA_STATUSCODE_BADIDENTITYTOKENINVALID;

        /* Try to match username/pw */
        UA_Boolean match = false;
        for(size_t i = 0; i < context->usernamePasswordLoginSize; i++) {
            if(UA_String_equal(&userToken->userName,
                               &context->usernamePasswordLogin[i].username) &&
               UA_String_equal(&userToken->password,
                               &context->usernamePasswordLogin[i].password)) {
                match = true;
                /* CC: Username */
                UA_String *username = UA_String_new();
                UA_String_copy(&userToken->userName, username);
                *sessionContext = username;
                break;
            }
        }
        if(!match)
            return UA_STATUSCODE_BADUSERACCESSDENIED;

        return UA_STATUSCODE_GOOD;
    }

    /* Unsupported token type */
    return UA_STATUSCODE_BADIDENTITYTOKENINVALID;
}

static void
closeSession_CC(UA_Server *server, UA_AccessControl *ac, const UA_NodeId *sessionId,
                void *sessionContext) {
    /* no context to clean up ?*/
    if(sessionContext != NULL)
        UA_String_delete((UA_String *)sessionContext);
}

void
setServerConfig(UA_Server *server, const char *applicationName,
                const char *applicationUri, UA_UInt16 port, UA_LogLevel loglevel) {
    UA_ServerConfig *config = UA_Server_getConfig(server);
    config->logger = UA_Log_Stdout_withLevel(loglevel);
    UA_ServerConfig_setMinimal(config, port, NULL);

    /* Change application description */
    UA_ApplicationDescription_clear(&config->applicationDescription);
    config->applicationDescription.applicationName =
        UA_LOCALIZEDTEXT_ALLOC("en-US", applicationName);

    config->applicationDescription.applicationUri.length =
        snprintf(NULL, 0, "opc.tcp://localhost:%d/%s", port, applicationUri) + 1;
    config->applicationDescription.applicationUri.data =
        malloc(sizeof(char *) * config->applicationDescription.applicationUri.length);
    snprintf(config->applicationDescription.applicationUri.data,
             config->applicationDescription.applicationUri.length,
             "opc.tcp://localhost:%d/%s", port, applicationUri);

    UA_ApplicationDescription_clear(&config->endpoints->server);
    UA_ApplicationDescription_copy(&config->applicationDescription,
                                   &config->endpoints->server);

    /* Add activateSession and closeSession callbacks to add usernames to sessionCtxt */
    config->accessControl.activateSession = activateSession_CC;
    config->accessControl.closeSession = closeSession_CC;

    /* Handle optional child generation */
    config->nodeLifecycle.createOptionalChild = createOptionalChildCallback;
}
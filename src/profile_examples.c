#include <profile_examples.h>

#include <cc_exampleOpMode.h>
#include <cc_instanciation.h>

/* Creates an Instance of a control component with the defined profile */
void
createControlComponentExample(UA_Server *server, C3_Profile profile, EXAMPLE example,
                              UA_UInt16 componentNo) {
    if(componentNo == 0) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Error creating new dummy control component. 0 not allowed as "
                     "component number.");
        return;
    }

    /* Create custom names for new control component */
    char *profileString = C3_Profile_print(profile);
    char name[1024];
    sprintf(name, "ControlComponent %i %s", componentNo, profileString);
    char browsename[1024];
    sprintf(browsename, "CC_%i", componentNo);
    char description[1024];
    sprintf(description, "A dummy for a %s control component with profile %s",
            EXAMPLESTRING_LOWER[example], profileString);
    char typeName[1024];
    sprintf(typeName, "%s%sType", EXAMPLESTRING_CASE[example], C3_SI_TOSTRING(profile.SI));
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                "Creating %i. dummy control component (ns=%i;i=%i) with browsename '%s' "
                "and profile '%s'",
                componentNo, NS_APPLICATION, componentNo, browsename, profileString);
    free(profileString);

    /* Create control component and fill in example informations */
    C3_CC *cc = C3_CC_new();
    C3_Info info;
    info.id = browsename;
    info.name = name;
    info.description = description;
    info.profile = profile;
    info.type = typeName;
    C3_CC_setInfo(cc, &info);

    // add type (example) specific operation modes
    // TODO Add a CCType concept to c3 lib and instanciate the type instead
    switch(example) {
        case EXAMPLE_NONE:
            cc_exampleOpMode_add(cc, "DEFAULT", EXAMPLE_NONE_DEFAULT_WORKSTATES, 4, 3);
            break;
        case EXAMPLE_PUMP:
            cc_exampleOpMode_add(cc, "Pump", EXAMPLE_PUMP_PUMP_WORKSTATES, 7, 5);
            break;
        case EXAMPLE_PICKPLACE:
            cc_exampleOpMode_add(cc, "PickPlace", EXAMPLE_PICKPLACE_PICKPLACE_WORKSTATES,
                                 7, 5);
            cc_exampleOpMode_add(cc, "Grasp", EXAMPLE_PICKPLACE_GRASP_WORKSTATES, 5, 2);
            cc_exampleOpMode_add(cc, "Open", EXAMPLE_PICKPLACE_OPEN_WORKSTATES, 4, 2);
            cc_exampleOpMode_add(cc, "PickPlace", EXAMPLE_PICKPLACE_CLOSE_WORKSTATES, 4,
                                 2);
            break;
    }

    /* Calculate type id */
    UA_NodeId typeId = UA_NODEID_NUMERIC(
        NS_PROFILES, NS2OFFSET_CCOBJECTYPES + NS2FACTOR_PROFILES * profile.SI);
    if(example != EXAMPLE_NONE) {
        typeId = UA_NODEID_NUMERIC(NS_DSTYPES, NS3OFFSET_DUMMYCCTYPES +
                                                   NS2FACTOR_PROFILES * profile.SI +
                                                   NS3FACTOR_EXAMPLES * example);
    }
    /* Create dummy type first
     * Handles SI combinations --> creates a new type on the fly, if not already
     * present, that has multiple HasInterface references depending on the SI value.
     */
    // TODO check if node exists beforehand to avoid AddNodes error log message
    createControlComponentType(server, cc, typeId);

    createControlComponent(server, cc, typeId,
                           UA_NODEID_NUMERIC(NS_APPLICATION, componentNo));
}

void
createControlComponentsForAllProfiles(UA_Server *server, int example,
                                      bool createOptional) {
    /* Create all defined profile and example combinations */
    for(UA_UInt16 componentNo = 1; componentNo <= C3_PROFILES_COUNT; componentNo++) {
        C3_Profile p = C3_PROFILES[componentNo - 1];
        p.optional = p.optional || createOptional;
        createControlComponentExample(server, C3_PROFILES[componentNo - 1], example,
                                      componentNo);
    }
}
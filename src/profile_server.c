/*
 ============================================================================
 Name        : profile_server.c
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Service interface profile reference implementation for
               BaSys 4.2 control components. Creates an OPC UA Server with
               dummy control components with different profiles.
 ============================================================================
 */

#include <profile_examples.h>
#include <profile_interfaces.h>
#include <profile_utilities.h>

#include <cc_connector.h>
#include <cc_instanciation.h>

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

UA_Boolean running = true;
static void
stopHandler(int sig) {
    running = false;
}

/******************************************************************************
 * Main function
 * Creates a simple server, with service interface types and example instances
 * as dummies.
 ******************************************************************************/

static void
usage(void) {
    // TODO add possibility to read information from config
    printf("Usage: profile_server");
    printf(" [--profile <name>|<csv>]");
    printf(" [--port <port>]");
    printf(" [--optional]");
    printf(" [--help]\n");
}
static void
usageError(const char *errorMsg) {
    fprintf(stderr, "Error: %s\n", errorMsg);
    usage();
}

static int
parseCLI(int argc, char **argv, C3_Profile *profile, UA_UInt16 *port,
         bool *createOptional) {
    for(int argpos = 1; argpos < argc; argpos++) {
        if(strcmp(argv[argpos], "--help") == 0 || strcmp(argv[argpos], "-h") == 0) {
            usage();
            printf(
                "\n\tExample: profile_server --profile BASYS --port 16664 --optional\n");
            printf("\tProfile parameter:  Available profiles are BASYSDEMO, BASYS, "
                   "PACKML, FULL, "
                   "FULLOPTIONAL.\n\t\tOr custom profiles in the format "
                   "'SI,OC,EM,ES,OM,optional', "
                   "e.g. '4,6,6,2,2,0' for BASYS profile.\n");
            return EXIT_FAILURE;
        }
        // parse server url
        if(strcmp(argv[argpos], "--port") == 0) {
            argpos++;
            if(argpos >= argc) {
                usageError("parameter --port given, but no port specified!");
                return EXIT_FAILURE;
            }
            *port = strtol(argv[argpos], NULL, 10);
            continue;
        }
        if(strcmp(argv[argpos], "--profile") == 0 || strcmp(argv[argpos], "-p") == 0) {
            argpos++;
            if(argpos >= argc) {
                usageError("parameter --profile given, but no profile specified!");
                return EXIT_FAILURE;
            }
            *profile = C3_Profile_parse(argv[argpos]);
            if(C3_Profile_isEqual(*profile, C3_PROFILE_NULL)) {
                usageError("parameter --profile given, but no valid profile specified! "
                           "Available profiles are BASYSDEMO, BASYS, PACKML, FULL, "
                           "FULLOPTIONAL. Or "
                           "custom profiles in the format 'SI,OC,EM,ES,OM,optional', "
                           "e.g. '4,6,6,2,2,0' for BASYS profile.");
                return EXIT_FAILURE;
            }
            continue;
        }
        // parse create optional nodes
        if(strcmp(argv[argpos], "--optional") == 0 || strcmp(argv[argpos], "-o") == 0) {
            *createOptional = true;
            continue;
        }
        // TODO add option to read CreateAllOptionalNodes?
        // TODO add option to specify example
        // Unknown option
        usageError("unknown command line option.");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int
main(int argc, char **argv) {
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

    /* Parse command line arguments */
    // TODO use regex or CLI library instead: http://tclap.sf.net/ or
    // http://optionparser.sourceforge.net/index.html
    C3_Profile profile = C3_PROFILE_UNKNOWN;
    UA_UInt16 port = 4840;
    bool createOptional = false;
    if(parseCLI(argc, argv, &profile, &port, &createOptional) != EXIT_SUCCESS)
        return EXIT_FAILURE;

    /* Create an empty server with default settings */
    UA_Server *server = UA_Server_new();
    /* Customize server name and ns1 name */
    // TODO make loglevel a cli parameter
    setServerConfig(server, "BaSys42 CC Profile Server", "profile_server", port,
                    UA_LOGLEVEL_INFO);

    // Create types and parent folder for dummy CCs
    createControlComponentEnvironment(server, NS_PROFILES_URI, "myCompany/consortium");

    /* Populate server with some dummy control components */
    if(C3_Profile_isUnknown(profile)) {
        // Profile not specified: try to create all variations
        // TODO get example from cli
        createControlComponentsForAllProfiles(server, EXAMPLE_NONE, createOptional);
    } else {
        // Profile specified, but no example specified: create cc with specified profile
        // for all examples.
        // Overwrite optional if specified
        profile.optional = profile.optional || createOptional;
        for(int example = EXAMPLE_NONE; example <= EXAMPLE_PICKPLACE; example++) {
            createControlComponentExample(server, profile, example, example + 1);
        }
    }

    /* Run the control components */
    void *cc_executionLoopContext = cc_executionLoopContext_new(server);
    UA_UInt64 cc_executionLoopId = 1;
    UA_Server_addRepeatedCallback(server, cc_executionLoop, cc_executionLoopContext, 100,
                                  &cc_executionLoopId);

    /* Run the server */
    UA_StatusCode retval = UA_Server_run(server, &running);

    /* Free ressources */
    UA_Server_removeCallback(server, cc_executionLoopId);
    cc_executionLoopContext_delete(cc_executionLoopContext);
    clearControlComponentEnvironment(server);
    UA_Server_delete(server);
    return retval == UA_STATUSCODE_GOOD ? EXIT_SUCCESS : EXIT_FAILURE;
}
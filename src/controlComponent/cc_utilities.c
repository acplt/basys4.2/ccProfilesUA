#include <cc_utilities.h>
char *
uaStringToCharArray(const UA_String *uaStr) {
    if(uaStr != NULL) {
        char *charArray = malloc(sizeof(char) * (uaStr->length + 1));
        if(charArray != NULL) {
            if(strncpy(charArray, uaStr->data, uaStr->length) != NULL) {
                charArray[uaStr->length] = '\0';
                return charArray;
            }
        }
    }
    return NULL;
}

UA_StatusCode
c3ResultToUAStatusCode(C3_Result result) {
    if(result >= 0) {
        return UA_STATUSCODE_GOOD;
    }
    switch(result) {
        case C3_RESULT_BAD_ACCESS:
            return UA_STATUSCODE_BADUSERACCESSDENIED;
        case C3_RESULT_BAD_NOTIMPLEMENTED:
            return UA_STATUSCODE_BADNOTIMPLEMENTED;
        case C3_RESULT_BAD_MEMORY:
            return UA_STATUSCODE_BADOUTOFMEMORY;
        case C3_RESULT_BAD_OPMODECHANGE:
        case C3_RESULT_BAD_OPMODEUNKNOWN:
        case C3_RESULT_BAD_CMDSYNTAX:
        case C3_RESULT_BAD_CMD:
        case C3_RESULT_BAD_STATE:
        case C3_RESULT_BAD_ORDER:
        case C3_RESULT_BAD:
        default:
            return UA_STATUSCODE_BADINTERNALERROR;
    }
    return UA_STATUSCODE_BADUNEXPECTEDERROR;
}
#include <cc_exampleOpMode.h>

typedef struct OpModeContext {
    size_t workState;
    C3_ES_State exState;
    UA_DateTime timer;
    const char **workStates;
    size_t workStatesSize;
    int stateDuration;
} OpModeContext;

static bool
opMode_init(C3_CC *cc, C3_OP_OpMode *opMode) {
    // printf("Initialising opMode %s for %s.\n", opMode->name, C3_CC_getId(cc));
    OpModeContext *context = (OpModeContext *)opMode->context;
    context->workState = -2;
    context->timer = 0;
    return true;
}

static void
opMode_clear(C3_CC *cc, struct C3_OP_OpMode *opMode) {
    // printf("Clearing opMode %s from %s.\n", opMode->name, C3_CC_getId(cc));
    free(opMode->context);
}

static bool
opMode_select(C3_CC *cc, struct C3_OP_OpMode *opMode) {
    // printf("Selecting opMode %s for %s.\n", opMode->name, C3_CC_getId(cc));
    const C3_Status status = C3_CC_getStatus(cc);
    if(C3_ES_ISACTIVESTATE[status.executionState]) {
        // Don't allow operation mode changes in active states
        // This could be even more permissive (e.g. only change operatin mode in STOPPED)
        return false;
    }

    OpModeContext *context = (OpModeContext *)opMode->context;
    context->exState = -1;
    context->workState = -1;
    context->timer = 0;
    return true;
}

static bool
opMode_deselect(C3_CC *cc, struct C3_OP_OpMode *opMode) {
    // printf("Deselecting opMode %s for %s.\n", opMode->name, C3_CC_getId(cc));
    const C3_Status status = C3_CC_getStatus(cc);
    if(C3_ES_ISACTIVESTATE[status.executionState]) {
        // Don't allow operation mode changes in active states
        // This could be even more permissive (e.g. only change operatin mode in STOPPED)
        return false;
    }
    return true;
}

static void
opMode_execute(C3_CC *cc, struct C3_OP_OpMode *opMode, C3_IO io, C3_ES_Order *order,
               const char **workingState, const char **errorState) {
    OpModeContext *context = (OpModeContext *)opMode->context;
    C3_Status status = C3_CC_getStatus(cc);
    int oldWorkState = context->workState;

    // Handle an execution state change
    if(status.executionState != context->exState) {
        // Reset state timer and save current state
        context->timer = UA_DateTime_now() + (context->stateDuration * UA_DATETIME_SEC);
        context->exState = status.executionState;

        // Set workstate (and do stuff in real a CC, e.g. read and write IOs, calculate
        // ...)
        if(status.executionState == C3_ES_STATE_STARTING ||
           status.executionState == C3_ES_STATE_UNHOLDING ||
           status.executionState == C3_ES_STATE_UNSUSPENDING) {
            context->workState = 1;
            // Prepare EXECUTE state
        } else if(status.executionState == C3_ES_STATE_EXECUTE) {
            // Execute
            context->workState = 2;
        } else if(status.executionState == C3_ES_STATE_COMPLETING ||
                  status.executionState == C3_ES_STATE_STOPPING ||
                  status.executionState == C3_ES_STATE_ABORTING ||
                  status.executionState == C3_ES_STATE_HOLDING ||
                  status.executionState == C3_ES_STATE_SUSPENDING) {
            // Stop the execution
            context->workState = context->workStatesSize - 1;
        } else if(status.executionState == C3_ES_STATE_RESETTING ||
                  status.executionState == C3_ES_STATE_CLEARING) {
            context->workState = 0;
        } else {
            // Passive execution states => no SC: wait for command
            context->workState = 0;
            context->timer = 0;
        }
    }

    // Check if the time is up and change work or execution state
    if((context->timer > 0) && (UA_DateTime_now() > context->timer)) {
        if((context->workState) > 1 &&
           (context->workState < (context->workStatesSize - 2))) {
            context->timer += (context->stateDuration * UA_DATETIME_SEC);
            context->workState++;
        } else {
            *order = C3_ES_ORDER_SC;
        }
    }

    // Output new working state
    if(oldWorkState != context->workState) {
        *workingState = context->workStates[context->workState];
    }
}

bool
cc_exampleOpMode_add(C3_CC *cc, char *name, const char **workStates,
                     size_t workStatesSize, int stateDuration) {
    OpModeContext *context = malloc(sizeof(OpModeContext));
    if(context == NULL)
        return false;
    context->workStates = workStates;
    context->workStatesSize = workStatesSize;
    context->stateDuration = stateDuration;

    C3_OP_OpMode opMode;
    opMode.context = context;
    opMode.name = name;
    opMode.init = opMode_init;
    opMode.execute = opMode_execute;
    opMode.clear = opMode_clear;
    opMode.select = opMode_select;
    opMode.deselect = opMode_deselect;
    if(C3_CC_addOperationMode(cc, &opMode) < 0) {
        free(context);
    }
    return true;
}

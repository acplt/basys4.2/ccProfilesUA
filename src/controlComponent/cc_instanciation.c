#include <profile_interfaces.h>
#ifdef CCPUA_ENABLE_PACKML
#include <profile_packML.h>
#endif

#include <cc_connector.h>
#include <cc_instanciation.h>

/******************************************************************************
 * Environment creation
 * Create the control component interfaces and types as well as a folder
 * for dummy CCs.
 ******************************************************************************/

void
createControlComponentEnvironment(UA_Server *server,
                                  const char *profilesNamespaceUri,
                                  const char *typeNamespaceUri) {
    // Add namespaces for types
    NS_PROFILES = UA_Server_addNamespace(server, profilesNamespaceUri);
    NS_DSTYPES = UA_Server_addNamespace(server, typeNamespaceUri);

    /* Create an object types (as interfaces) for control components with different
     * profiles */
    createControlComponentInterface(server);

    /* Create a folder for the dummy control component instances */
    addFolder(server, "CCs", "ControlComponents",
              "A folder for control components. Control component instances may be "
              "located anywhere in the OPC UA hierarchy.",
              ID_DUMMYCCFOLDER, UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), UA_FALSE);
}

/******************************************************************************
 * DummyType creation
 * Create some example types for different profiles
 ******************************************************************************/

void
createControlComponentType(UA_Server *server, C3_CC *cc, UA_NodeId id) {
    const C3_Info info = C3_CC_getInfo(cc);
    C3_SI_FACET si = info.profile.SI;
    /* Create custom names for new control component */
    char description[1024];
    sprintf(description,
            "A control component type with SI facet %i:%s.", si,
            C3_SI_TOSTRING(si));
    /* Create an object type node for the control component type */
    UA_StatusCode result = addObjectType(server, info.type, info.type, description, id,
                                         UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE));
    if(result == UA_STATUSCODE_BADNODEIDEXISTS) {
        UA_LOG_DEBUG(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Couldn't create control component type (ns=%i;i=%i). NodeId "
                     "already in use.",
                     id.namespaceIndex, id.identifier.numeric);
        return;
    } else if(result != UA_STATUSCODE_GOOD) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Couldn't create control component type (ns=%i;i=%i): %s.",
                     id.namespaceIndex, id.identifier.numeric,
                     UA_StatusCode_name(result));
        return;
    }
    UA_LOG_INFO(
        UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
        "Created control component type (ns=%i;i=%i) with SI facet %i:%s and "
        "browsename '%s'.",
        id.namespaceIndex, id.identifier.numeric, si, C3_SI_TOSTRING(si), info.type);

    /* Add the service interface */
    // TODO interface inheritance not working or not intended?
    markInterface(server, id, C3_SI_UNKNOWN);
    // mark single values of si bitwise
    for(int i = 1; i < C3_SI_FACET_COUNT - 1; ++i) {
        if(si & (1 << i)) {
            markInterface(server, id, (1 << i));
        }
    }

    // Get operation modes from CC
    char **operationModeNames = NULL;
    size_t operationModeCount = C3_CC_getOperationModeNames(cc, &operationModeNames);

    UA_UInt32 idOffset = id.identifier.numeric * NS2FACTOR_PROFILES;
    /* Add SI facet specific objects */
    if(si & C3_SI_CMD) {
        UA_NodeId variableId = UA_NODEID_NUMERIC(NS_DSTYPES, idOffset + 1 * NS2FACTOR_FOLDER);
        // TODO append orders depending on profile (e.g. MANUAL or SUSPEND only if it is
        // available for the specified profile) --> Alternatively add a callback, that
        // evaluates context->info.profile and getOperationModes instead
        addStringArrayProperty(
            server, "CMDOLIST", "CommandOrderList", "Array of supportet orders.",
            variableId, id,
            (const char *[16]){"START", "STOP", "RESET", "HOLD", "UNHOLD", "SUSPEND",
                               "UNSUSPEND", "ABORT", "CLEAR", "AUTO", "SEMIAUTO",
                               "MANUAL", "SIMULATE", "OCCUPY", "PRIO", "FREE"},
            16, false);
        markMandatory(server, variableId, false, true);
        if(info.profile.OM != C3_OM_NONE && operationModeCount > 0) {
            // overwrite CMDOLIST values
            appendStringArray(server, variableId, operationModeNames, operationModeCount);
        }

        // overwrite CMDOPREF values
        variableId.identifier.numeric++;
        addStringArrayProperty(server, "CMDOPREF", "CommandOrderParameterReference",
                               "Mapping of necessary Parameters for every order.",
                               variableId, id,
                               (const char *[16]){"", "", "", "", "", "", "", "", "", "",
                                                  "", "", "", "", "", ""},
                               16, false);
        markMandatory(server, variableId, false, true);
        // TODO add some inputs / outputs to methods and CMDOPREF values
        if(info.profile.OM != C3_OM_NONE && operationModeCount > 0) {
            // overwrite CMDOPREF values
            char **cmdOrderParameterReference = malloc(sizeof(char*) * operationModeCount);
            for(size_t i = 0; i < operationModeCount; i++) {
                cmdOrderParameterReference[i] = "";
            }
            variableId.identifier.numeric++;
            appendStringArray(server, variableId, cmdOrderParameterReference,
                            operationModeCount);
            free(cmdOrderParameterReference);
        }
    }
    if(si & C3_SI_OPERATIONS) {
        if(info.profile.OM != C3_OM_NONE && operationModeCount > 0) {
            UA_NodeId idFolder = UA_NODEID_NUMERIC(NS_DSTYPES, idOffset + 2 * NS2FACTOR_FOLDER);
            // Overwrite OPERATIONS Folder
            UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
            oAttr.description = UA_LOCALIZEDTEXT("en-US", "A folder for all operations of this control component.");
            oAttr.displayName = UA_LOCALIZEDTEXT("en-US", "Operations");
            UA_StatusCode result = UA_Server_addObjectNode(server, idFolder, id,
                UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
                UA_QUALIFIEDNAME(NS_PROFILES, "OPERATIONS"), //Browsename has to be from NS_PROFILES!
                UA_NODEID_NUMERIC(0, UA_NS0ID_FOLDERTYPE), oAttr, NULL, NULL);
            markMandatory(server, idFolder, true, false);

            UA_NodeId idMethod = UA_NODEID_NUMERIC(NS_DSTYPES, idFolder.identifier.numeric + 1);
            for(size_t i = 0; i < operationModeCount; i++) {
                addMethod(server, operationModeNames[i], operationModeNames[i],
                        "Select operation mode with this name.", idMethod, idFolder,
                        cc_operationCallback);
                markMandatory(server, idMethod, true, false);
                idMethod.identifier.numeric++;
            }
        }
    }

    for(size_t i = 0; i < operationModeCount; i++) {
        free(operationModeNames[i]);
    }
    free(operationModeNames);
}

/******************************************************************************
 * Instance creation
 * Create dummy instances from the profile interface types directly
 * or from DummyTypes.
 ******************************************************************************/

void
createControlComponent(UA_Server *server, C3_CC *cc, UA_NodeId typeId, UA_NodeId id) {
    const C3_Info info = C3_CC_getInfo(cc);

    /* Create an object node for the dummy control component from profile specific
     * DummyCCType */
    requestedExampleProfile =
        info.profile;  // is evaluated in createOptionalChildCallback

    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    oAttr.description = UA_LOCALIZEDTEXT("en-US", info.description);
    oAttr.displayName = UA_LOCALIZEDTEXT("en-US", info.name);
    UA_Server_addObjectNode(
        server, id, ID_DUMMYCCFOLDER, UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
        UA_QUALIFIEDNAME(NS_APPLICATION, info.id), typeId, oAttr, NULL, NULL);
    UA_Server_setNodeContext(server, id, (void *)cc);

    /* Configure profile specific instance values */
#ifdef CCPUA_ENABLE_PACKML
    if(info.profile.SI & C3_SI_PACKML)
        configurePackMLBaseObject(server, id, info.profile);
#endif
    /* Set nodecontext for all PROFILE children for faster variable access in
     * cc_VariableCallback_ callbacks */
    UA_QualifiedName profileBrowseName = UA_QUALIFIEDNAME(NS_PROFILES, "C3_Profile");
    UA_BrowsePathResult profileBpr =
        UA_Server_browseSimplifiedBrowsePath(server, id, 1, &profileBrowseName);
    // Check if C3_Profile is present
    if(profileBpr.statusCode == UA_STATUSCODE_GOOD && profileBpr.targetsSize > 0) {
        UA_Server_setNodeContext(server, profileBpr.targets[0].targetId.nodeId,
                                 (void *)cc);

        UA_BrowseDescription bd;
        bd.browseDirection = UA_BROWSEDIRECTION_FORWARD;
        bd.includeSubtypes = UA_FALSE;
        bd.nodeClassMask = UA_NODECLASS_VARIABLE;
        bd.nodeId = profileBpr.targets[0].targetId.nodeId;
        bd.referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_HASPROPERTY);
        bd.resultMask = UA_BROWSERESULTMASK_TARGETINFO;
        UA_BrowseResult br = UA_Server_browse(server, 0, &bd);
        if(br.statusCode == UA_STATUSCODE_GOOD) {
            for(size_t i = 0; i < br.referencesSize; i++) {
                UA_Server_setNodeContext(server, br.references[i].nodeId.nodeId,
                                         (void *)cc);
            }
        }
        UA_BrowseResult_clear(&br);
    }
    UA_BrowsePathResult_clear(&profileBpr);

    /* Set nodecontext for all STATUS children for faster variable access in
     * cc_VariableCallback_ callbacks */
    UA_NodeId statusId;
    if(getChildByBrowsename(server, id, UA_QUALIFIEDNAME(NS_PROFILES, "STATUS"),
                            &statusId) == UA_STATUSCODE_GOOD) {
        UA_BrowseDescription bd;
        bd.browseDirection = UA_BROWSEDIRECTION_FORWARD;
        bd.includeSubtypes = UA_FALSE;
        bd.nodeClassMask = UA_NODECLASS_VARIABLE;
        bd.nodeId = statusId;
        bd.referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_HASPROPERTY);
        bd.resultMask = UA_BROWSERESULTMASK_TARGETINFO;
        UA_BrowseResult br = UA_Server_browse(server, 0, &bd);
        if(br.statusCode == UA_STATUSCODE_GOOD) {
            for(size_t i = 0; i < br.referencesSize; i++) {
                UA_Server_setNodeContext(server, br.references[i].nodeId.nodeId,
                                         (void *)cc);
            }
        }
        UA_BrowseResult_clear(&br);
    }
    // Set nodecontext for CMDLAST variable
    UA_QualifiedName cmdLastBrowseName = UA_QUALIFIEDNAME(NS_PROFILES, "CMDLAST");
    UA_BrowsePathResult cmdLastBpr =
        UA_Server_browseSimplifiedBrowsePath(server, id, 1, &cmdLastBrowseName);
    if(cmdLastBpr.statusCode == UA_STATUSCODE_GOOD || cmdLastBpr.targetsSize == 1) {
        UA_Server_setNodeContext(server, cmdLastBpr.targets[0].targetId.nodeId,
                                 (void *)cc);
    }
    UA_BrowsePathResult_clear(&cmdLastBpr);
}

static void
clearControlComponents(UA_Server *server) {
    UA_BrowseDescription bd;
    bd.browseDirection = UA_BROWSEDIRECTION_FORWARD;
    bd.includeSubtypes = UA_FALSE;
    bd.nodeClassMask = UA_NODECLASS_OBJECT;
    bd.nodeId = ID_DUMMYCCFOLDER;
    bd.referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    bd.resultMask = UA_BROWSERESULTMASK_TARGETINFO;
    UA_BrowseResult br = UA_Server_browse(server, 0, &bd);
    if(br.statusCode == UA_STATUSCODE_GOOD) {
        for(size_t i = 0; i < br.referencesSize; i++) {
            C3_CC *cc = NULL;
            if(UA_Server_getNodeContext(server, br.references[i].nodeId.nodeId,
                                        (void **)&cc) == UA_STATUSCODE_GOOD) {
                C3_CC_clear(cc);
                free(cc);
                UA_Server_setNodeContext(server, br.references[i].nodeId.nodeId, NULL);
            }
        }
    }
    UA_BrowseResult_clear(&br);
}

void
clearControlComponentEnvironment(UA_Server *server) {
#ifdef CCPUA_ENABLE_PACKML
    clearProfilePackML(server);
#endif
    clearControlComponents(server);
}
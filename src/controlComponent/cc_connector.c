#include <cc_connector.h>

UA_StatusCode
cc_executeCommand(UA_Server *server, const UA_NodeId *ccId, const UA_String *sender,
                  const UA_String *order, const UA_String *parameter) {
    C3_CC *cc = NULL;
    if(UA_Server_getNodeContext(server, *ccId, (void **)&cc) == UA_STATUSCODE_GOOD) {
        C3_Command cmd;
        cmd.type = C3_CMDTYPE_UNKNOWN;
        cmd.order.str = uaStringToCharArray(order);
        cmd.sender = sender ? uaStringToCharArray(sender) : NULL;
        cmd.paramCount = 0;
        cmd.paramKeys = NULL;
        cmd.paramVals = NULL;
        C3_Result result = C3_CC_Cmd(cc, &cmd);
        if(result < 0) {
            UA_NODEID_WRAP(
                ccId,
                UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                               "Component " UA_PRINTF_STRING_FORMAT
                               " returned error %s on order: " UA_PRINTF_STRING_FORMAT,
                               UA_PRINTF_STRING_DATA(nodeIdStr),
                               C3_RESULT_TOSTRING(result), UA_PRINTF_STRING_DATA(*order)))
        } else {
            UA_NODEID_WRAP(ccId, UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                                             "Component " UA_PRINTF_STRING_FORMAT
                                             " evaluated order: " UA_PRINTF_STRING_FORMAT,
                                             UA_PRINTF_STRING_DATA(nodeIdStr),
                                             UA_PRINTF_STRING_DATA(*order)))
        }
        C3_Command_clear(&cmd);
        return c3ResultToUAStatusCode(result);
    } else {
        UA_NODEID_WRAP(
            ccId, UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                                 "Couldn't get nodecontext for " UA_PRINTF_STRING_FORMAT,
                                 UA_PRINTF_STRING_DATA(nodeIdStr)))
    }
    return UA_STATUSCODE_BADINTERNALERROR;
}

void
cc_commandInputCallback(UA_Server *server, const UA_NodeId *sessionId,
                        void *sessionContext, const UA_NodeId *nodeId, void *nodeContext,
                        const UA_NumericRange *range, const UA_DataValue *data) {
    UA_String cmd = *((UA_String *)data->value.data);

    /* Get control componentn and evaluate cmd*/
    UA_NodeId ccId = UA_NODEID_NULL;
    if(getParent(server, nodeId, &ccId, NULL) == UA_STATUSCODE_GOOD) {
        C3_CC *cc = NULL;
        if(UA_Server_getNodeContext(server, ccId, (void **)&cc) == UA_STATUSCODE_GOOD) {
            char *cmdChar = uaStringToCharArray(&cmd);
            C3_Result result = C3_CC_CmdString(cc, cmdChar);
            if(result < 0) {
                UA_NODEID_WRAP(&ccId,
                               UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                                              "Component " UA_PRINTF_STRING_FORMAT
                                              " returned error %s on command: %s",
                                              UA_PRINTF_STRING_DATA(nodeIdStr),
                                              C3_RESULT_TOSTRING(result), cmdChar))
            } else {
                UA_NODEID_WRAP(&ccId,
                               UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                                           "Component " UA_PRINTF_STRING_FORMAT
                                           " evaluated cmd: %s",
                                           UA_PRINTF_STRING_DATA(nodeIdStr), cmdChar))
            }
            free(cmdChar);
        } else {
            UA_NODEID_WRAP(
                &ccId,
                UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                               "Couldn't get nodecontext for " UA_PRINTF_STRING_FORMAT,
                               UA_PRINTF_STRING_DATA(nodeIdStr)))
        }
    } else {
        UA_NODEID_WRAP(nodeId,
                       UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                                      "Couldn't get parent for " UA_PRINTF_STRING_FORMAT,
                                      UA_PRINTF_STRING_DATA(nodeIdStr)))
    }

    /* Free resources */
    UA_NodeId_clear(&ccId);
}

UA_StatusCode
cc_operationCallback(UA_Server *server, const UA_NodeId *sessionId, void *sessionHandle,
                     const UA_NodeId *methodId, void *methodContext,
                     const UA_NodeId *objectId, void *objectContext, size_t inputSize,
                     const UA_Variant *input, size_t outputSize, UA_Variant *output) {

    /* Get order id from method name */
    UA_QualifiedName *order = UA_QualifiedName_new();
    UA_Server_readBrowseName(server, *methodId, order);

    /* Get control component node id and evaluate the order*/
    UA_NodeId ccId = UA_NODEID_NULL;
    UA_StatusCode result = getParent(server, objectId, &ccId, NULL);
    if(result == UA_STATUSCODE_GOOD) {
        result = cc_executeCommand(server, &ccId, (UA_String *)sessionHandle,
                                   &order->name, NULL);
        UA_NodeId_clear(&ccId);
    } else {
        UA_NODEID_WRAP(objectId,
                       UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                                      "Couldn't get parent for " UA_PRINTF_STRING_FORMAT,
                                      UA_PRINTF_STRING_DATA(nodeIdStr)))
    }

    /* Free common ressources */
    UA_QualifiedName_delete(order);
    return result;
}

#define DEFINE_CCVARIABLECALLBACK_UA(variableName, expression)                 \
    void cc_VariableCallback_##variableName(                                             \
        UA_Server *server, const UA_NodeId *sessionId, void *sessionContext,             \
        const UA_NodeId *nodeid, void *nodeContext, const UA_NumericRange *range,        \
        const UA_DataValue *data) {                                                      \
        if(nodeContext == NULL)                                                          \
            return;                                                                      \
        const C3_Status status = C3_CC_getStatus((C3_CC *)nodeContext);                  \
        const C3_Info info = C3_CC_getInfo((C3_CC *)nodeContext);                        \
        UA_String valueString = expression;                            \
        UA_Variant value;                                                                \
        UA_Variant_setScalar(&value, &valueString, &UA_TYPES[UA_TYPES_STRING]);          \
        UA_Server_writeValue(server, *nodeid, value);                                    \
    }
#define DEFINE_CCVARIABLECALLBACK(variableName, expression)                              \
    DEFINE_CCVARIABLECALLBACK_UA(variableName, UA_STRING_ALLOC(expression))

// Status variables
DEFINE_CCVARIABLECALLBACK(CMDLAST, status.lastCMD)
DEFINE_CCVARIABLECALLBACK(OCCST, C3_OC_STATESTRING[status.occupationState])
DEFINE_CCVARIABLECALLBACK(OCCUPIER, status.occupier)
DEFINE_CCVARIABLECALLBACK(OCCLAST, status.lastOccupier)
DEFINE_CCVARIABLECALLBACK(EXMODE, C3_EM_STATESTRING[status.executionMode])
DEFINE_CCVARIABLECALLBACK(EXST, C3_ES_STATESTRING[status.executionState])
DEFINE_CCVARIABLECALLBACK(OPMODE, status.operationMode)
DEFINE_CCVARIABLECALLBACK(WORKST, status.workingState)
DEFINE_CCVARIABLECALLBACK(ER, status.error)
DEFINE_CCVARIABLECALLBACK(ERLAST, status.lastError)
// Profile variables
DEFINE_CCVARIABLECALLBACK_UA(PROFILE, UA_STRING(C3_Profile_print(info.profile)))
DEFINE_CCVARIABLECALLBACK(SI, C3_SI_TOSTRING(info.profile.SI))
DEFINE_CCVARIABLECALLBACK(OC, C3_OC_TOSTRING(info.profile.OC))
DEFINE_CCVARIABLECALLBACK(EM, C3_EM_TOSTRING(info.profile.EM))
DEFINE_CCVARIABLECALLBACK(ES, C3_ES_TOSTRING(info.profile.ES))
DEFINE_CCVARIABLECALLBACK(OM, C3_OM_TOSTRING(info.profile.OM))

typedef struct ExecutionLoopContext {
    size_t ccCount;
    C3_CC **ccArray;
} ExecutionLoopContext;

void
cc_executionLoop(UA_Server *server, void *data) {
    if(data == NULL)
        return;
    ExecutionLoopContext *context = (ExecutionLoopContext *)data;
    for(size_t i = 0; i < context->ccCount; i++) {
        C3_CC_runIterate(context->ccArray[i]);
    }
}

void *
cc_executionLoopContext_new(UA_Server *server) {
    UA_BrowseDescription bd;
    bd.browseDirection = UA_BROWSEDIRECTION_FORWARD;
    bd.includeSubtypes = UA_FALSE;
    bd.nodeClassMask = UA_NODECLASS_OBJECT;
    bd.nodeId = ID_DUMMYCCFOLDER;
    bd.referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    bd.resultMask = UA_BROWSERESULTMASK_TARGETINFO;
    UA_BrowseResult br = UA_Server_browse(server, 0, &bd);
    if(br.statusCode == UA_STATUSCODE_GOOD) {
        ExecutionLoopContext *context = malloc(sizeof(ExecutionLoopContext));
        if(context == NULL) {
            UA_BrowseResult_clear(&br);
            return NULL;
        }
        context->ccCount = br.referencesSize;
        context->ccArray = malloc(sizeof(C3_CC *) * context->ccCount);
        if(context->ccArray == NULL) {
            UA_BrowseResult_clear(&br);
            free(context);
            return NULL;
        }
        for(size_t i = 0; i < context->ccCount; i++) {
            UA_Server_getNodeContext(server, br.references[i].nodeId.nodeId,
                                     (void **)&context->ccArray[i]);
        }
        UA_BrowseResult_clear(&br);
        return context;
    }
    UA_BrowseResult_clear(&br);
    return NULL;
}

void
cc_executionLoopContext_delete(void *context) {
    if(context == NULL)
        return;
    free(((ExecutionLoopContext *)context)->ccArray);
    free(context);
}

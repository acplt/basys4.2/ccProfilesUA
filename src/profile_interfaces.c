#include <profile_interfaces.h>
#ifdef CCPUA_ENABLE_PACKML
#include <profile_packML.h>
#endif

#include <cc_connector.h>

/*
 * Create modelling rules to mark which variables, objects and methods are necessary in
 * which profile or facet.
 *
 * See colums Rule and Facet in tables:
 * https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponentProfiles#C3_SI_Facet_CMD
 * https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponentProfiles#C3_SI_Facet_OPERATIONS
 */

void
createFacetModellingRules(UA_Server *server, char *facetGroupName,
                          char *facetGroupDisplayName, FACET_GROUP facetGroupNo,
                          char **facetStrings, size_t facetStringsSize) {
    /* Add a folder for the rules */
    UA_NodeId folderId =
        UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCMODELLINGRULES +
                                           (facetGroupNo + 1) * NS2FACTOR_PROFILES);
    addFolder(server, facetGroupName, facetGroupDisplayName, "", folderId,
              UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCMODELLINGRULES), true);

    /* Add a rule for each facet group value*/
    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    UA_NodeId ruleId;
    UA_Variant namingRule;
    UA_Int32 namingRuleValue = 3;  // FIXME: use correct naming rule values?
    UA_Variant_setScalar(&namingRule, &namingRuleValue, &UA_TYPES[UA_TYPES_INT32]);
    for(int i = 1; i < facetStringsSize; ++i) {
        char iString[12];
        sprintf(iString, "%d", (facetGroupNo < 3) ? 1 << (i - 1) : i);
        ruleId = UA_NODEID_NUMERIC(NS_PROFILES,
                                   NS2OFFSET_CCMODELLINGRULES +
                                       (facetGroupNo + 1) * NS2FACTOR_PROFILES + i);
        // TODO create array of descriptions from comments in C3_Profiles.h
        oAttr.description = UA_LOCALIZEDTEXT("en-US", iString);
        oAttr.displayName = UA_LOCALIZEDTEXT("en-US", facetStrings[i]);
        UA_Server_addObjectNode(
            server, ruleId, folderId, UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
            UA_QUALIFIEDNAME(NS_PROFILES, facetStrings[i]),
            UA_NODEID_NUMERIC(0, UA_NS0ID_MODELLINGRULETYPE), oAttr, NULL, NULL);
        // Set naming rule value //TODO if an own modelling rule type is used, this can be
        // defined in the type
        serverWriteValueByBrowsename(server, ruleId, UA_QUALIFIEDNAME(0, "NamingRule"),
                                     namingRule);
    }
}

void
createProfileModellingRules(UA_Server *server) {
    UA_LOG_DEBUG(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "createProfile_OPERATIONS");

    /* Add modelling rule object type
    UA_ObjectTypeAttributes otAttr = UA_ObjectTypeAttributes_default;
    otAttr.description = UA_LOCALIZEDTEXT("en-US", "A modelling rule for control component
    interfaces. It determines whether a node is mandatory or optional in a specific
    facet."); otAttr.displayName = UA_LOCALIZEDTEXT("en-US",
    "ControlComponentModellingRuleType"); UA_Server_addObjectTypeNode(server,
    UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCMODELLINGRULES+NS2FACTOR_FOLDER),
                                UA_NODEID_NUMERIC(0, UA_NS0ID_MODELLINGRULETYPE),
                                UA_NODEID_NUMERIC(0, UA_NS0ID_HASSUBTYPE),
                                UA_QUALIFIEDNAME(1, "CCModellingRuleType"), otAttr, NULL,
    NULL);
    //Add necessary properties
    */

    /* Create folder and modelling rule instances */
    addFolder(
        server, "CCFacets", "ControlComponentFacets",
        "Folder for modelling rules, that determine the facets of control components.",
        UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCMODELLINGRULES),
        UA_NODEID_NUMERIC(0, UA_NS0ID_SERVER_SERVERCAPABILITIES_MODELLINGRULES), true);

    // Mandatory or optional in sense of ccInterface
    // TODO maybe its better to use the NS0 rules and overwrite the instanciation process
    // in a different callback
    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    oAttr.description = UA_LOCALIZEDTEXT(
        "en-US", "Marks an Control Component Type node child as mandatory in "
                 "combination with other facet modelling rules.");
    oAttr.displayName = UA_LOCALIZEDTEXT("en-US", "CCMandatory");
    UA_Server_addObjectNode(
        server, UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCMODELLINGRULES + 1),
        UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCMODELLINGRULES),
        UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
        UA_QUALIFIEDNAME(NS_PROFILES, "CCMandatory"),
        UA_NODEID_NUMERIC(0, UA_NS0ID_MODELLINGRULETYPE), oAttr, NULL, NULL);
    oAttr.description = UA_LOCALIZEDTEXT(
        "en-US", "Marks an Control Component Type node child as optional in "
                 "combination with other facet modelling rules.");
    oAttr.displayName = UA_LOCALIZEDTEXT("en-US", "CCOptional");
    UA_Server_addObjectNode(
        server, UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCMODELLINGRULES + 2),
        UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCMODELLINGRULES),
        UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
        UA_QUALIFIEDNAME(NS_PROFILES, "CCOptional"),
        UA_NODEID_NUMERIC(0, UA_NS0ID_MODELLINGRULETYPE), oAttr, NULL, NULL);

    // facet values
    createFacetModellingRules(server, "SIFacets", "ServiceInterfaceFacets",
                              FACET_GROUP_SI, (char **)C3_SI_FACET_STRINGS,
                              C3_SI_FACET_COUNT);
    createFacetModellingRules(server, "OCFacets", "OccupationFacets", FACET_GROUP_OC,
                              (char **)C3_OC_FACET_STRINGS, C3_OC_FACET_COUNT);
    createFacetModellingRules(server, "EMFacets", "ExecutionModeFacets", FACET_GROUP_EM,
                              (char **)C3_EM_FACET_STRINGS, C3_EM_FACET_COUNT);
    createFacetModellingRules(server, "ESFacets", "ExecutionStateFacets", FACET_GROUP_ES,
                              (char **)C3_ES_FACET_STRINGS, C3_ES_FACET_COUNT);
    createFacetModellingRules(server, "OMFacets", "OperationModeFacets", FACET_GROUP_OM,
                              (char **)C3_OM_FACET_STRINGS, C3_OM_FACET_COUNT);
}

/* Add a cmd profile service interface
 * (single command input string variable)
 * as specialised dummy control component object type node */
// Literatur: http://publications.rwth-aachen.de/record/766365
void
createProfile_CMD(UA_Server *server) {
    UA_LOG_DEBUG(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "createProfile_CMD");
    UA_NodeId id = UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCINTERFACETYPES +
                                                      C3_SI_CMD * NS2FACTOR_PROFILES);
    /* Create an object type node for the dummy control component */
    addObjectType(
        server, "ICommandType", "ICommandType",
        "Interface for basys control components, with a CMD service interface profile.",
        id, UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCINTERFACETYPES));
    UA_Server_writeIsAbstract(server, id, UA_TRUE);

    /* Add CMD variable */
    UA_NodeId variableId = UA_NODEID_NUMERIC(NS_PROFILES, id.identifier.numeric + 1);
    addStringProperty(server, "CMD", "CommandInput",
                      "String command input. Syntax: [sender];order;[param=value, ...]. "
                      "Example: 'Operator1;START;'",
                      variableId, id, "", UA_TRUE);
    markMandatory(server, variableId, true, false);
    // Set callback to evaluate comand inputs
    addVariableCallback(server, variableId, NULL, cc_commandInputCallback);

    /* Optional variables */
    // TODO derive from own CMD variable Type, with optional configuration values
    // (CMDOLIST, CMDOEREF, CMDOPREF)
    variableId.identifier.numeric++;
    addStringArrayProperty(server, "CMDOLIST", "CommandOrderList",
                           "Array of supportet orders.", variableId, id,
                           (const char *[16]){"START", "STOP", "RESET", "HOLD", "UNHOLD",
                                              "SUSPEND", "UNSUSPEND", "ABORT", "CLEAR",
                                              "AUTO", "SEMIAUTO", "MANUAL", "SIMULATE",
                                              "OCCUPY", "PRIO", "FREE"},
                           16, false);
    markMandatory(server, variableId, false, true);
    markFacetModellingRule(server, variableId, FACET_GROUP_SI, (int[]){C3_SI_CMD}, 1);
    variableId.identifier.numeric++;
    addStringArrayProperty(server, "CMDOPREF", "CommandOrderParameterReference",
                           "Mapping of necessary Parameters for every order.", variableId,
                           id,
                           (const char *[16]){"", "", "", "", "", "", "", "", "", "", "",
                                              "", "", "", "", ""},
                           16, false);
    markMandatory(server, variableId, false, true);
    markFacetModellingRule(server, variableId, FACET_GROUP_SI, (int[]){C3_SI_CMD}, 1);
    variableId.identifier.numeric++;
    addStringProperty(server, "CMDLAST", "LastCommand", "Last executed command.",
                      variableId, id, "", false);
    markMandatory(server, variableId, false, true);
    markFacetModellingRule(server, variableId, FACET_GROUP_SI, (int[]){C3_SI_CMD}, 1);
    addVariableCallback(server, variableId, cc_VariableCallback_CMDLAST, NULL);
}
/* Add a operations profile service interface
 * (folder with methods for every command)
 * as a specialised dummy control component object type node */
void
createProfile_OPERATIONS(UA_Server *server) {
    UA_LOG_DEBUG(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "createProfile_OPERATIONS");
    UA_NodeId id = UA_NODEID_NUMERIC(
        NS_PROFILES, NS2OFFSET_CCINTERFACETYPES + C3_SI_OPERATIONS * NS2FACTOR_PROFILES);
    /* Create an object type node for the dummy control component */
    addObjectType(server, "IOperationsType", "IOperationsType",
                  "Interface for basys control components, with a OPERATIONS service "
                  "interface profile.",
                  id, UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCINTERFACETYPES));
    UA_Server_writeIsAbstract(server, id, UA_TRUE);

    /* Add Folder for OPERATIONS */
    UA_NodeId idFolder =
        UA_NODEID_NUMERIC(NS_PROFILES, id.identifier.numeric + NS2FACTOR_FOLDER);
    addFolder(server, "OPERATIONS", "Operations",
              "A folder for all operations of this control component.", idFolder, id,
              UA_TRUE);
    markMandatory(server, idFolder, true, false);

    // EXST
    UA_NodeId idMethod = UA_NODEID_NUMERIC(NS_PROFILES, idFolder.identifier.numeric + 1);
    addMethod(server, "START", "Start",
              "Execution state command: may be called, when operation mode is selected "
              "and WORKST is READY.",
              idMethod, idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_ES,
                           (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP,
                                   C3_ES_OPCUA, C3_ES_NE160},
                           6);
    idMethod.identifier.numeric++;
    addMethod(server, "STOP", "Stop",
              "Execution state command: stops the component during IDLE, EXECUTE, HOLD, "
              "SUSPEND, COMPLETE and their transient states.",
              idMethod, idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(
        server, idMethod, FACET_GROUP_ES,
        (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_OPCUA}, 5);
    idMethod.identifier.numeric++;
    addMethod(server, "RESET", "Reset",
              "Execution state command: resets a STOPPED component.", idMethod, idFolder,
              cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(
        server, idMethod, FACET_GROUP_ES,
        (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_OPCUA}, 5);
    idMethod.identifier.numeric++;
    addMethod(server, "CLEAR", "Clear",
              "Execution state command: clears an component from ABORTED state.",
              idMethod, idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(
        server, idMethod, FACET_GROUP_ES,
        (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_NE160}, 5);
    idMethod.identifier.numeric++;
    addMethod(server, "ABORT", "Abort",
              "Execution state command: aborts the commands execution from any state.",
              idMethod, idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(
        server, idMethod, FACET_GROUP_ES,
        (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_NE160}, 5);
    idMethod.identifier.numeric++;
    addMethod(server, "HOLD", "Hold",
              "Execution state command: holds the component because of an internal error "
              "from EXECUTE.",
              idMethod, idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_ES,
                           (int[]){C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_NE160}, 4);
    idMethod.identifier.numeric++;
    addMethod(server, "UNHOLD", "Unhold",
              "Execution state command: resumes EXECUTE state from HELD.", idMethod,
              idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_ES,
                           (int[]){C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_NE160}, 4);
    idMethod.identifier.numeric++;
    addMethod(server, "SUSPEND", "Suspend",
              "Execution state command: suspends the component because of an external "
              "condition from EXECUTE.",
              idMethod, idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_ES,
                           (int[]){C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_OPCUA}, 4);
    idMethod.identifier.numeric++;
    addMethod(server, "UNSUSPEND", "Unsuspend",
              "Execution state command: resumes EXECUTE state from SUSPENDED.", idMethod,
              idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_ES,
                           (int[]){C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_OPCUA}, 4);

    // EXMODE
    idMethod.identifier.numeric++;
    addMethod(server, "AUTO", "Automatic",
              "Execution mode command: change to automatic execution.", idMethod,
              idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_EM, (int[]){C3_EM_AUTO}, 1);
    idMethod.identifier.numeric++;
    addMethod(server, "MANUAL", "Manual",
              "Execution mode command: change to manual execution", idMethod, idFolder,
              cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_EM, (int[]){C3_EM_MANUAL}, 1);
    idMethod.identifier.numeric++;
    addMethod(server, "SEMIAUTO", "SemiAutomatic",
              "Execution mode command: change to semiautomatic execution.", idMethod,
              idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_EM, (int[]){C3_EM_SEMIAUTO}, 1);
    idMethod.identifier.numeric++;
    addMethod(server, "SIMULATE", "Simulate",
              "Execution mode command: change to simulation mode.", idMethod, idFolder,
              cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_EM, (int[]){C3_EM_SIMULATE}, 1);

    // OCCUPATION
    idMethod.identifier.numeric++;
    addMethod(server, "FREE", "Free",
              "Occupation command: free control component, so that other senders are "
              "able to occupy the component.",
              idMethod, idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_OC,
                           (int[]){C3_OC_OCCUPIED, C3_OC_ORDER_PRIO}, 2);
    idMethod.identifier.numeric++;
    addMethod(server, "OCCUPY", "Occupy",
              "Occupation command: occupy control component, so that other senders can't "
              "occupy and operate the component.",
              idMethod, idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_OC, (int[]){C3_OC_OCCUPIED}, 1);
    idMethod.identifier.numeric++;
    addMethod(server, "PRIO", "Priority",
              "Occupation command: occupy with priority to overwrite the occupation of "
              "another sender. Should only be used by operator or maintainer.",
              idMethod, idFolder, cc_operationCallback);
    markMandatory(server, idMethod, true, true);
    markFacetModellingRule(server, idMethod, FACET_GROUP_OC, (int[]){C3_OC_ORDER_PRIO},
                           1);
    /* OPMODE
    idMethod.identifier.numeric++;
    addMethod(
        server, "JOG", "Jogging",
        "Operation mode: selects a special operation mode to jog the skills of a "
        "component. Only possible in execution mode MANUAL and in execution state IDLE.",
        idMethod, idFolder, UA_FALSE, NULL);
    */
}

UA_StatusCode
createControlComponentProfileType(UA_Server *server) {
    UA_LOG_DEBUG(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                 "createControlComponentProfileType");
    UA_NodeId id = ID_CCPROFILETYPE;

    /* The whole C3_Profile as a string */  // TODO use own dataType for status as a
                                            // struct with numerical/byte values for
                                            // performance
    UA_VariableTypeAttributes vtAttr = UA_VariableTypeAttributes_default;
    vtAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    vtAttr.valueRank = UA_VALUERANK_SCALAR;
    vtAttr.displayName = UA_LOCALIZEDTEXT("en-US", "ControlComponentProfileType");
    vtAttr.description =
        UA_LOCALIZEDTEXT("en-US", "Holds profile values of a control component.");
    /* a matching default value is required */
    UA_String classVar = UA_STRING("SI:0,OC:0,EM:0,ES:0,OM:0");
    UA_Variant_setScalar(&vtAttr.value, &classVar, &UA_TYPES[UA_TYPES_STRING]);
    UA_Server_addVariableTypeNode(server, id,
                                  UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                  UA_NODEID_NUMERIC(0, UA_NS0ID_HASSUBTYPE),
                                  UA_QUALIFIEDNAME(NS_PROFILES, "CCProfileType"),
                                  UA_NODEID_NULL, vtAttr, NULL, NULL);
    addVariableCallback(server, id, cc_VariableCallback_PROFILE, NULL);

    /* Add a all Profile properties to variable */
    // TODO use enumeration as datatype?
    UA_NodeId idVariable = UA_NODEID_NUMERIC(NS_PROFILES, id.identifier.numeric + 1);
    addStringProperty(server, "SI", "ServiceInterface",
                      "Definition of the used service interface profile.", idVariable, id,
                      C3_SI_FACET_STRINGS[0], UA_FALSE);
    markMandatory(server, idVariable, true, false);
    addVariableCallback(server, idVariable, cc_VariableCallback_SI, NULL);
    idVariable.identifier.numeric++;
    addStringProperty(server, "OC", "Occupation",
                      "Definition of the used occupation profile.", idVariable, id,
                      C3_OC_FACET_STRINGS[0], UA_FALSE);
    markMandatory(server, idVariable, true, false);
    addVariableCallback(server, idVariable, cc_VariableCallback_OC, NULL);
    idVariable.identifier.numeric++;
    addStringProperty(server, "EM", "ExecutionMode",
                      "Definition of the used execution mode profile.", idVariable, id,
                      C3_EM_FACET_STRINGS[0], UA_FALSE);
    markMandatory(server, idVariable, true, false);
    addVariableCallback(server, idVariable, cc_VariableCallback_EM, NULL);
    idVariable.identifier.numeric++;
    addStringProperty(server, "ES", "ExecutionState",
                      "Definition of the used execution state profile.", idVariable, id,
                      C3_ES_FACET_STRINGS[0], UA_FALSE);
    markMandatory(server, idVariable, true, false);
    addVariableCallback(server, idVariable, cc_VariableCallback_ES, NULL);
    idVariable.identifier.numeric++;
    addStringProperty(server, "OM", "OperationMode",
                      "Definition of the used operation mode profile.", idVariable, id,
                      C3_OM_FACET_STRINGS[0], UA_FALSE);
    markMandatory(server, idVariable, true, false);
    addVariableCallback(server, idVariable, cc_VariableCallback_OM, NULL);
    idVariable.identifier.numeric++;
}

UA_StatusCode
createControlComponentStatusType(UA_Server *server) {
    UA_LOG_DEBUG(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                 "createControlComponentStatusType");
    UA_NodeId id = ID_CCSTATUSTYPE;

    /* The whole status as a string */  // TODO use own dataType for status as a struct
                                        // with numerical state values --> performance
    UA_VariableTypeAttributes vtAttr = UA_VariableTypeAttributes_default;
    vtAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    vtAttr.valueRank = UA_VALUERANK_SCALAR;
    vtAttr.displayName = UA_LOCALIZEDTEXT("en-US", "ControlComponentStatusType");
    vtAttr.description = UA_LOCALIZEDTEXT(
        "en-US", "Holds all aggregated standardized states of a control component.");
    /* a matching default value is required */
    UA_String classVar = UA_STRING("OK");
    UA_Variant_setScalar(&vtAttr.value, &classVar, &UA_TYPES[UA_TYPES_STRING]);

    UA_Server_addVariableTypeNode(server, id,
                                  UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                  UA_NODEID_NUMERIC(0, UA_NS0ID_HASSUBTYPE),
                                  UA_QUALIFIEDNAME(NS_PROFILES, "CCStatusType"),
                                  UA_NODEID_NULL, vtAttr, NULL, NULL);

    /* Add all STATUS properties to variable */
    UA_NodeId idVariable = UA_NODEID_NUMERIC(NS_PROFILES, id.identifier.numeric);

    // WORKST
    idVariable.identifier.numeric++;
    addStringProperty(server, "WORKST", "WorkState",
                      "Current work state. READY after operation mode is selected and "
                      "start command is granted. Specific to operation mode.",
                      idVariable, id, "IDLE", UA_TRUE);
    markMandatory(server, idVariable, true, false);
    addVariableCallback(server, idVariable, cc_VariableCallback_WORKST, NULL);
    // ERROR
    idVariable.identifier.numeric++;
    addStringProperty(server, "ER", "Error",
                      "Current error state. OK if no error is present.", idVariable, id,
                      "OK", UA_FALSE);
    markMandatory(server, idVariable, false, true);
    markFacetModellingRule(
        server, idVariable, FACET_GROUP_SI,
        (int[]){C3_SI_CMD, C3_SI_OPERATIONS, C3_SI_SERVICES, C3_SI_MTP, C3_SI_PACKML}, 5);
    addVariableCallback(server, idVariable, cc_VariableCallback_ER, NULL);
    idVariable.identifier.numeric++;
    addStringProperty(server, "ERLAST", "LastError", "Previous error state.", idVariable,
                      id, "OK", UA_FALSE);
    markMandatory(server, idVariable, false, true);
    markFacetModellingRule(
        server, idVariable, FACET_GROUP_SI,
        (int[]){C3_SI_CMD, C3_SI_OPERATIONS, C3_SI_SERVICES, C3_SI_MTP, C3_SI_PACKML}, 5);
    addVariableCallback(server, idVariable, cc_VariableCallback_ERLAST, NULL);
    // OCCST
    idVariable.identifier.numeric++;
    addStringProperty(server, "OCCUPIER", "Occupier", "ID of current occupier.",
                      idVariable, id, C3_OC_OCCUPIER_FREE, UA_FALSE);
    markMandatory(server, idVariable, true, true);
    markFacetModellingRule(server, idVariable, FACET_GROUP_OC,
                           (int[]){C3_OC_OCCUPIED, C3_OC_PRIO, C3_OC_LOCAL}, 3);
    addVariableCallback(server, idVariable, cc_VariableCallback_OCCUPIER, NULL);
    idVariable.identifier.numeric++;
    addStringProperty(server, "OCCST", "OccupationState",
                      "Current occupation state: FREE, OCCUPIED, PRIO, LOCAL", idVariable,
                      id, "FREE", UA_FALSE);
    markMandatory(server, idVariable, true, true);
    markFacetModellingRule(server, idVariable, FACET_GROUP_OC,
                           (int[]){C3_OC_OCCUPIED, C3_OC_PRIO, C3_OC_LOCAL}, 3);
    addVariableCallback(server, idVariable, cc_VariableCallback_OCCST, NULL);
    idVariable.identifier.numeric++;
    addStringProperty(server, "OCCLAST", "LastOccupier", "ID of previous occupier.",
                      idVariable, id, C3_OC_OCCUPIER_FREE, UA_FALSE);
    markMandatory(server, idVariable, false, true);
    markFacetModellingRule(server, idVariable, FACET_GROUP_OC,
                           (int[]){C3_OC_OCCUPIED, C3_OC_PRIO, C3_OC_LOCAL}, 3);
    addVariableCallback(server, idVariable, cc_VariableCallback_OCCLAST, NULL);
    // EXMODE
    idVariable.identifier.numeric++;
    addStringProperty(server, "EXMODE", "ExecutionMode",
                      "Current execution mode: AUTO, SEMIAUTO, MANUAL, SIMULATE",
                      idVariable, id, "AUTO", UA_FALSE);
    markMandatory(server, idVariable, true, true);
    markFacetModellingRule(
        server, idVariable, FACET_GROUP_EM,
        (int[]){C3_EM_AUTO, C3_EM_MANUAL, C3_EM_SEMIAUTO, C3_EM_SIMULATE}, 4);
    addVariableCallback(server, idVariable, cc_VariableCallback_EXMODE, NULL);
    // EXST
    idVariable.identifier.numeric++;
    addStringProperty(server, "EXST", "ExecutionState",
                      "Current execution state (PackML State).", idVariable, id, "IDLE",
                      UA_FALSE);
    markMandatory(server, idVariable, true, true);
    markFacetModellingRule(server, idVariable, FACET_GROUP_ES,
                           (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP,
                                   C3_ES_OPCUA, C3_ES_NE160},
                           6);
    addVariableCallback(server, idVariable, cc_VariableCallback_EXST, NULL);
    // OPMODE
    idVariable.identifier.numeric++;
    addStringProperty(server, "OPMODE", "OperationMode",
                      "Current operation mode / program.", idVariable, id, "NONE",
                      UA_FALSE);
    markMandatory(server, idVariable, true, true);
    markFacetModellingRule(server, idVariable, FACET_GROUP_OM,
                           (int[]){C3_OM_MULTI, C3_OM_PARALLEL, C3_OM_CONTI}, 3);
    addVariableCallback(server, idVariable, cc_VariableCallback_OPMODE, NULL);
}

void
createControlComponentInterface(UA_Server *server) {
    // To keep optional nodes in instance declarations for the typesystem
    CreateAllOptionalNodes = true;

    /* Create modelling rules */
    createProfileModellingRules(server);  // TODO move to own .c file

    /* Create an object type node for the control component type */
    UA_NodeId id = UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCINTERFACETYPES);
    UA_LOG_DEBUG(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                 "createControlComponentInterface");
    addObjectType(server, "IControlComponentType", "IControlComponentType",
                  "An abstract basys control component type.", id,
                  UA_NODEID_NUMERIC(0, UA_NS0ID_BASEINTERFACETYPE));
    // Make type abstract
    UA_Server_writeIsAbstract(server, id, UA_TRUE);

    /* Create the profile variable */
    createControlComponentProfileType(server);
    // Add variable of CCProfileType
    UA_VariableAttributes vAttr = UA_VariableAttributes_default;
    vAttr.displayName = UA_LOCALIZEDTEXT("en-US", "Profile");
    vAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    vAttr.valueRank = UA_VALUERANK_SCALAR;
    vAttr.description =
        UA_LOCALIZEDTEXT("en-US", "Holds profile values of a control component.");
    UA_NodeId profileId = UA_NODEID_NUMERIC(NS_PROFILES, id.identifier.numeric + 1);
    UA_Server_addVariableNode(
        server, profileId, id, UA_NODEID_NUMERIC(0, UA_NS0ID_HASPROPERTY),
        UA_QUALIFIEDNAME(NS_PROFILES, "C3_Profile"), ID_CCPROFILETYPE, vAttr, NULL, NULL);
    // Make the variable optional
    markMandatory(server, profileId, false, true);
    markFacetModellingRule(
        server, profileId, FACET_GROUP_SI,
        (int[]){C3_SI_CMD, C3_SI_OPERATIONS, C3_SI_SERVICES, C3_SI_MTP, C3_SI_PACKML}, 5);

    /* Create Status Variable Type and instanciate it */
    createControlComponentStatusType(server);
    // Add variable of CCStatusType
    vAttr.displayName = UA_LOCALIZEDTEXT("en-US", "Status");
    vAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    vAttr.valueRank = UA_VALUERANK_SCALAR;
    vAttr.description = UA_LOCALIZEDTEXT(
        "en-US", "Holds all aggregated standardized states of a control component.");
    UA_NodeId statusId = UA_NODEID_NUMERIC(NS_PROFILES, id.identifier.numeric + 2);
    UA_Server_addVariableNode(
        server, statusId, id, UA_NODEID_NUMERIC(0, UA_NS0ID_HASPROPERTY),
        UA_QUALIFIEDNAME(NS_PROFILES, "STATUS"), ID_CCSTATUSTYPE, vAttr, NULL, NULL);
    // Make the variable mandatory for all control components, as it holds the states for
    // the cc_connector
    markMandatory(server, statusId, true, false);

    /* Create specialised types for every profile */
    createProfile_CMD(server);
    createProfile_OPERATIONS(server);
#ifdef CCPUA_ENABLE_PACKML
    createProfile_PACKML(server);
#endif
    CreateAllOptionalNodes = false;
}
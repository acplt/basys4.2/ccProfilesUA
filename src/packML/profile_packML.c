#include <profile_packML.h>

#include <cc_connector.h>

#include <C3_ExecutionState.h>
#include <nodeset_packml.h>

/* Custom utility functions -> TODO move to utilities?*/
bool
uaStringInCharArray(const UA_String *string, const char **array, size_t arraySize,
                    size_t *elementIndex) {
    UA_String tempStr;
    for(int i = 0; i < arraySize; i++) {
        tempStr = UA_STRING((char *)array[i]);
        if(UA_String_equal(string, &tempStr)) {
            *elementIndex = i;
            return true;
        }
    }
    return false;
}

/* Callback for PackML ES methods */
static UA_StatusCode
ESOperation(UA_Server *server, const UA_NodeId *sessionId, void *sessionHandle,
            const UA_NodeId *methodId, void *methodContext, const UA_NodeId *objectId,
            void *objectContext, size_t inputSize, const UA_Variant *input,
            size_t outputSize, UA_Variant *output) {
    UA_StatusCode result = UA_STATUSCODE_GOOD;
    UA_NodeId *ccId = (UA_NodeId *)objectContext;
    if(UA_NodeId_isNull(ccId))
        return UA_STATUSCODE_BADINVALIDSELFREFERENCE;

    /* Get order id from method name and convert to upper case*/
    UA_QualifiedName order;
    result = UA_Server_readBrowseName(server, *methodId, &order);
    if(result != UA_STATUSCODE_GOOD) {
        return result;
    }
    // change to upper case
    for(int i = 0; i < order.name.length; ++i) {
        if(order.name.data[i] >= 'a' && order.name.data[i] <= 'z') {
            order.name.data[i] = order.name.data[i] - 32;
        }
    }

    /* Evaluate the order*/
    if(result != UA_STATUSCODE_GOOD) {
        UA_QualifiedName_clear(&order);
        return result;
    }
    result =
        cc_executeCommand(server, ccId, (UA_String *)sessionHandle, &order.name, NULL);

    /* Free common ressources */
    UA_QualifiedName_clear(&order);
    return result;
}

void
createProfile_PACKML(UA_Server *server) {
    UA_LOG_DEBUG(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "createProfile_PACKML");

    /* Load PackML types from companion specification
     * Assumes that CreateAllOptionalNodes is set to true during instanciation process of
     * types.
     */
    UA_StatusCode result = nodeset_packml(server);
    if(result != UA_STATUSCODE_GOOD) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Couldn't load PackML nodeset: %s.", UA_StatusCode_name(result));
        return;
    }

    /* Create an object type node for the dummy control component */
    UA_NodeId id = UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCINTERFACETYPES +
                                                      C3_SI_PACKML * NS2FACTOR_PROFILES);
    addObjectType(server, "IPackMLType", "IPackMLType",
                  "Interface for basys control components, with a PACKML service "
                  "interface profile.",
                  id, UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCINTERFACETYPES));
    UA_Server_writeIsAbstract(server, id, UA_TRUE);

    /* Link CCInterface to PackMLBaseObject as instance declaration */
    UA_NodeId baseId = UA_NODEID_NUMERIC(NS_PROFILES, id.identifier.numeric + 1);
    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    oAttr.description =
        UA_LOCALIZEDTEXT("en-US", "PackMLBaseObject of a control component");
    oAttr.displayName = UA_LOCALIZEDTEXT("en-US", PACKML_BASEOBJECT_NAME);
    result = UA_Server_addObjectNode(
        server, baseId, id, UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
        UA_QUALIFIEDNAME(NS_PROFILES, PACKML_BASEOBJECT_NAME), PACKML_OBJECTSFOLDER,
        oAttr, NULL, NULL);
    // TODO mark as mandatory_place_holder and rename to CC name during instanciation
    markMandatory(server, baseId, true, false);

    /* Mark ES methods according to ES facet modelling rules and add callbacks.
     * See order table in:
     * https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponentProfiles#C3_ES_Facets
     */
    // Mark mandatory with custom rules and add callback
    for(int i = 0; i < PACKML_ESMETHOD_ID_SIZE; ++i) {
        markMandatory(server, UA_NODEID_NUMERIC(NS_PACKML, PACKML_ESMETHOD_ID[i]), true,
                      true);
        UA_Server_setMethodNode_callback(
            server, UA_NODEID_NUMERIC(NS_PACKML, PACKML_ESMETHOD_ID[i]), ESOperation);
        // TODO check Start parameter --> use as cmd parameter or to select OpMode ?
    }
    // mark custom rules
    markFacetModellingRule(server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_START),
                           FACET_GROUP_ES,
                           (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP,
                                   C3_ES_OPCUA, C3_ES_NE160},
                           6);
    markFacetModellingRule(
        server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_STOP), FACET_GROUP_ES,
        (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_OPCUA}, 5);
    markFacetModellingRule(
        server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_RESET), FACET_GROUP_ES,
        (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_OPCUA}, 5);
    markFacetModellingRule(
        server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_RESET_COMPLETE), FACET_GROUP_ES,
        (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_OPCUA}, 5);
    markFacetModellingRule(
        server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_ABORT), FACET_GROUP_ES,
        (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_NE160}, 5);
    markFacetModellingRule(
        server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_CLEAR), FACET_GROUP_ES,
        (int[]){C3_ES_BASYS, C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_NE160}, 5);
    markFacetModellingRule(server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_HOLD),
                           FACET_GROUP_ES,
                           (int[]){C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_NE160}, 4);
    markFacetModellingRule(server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_UNHOLD),
                           FACET_GROUP_ES,
                           (int[]){C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_NE160}, 4);
    markFacetModellingRule(server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_SUSPEND),
                           FACET_GROUP_ES,
                           (int[]){C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_OPCUA}, 4);
    markFacetModellingRule(server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_UNSUSPEND),
                           FACET_GROUP_ES,
                           (int[]){C3_ES_PACKML, C3_ES_ISA88, C3_ES_MTP, C3_ES_OPCUA}, 4);

    // TODO add callback for SetUnitMode
    // TODO add dummy callbacks for SetMachSpeed, SetParameter, SetProduct
}

static void
setStatesAndTransitions(UA_Server *server, UA_NodeId stateMachineId, bool transitions,
                        UA_UInt16 idsCount, UA_UInt32 *ids) {
    UA_NodeId nodeIds[idsCount];
    for(int i = 0; i < idsCount; i++) {
        nodeIds[i] = UA_NODEID_NUMERIC(NS_PACKML, ids[i]);
    }
    UA_Variant value;
    UA_Variant_setArray(&value, nodeIds, idsCount, &UA_TYPES[UA_TYPES_NODEID]);
    serverWriteValueByBrowsename(server, stateMachineId,
                                 (transitions)
                                     ? UA_QUALIFIEDNAME(0, "AvailableTransitions")
                                     : UA_QUALIFIEDNAME(0, "AvailableStates"),
                                 value);
}

typedef struct CurrentStateContext {
    UA_NodeId ccEXSTId;
    // 0 = Base, 1 = Machine, 2 = Execute
    int subStateMachine;
    bool isId;
    int lastState;  // should be initialised with -1
} CurrentStateContext;

static void
freeStateMachineContext(UA_Server *server, UA_NodeId parentId, char *name,
                        UA_NodeId *stateMachineIdOut) {
    *stateMachineIdOut = UA_NODEID_NULL;
    if(UA_NodeId_isNull(&parentId))
        return;
    if(getChildByBrowsename(server, parentId, UA_QUALIFIEDNAME(NS_PACKML, name),
                            stateMachineIdOut) != UA_STATUSCODE_GOOD)
        return;
    // Free machine node context (ccId)
    void *ctx = NULL;
    if(UA_Server_getNodeContext(server, *stateMachineIdOut, (void **)&ctx) ==
           UA_STATUSCODE_GOOD &&
       ctx != NULL) {
        UA_free(ctx);
        ctx = NULL;
    }
    // Browse and free CurrentState
    UA_NodeId id = UA_NODEID_NULL;
    if(getChildByBrowsename(server, *stateMachineIdOut,
                            UA_QUALIFIEDNAME(0, "CurrentState"),
                            &id) != UA_STATUSCODE_GOOD)
        return;
    if(UA_Server_getNodeContext(server, id, (void **)&ctx) == UA_STATUSCODE_GOOD &&
       ctx != NULL) {
        UA_free(ctx);
        ctx = NULL;
    }
    // Browse and free Id
    if(getChildByBrowsename(server, id, UA_QUALIFIEDNAME(0, "Id"), &id) !=
       UA_STATUSCODE_GOOD)
        return;
    if(UA_Server_getNodeContext(server, id, (void **)&ctx) == UA_STATUSCODE_GOOD &&
       ctx != NULL) {
        UA_free(ctx);
        ctx = NULL;
    }
}

void
clearProfilePackML(UA_Server *server) {
    /* Browse for PackMLBaseObjects */
    UA_BrowseDescription bd;
    bd.browseDirection = UA_BROWSEDIRECTION_FORWARD;
    bd.includeSubtypes = UA_FALSE;
    bd.nodeClassMask = UA_NODECLASS_OBJECT;
    bd.nodeId = UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_PACKMLOBJECTS);
    bd.referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT);
    bd.resultMask = UA_BROWSERESULTMASK_BROWSENAME;
    UA_BrowseResult br = UA_Server_browse(server, 0, &bd);
    if(br.statusCode != UA_STATUSCODE_GOOD || br.referencesSize < 1) {
        UA_BrowseResult_clear(&br);
        return;
    }

    /* Clear all CurrentStatus contexts */
    UA_NodeId id = UA_NODEID_NULL;
    for(int i = 0; i < br.referencesSize; ++i) {
        freeStateMachineContext(server, br.references[i].nodeId.nodeId,
                                "BaseStateMachine", &id);
        freeStateMachineContext(server, id, "MachineState", &id);
        freeStateMachineContext(server, id, "ExecuteState", &id);
    }
    UA_BrowseResult_clear(&br);
}

static UA_StatusCode
readCurrentState(UA_Server *server, const UA_NodeId *sessionId, void *sessionContext,
                 const UA_NodeId *nodeId, void *nodeContext,
                 UA_Boolean includeSourceTimeStamp, const UA_NumericRange *range,
                 UA_DataValue *value) {
    CurrentStateContext *ctx = (CurrentStateContext *)nodeContext;
    // Get the state string value from CC
    UA_Variant ccEXSTVariant;
    UA_StatusCode result = UA_Server_readValue(server, ctx->ccEXSTId, &ccEXSTVariant);
    if(result != UA_STATUSCODE_GOOD)
        return result;

    // Convert its value to an C3_ES_STATE number
    size_t exst;
    if(!uaStringInCharArray((UA_String *)ccEXSTVariant.data, C3_ES_STATESTRING,
                            C3_ES_STATESIZE, &exst)) {
        UA_Variant_clear(&ccEXSTVariant);
        return UA_STATUSCODE_BADINVALIDSTATE;
    }
    UA_Variant_clear(&ccEXSTVariant);

    // check whether macro states or substates are applied
    if(ctx->subStateMachine == 0) {
        // State from SubStateMachine MachineState is active --> Cleared
        if(exst != C3_ES_STATE_ABORTED && exst != C3_ES_STATE_ABORTING)
            exst = 17;
    } else if(ctx->subStateMachine == 1) {
        // State from BaseStateMachine is active --> remain in the state from before
        if(exst == C3_ES_STATE_ABORTED || exst == C3_ES_STATE_ABORTING)
            exst = ctx->lastState > -1 ? ctx->lastState
                                       : C3_ES_STATE_STOPPED;  // inital state not defined
        // State from SubStateMachine ExecuteState is active --> Running
        else if(!(exst == C3_ES_STATE_CLEARING || exst == C3_ES_STATE_STOPPING ||
                  exst == C3_ES_STATE_STOPPED))
            exst = 18;
    } else if(ctx->subStateMachine == 2) {
        // State from BaseStateMachine or MachineStateMachine is active --> remain in the
        // state from before
        if(exst == C3_ES_STATE_ABORTED || exst == C3_ES_STATE_ABORTING ||
           exst == C3_ES_STATE_CLEARING || exst == C3_ES_STATE_STOPPING ||
           exst == C3_ES_STATE_STOPPED)
            exst = ctx->lastState > -1 ? ctx->lastState
                                       : C3_ES_STATE_IDLE;  // inital state not defined
    } else
        return UA_STATUSCODE_BADINTERNALERROR;
    ctx->lastState = exst;

    // Copy the correct Name or Id value to the output
    if(ctx->isId) {
        UA_NodeId state = UA_NODEID_NUMERIC(NS_PACKML, PACKML_STATE_ID[exst]);
        result =
            UA_Variant_setScalarCopy(&value->value, &state, &UA_TYPES[UA_TYPES_NODEID]);
    } else {
        // TODO support other locales defined in
        // https://reference.opcfoundation.org/v104/PackML/v100/docs/B.1.2/
        UA_LocalizedText state = UA_LOCALIZEDTEXT("en", (char *)PACKML_STATE_NAME[exst]);
        result = UA_Variant_setScalarCopy(&value->value, &state,
                                          &UA_TYPES[UA_TYPES_LOCALIZEDTEXT]);
    }
    value->hasValue = UA_TRUE;
    return result;
}

static UA_StatusCode
setCurrentStateCallback(UA_Server *server, UA_NodeId stateMachineId,
                        const UA_NodeId *ccEXSTId, int selectSubstateMachine) {
    /* Read the node ids */
    UA_NodeId currentStateId = UA_NODEID_NULL;
    UA_NodeId currentStateIdId = UA_NODEID_NULL;
    UA_StatusCode result = getChildByBrowsename(
        server, stateMachineId, UA_QUALIFIEDNAME(0, "CurrentState"), &currentStateId);
    result |= getChildByBrowsename(server, currentStateId, UA_QUALIFIEDNAME(0, "Id"),
                                   &currentStateIdId);
    if(result != UA_STATUSCODE_GOOD)
        return result;

    /* Save necessary information for read callback in node context of CurrentState
     * and Id
     */
    // CurrentState
    CurrentStateContext *ctx = UA_malloc(sizeof(CurrentStateContext));
    UA_NodeId_copy(ccEXSTId, &ctx->ccEXSTId);
    ctx->subStateMachine = selectSubstateMachine;
    ctx->isId = false;
    ctx->lastState = -1;
    result = UA_Server_setNodeContext(server, currentStateId, ctx);
    // Id
    ctx = UA_malloc(sizeof(CurrentStateContext));
    UA_NodeId_copy(ccEXSTId, &ctx->ccEXSTId);
    ctx->subStateMachine = selectSubstateMachine;
    ctx->isId = true;
    ctx->lastState = -1;
    result |= UA_Server_setNodeContext(server, currentStateIdId, ctx);
    if(result != UA_STATUSCODE_GOOD)
        return result;

    /* Add read callback to CurrentState and Id */
    UA_DataSource ds;
    ds.read = readCurrentState;
    ds.write = NULL;
    result = UA_Server_setVariableNode_dataSource(server, currentStateId, ds);
    result |= UA_Server_setVariableNode_dataSource(server, currentStateIdId, ds);
    return result;
}

void
configurePackMLBaseObject(UA_Server *server, const UA_NodeId ccId, C3_Profile profile) {
    UA_LOG_DEBUG(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "configurePackMLBaseObject");
    UA_StatusCode result = UA_STATUSCODE_GOOD;
    // TODO log errors

    /* Add reference from PackMLObjects folder to PackMLBaseObject
     * Browse for PackMLBaseObject beneath ccId and check for HasTypeDefinition =
     * PackMLBaseObjectType
     */
    UA_BrowseDescription bd;
    bd.nodeId = ccId;
    bd.nodeClassMask = UA_NODECLASS_OBJECT;
    bd.referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT);
    bd.includeSubtypes = UA_FALSE;
    bd.browseDirection = UA_BROWSEDIRECTION_FORWARD;
    bd.resultMask = UA_BROWSERESULTMASK_TYPEDEFINITION;
    UA_BrowseResult br = UA_Server_browse(server, 0, &bd);
    result = br.statusCode;
    if(result != UA_STATUSCODE_GOOD || br.referencesSize < 1) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Couldn't find PackMLBaseObject: %s with referencesSize %i",
                     UA_StatusCode_name(br.statusCode), (int)br.referencesSize);
        UA_BrowseResult_clear(&br);
        return;
    }
    UA_ExpandedNodeId baseId = UA_EXPANDEDNODEID_NULL;
    for(int i = 0; i < br.referencesSize; ++i) {
        if(UA_NodeId_equal(&br.references[i].typeDefinition.nodeId,
                           &PACKML_OBJECTSFOLDER)) {
            UA_ExpandedNodeId_copy(&br.references[i].nodeId, &baseId);
            break;
        }
    }
    UA_BrowseResult_clear(&br);
    if(UA_NodeId_isNull(&baseId.nodeId)) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Couldn't find PackMLBaseObject under control component.");
    }
    // Add reference from PackMLObjects folder
    result = UA_Server_addReference(
        server, UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_PACKMLOBJECTS),
        UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT), baseId, true);
    if(result != UA_STATUSCODE_GOOD) {
        return;
    }

    /* Get state machine ids */
    UA_NodeId baseStateMachineId = UA_NODEID_NULL;
    UA_NodeId machineStateId = UA_NODEID_NULL;
    UA_NodeId executeStateId = UA_NODEID_NULL;
    UA_NodeId statusId = UA_NODEID_NULL;
    result |= getChildByBrowsename(server, baseId.nodeId,
                                   UA_QUALIFIEDNAME(NS_PACKML, "BaseStateMachine"),
                                   &baseStateMachineId);
    result |= getChildByBrowsename(server, baseStateMachineId,
                                   UA_QUALIFIEDNAME(NS_PACKML, "MachineState"),
                                   &machineStateId);
    result |= getChildByBrowsename(server, machineStateId,
                                   UA_QUALIFIEDNAME(NS_PACKML, "ExecuteState"),
                                   &executeStateId);
    result |= getChildByBrowsename(server, baseId.nodeId,
                                   UA_QUALIFIEDNAME(NS_PACKML, "Status"), &statusId);
    if(result != UA_STATUSCODE_GOOD) {
        return;
    }
    /* Set state machine node context to ccId for method callbacks */
    UA_NodeId *ccIdCopy = UA_NodeId_new();
    UA_NodeId_copy(&ccId, ccIdCopy);
    UA_Server_setNodeContext(server, baseStateMachineId, ccIdCopy);
    ccIdCopy = UA_NodeId_new();
    UA_NodeId_copy(&ccId, ccIdCopy);
    UA_Server_setNodeContext(server, machineStateId, ccIdCopy);
    ccIdCopy = UA_NodeId_new();
    UA_NodeId_copy(&ccId, ccIdCopy);
    UA_Server_setNodeContext(server, executeStateId, ccIdCopy);

    /* Set AvailableStates and AvailableTransitions according to profile */
    // TODO add other facet values
    if(profile.ES == C3_ES_PACKML) {
        // Full PackML State machine
        // BaseStateMachine
        setStatesAndTransitions(
            server, baseStateMachineId, false, 3,
            (UA_UInt32[]){UA_NS4ID_ABORTED, UA_NS4ID_ABORTING,
                          UA_NS4ID_PACKMLBASESTATEMACHINETYPE_CLEARED});
        setStatesAndTransitions(server, baseStateMachineId, true, 3,
                                (UA_UInt32[]){UA_NS4ID_ABORTEDTOCLEARED,
                                              UA_NS4ID_ABORTINGTOABORTED,
                                              UA_NS4ID_CLEAREDTOABORTING});
        // MachineStateMachine
        setStatesAndTransitions(server, machineStateId, false, 4,
                                (UA_UInt32[]){UA_NS4ID_CLEARING, UA_NS4ID_RUNNING,
                                              UA_NS4ID_STOPPED, UA_NS4ID_STOPPING});
        setStatesAndTransitions(
            server, machineStateId, true, 4,
            (UA_UInt32[]){UA_NS4ID_CLEARINGTOSTOPPED, UA_NS4ID_RUNNINGTOSTOPPING,
                          UA_NS4ID_STOPPEDTORUNNING, UA_NS4ID_STOPPINGTOSTOPPED});
        // ExecuteStateMachine
        setStatesAndTransitions(server, executeStateId, false, 12,
                                (UA_UInt32[]){UA_NS4ID_COMPLETE, UA_NS4ID_COMPLETING,
                                              UA_NS4ID_EXECUTE, UA_NS4ID_HELD,
                                              UA_NS4ID_HOLDING, UA_NS4ID_IDLE,
                                              UA_NS4ID_RESETTING, UA_NS4ID_STARTING,
                                              UA_NS4ID_SUSPENDED, UA_NS4ID_SUSPENDING,
                                              UA_NS4ID_UNHOLDING, UA_NS4ID_UNSUSPENDING});
        setStatesAndTransitions(
            server, executeStateId, true, 19,
            (UA_UInt32[]){UA_NS4ID_COMPLETETORESETTING, UA_NS4ID_COMPLETINGTOCOMPLETE,
                          UA_NS4ID_EXECUTETOCOMPLETING, UA_NS4ID_EXECUTETOHOLDING,
                          UA_NS4ID_EXECUTETOSUSPENDING, UA_NS4ID_HELDTOUNHOLDING,
                          UA_NS4ID_HOLDINGTOHELD, UA_NS4ID_IDLETOSTARTING,
                          UA_NS4ID_RESETTINGTOIDLE, UA_NS4ID_STARTINGTOEXECUTE,
                          UA_NS4ID_STARTINGTOHOLDING, UA_NS4ID_SUSPENDEDTOHOLDING,
                          UA_NS4ID_SUSPENDEDTOUNSUSPENDING, UA_NS4ID_SUSPENDINGTOHOLDING,
                          UA_NS4ID_SUSPENDINGTOSUSPENDED, UA_NS4ID_UNHOLDINGTOEXECUTE,
                          UA_NS4ID_UNHOLDINGTOHOLDING, UA_NS4ID_UNSUSPENDINGTOEXECUTE,
                          UA_NS4ID_UNSUSPENDINGTOHOLDING});
    } else if(profile.ES == C3_ES_BASYS) {
        // BaseStateMachine
        setStatesAndTransitions(
            server, baseStateMachineId, false, 3,
            (UA_UInt32[]){UA_NS4ID_ABORTED, UA_NS4ID_ABORTING,
                          UA_NS4ID_PACKMLBASESTATEMACHINETYPE_CLEARED});
        setStatesAndTransitions(server, baseStateMachineId, true, 3,
                                (UA_UInt32[]){UA_NS4ID_ABORTEDTOCLEARED,
                                              UA_NS4ID_ABORTINGTOABORTED,
                                              UA_NS4ID_CLEAREDTOABORTING});
        // MachineStateMachine
        setStatesAndTransitions(server, machineStateId, false, 4,
                                (UA_UInt32[]){UA_NS4ID_CLEARING, UA_NS4ID_RUNNING,
                                              UA_NS4ID_STOPPED, UA_NS4ID_STOPPING});
        setStatesAndTransitions(
            server, machineStateId, true, 4,
            (UA_UInt32[]){UA_NS4ID_CLEARINGTOSTOPPED, UA_NS4ID_RUNNINGTOSTOPPING,
                          UA_NS4ID_STOPPEDTORUNNING, UA_NS4ID_STOPPINGTOSTOPPED});
        // ExecuteStateMachine
        setStatesAndTransitions(server, executeStateId, false, 6,
                                (UA_UInt32[]){UA_NS4ID_COMPLETE, UA_NS4ID_COMPLETING,
                                              UA_NS4ID_EXECUTE, UA_NS4ID_IDLE,
                                              UA_NS4ID_RESETTING, UA_NS4ID_STARTING});
        setStatesAndTransitions(
            server, executeStateId, true, 6,
            (UA_UInt32[]){UA_NS4ID_COMPLETETORESETTING, UA_NS4ID_COMPLETINGTOCOMPLETE,
                          UA_NS4ID_EXECUTETOCOMPLETING, UA_NS4ID_IDLETOSTARTING,
                          UA_NS4ID_RESETTINGTOIDLE, UA_NS4ID_STARTINGTOEXECUTE});
    }
    /* Connect CurrentState values with STATUS/EXST.
     * Also sets id values under CurrentState.
     */
    UA_NodeId ccStatusId = UA_NODEID_NULL;
    UA_NodeId ccEXSTId = UA_NODEID_NULL;
    result = getChildByBrowsename(server, ccId, UA_QUALIFIEDNAME(NS_PROFILES, "STATUS"),
                                  &ccStatusId);
    result |= getChildByBrowsename(server, ccStatusId,
                                   UA_QUALIFIEDNAME(NS_PROFILES, "EXST"), &ccEXSTId);
    if(result != UA_STATUSCODE_GOOD) {
        return;
    }
    result = setCurrentStateCallback(server, baseStateMachineId, &ccEXSTId, 0);
    result |= setCurrentStateCallback(server, machineStateId, &ccEXSTId, 1);
    result |= setCurrentStateCallback(server, executeStateId, &ccEXSTId, 2);

    /* Set PackML Status/UnitSupportedModes according to EM facet value
     * https://reference.opcfoundation.org/v104/PackML/v100/docs/6.5.2/
     * https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponent#Execution_mode_state_machine
     */
    // TODO Create additional ENUMS for EM combinations and take profile.EM value
    // into account.
    //
    UA_NodeId unitSupportedModesEnum =
        UA_NODEID_NUMERIC(NS_PACKML, UA_NS4ID_PRODUCTIONMAINTENANCEMODEENUM);
    UA_Variant unitSupportedModes;
    UA_Variant_setScalar(&unitSupportedModes, &unitSupportedModesEnum,
                         &UA_TYPES[UA_TYPES_NODEID]);
    result = serverWriteValueByBrowsename(
        server, statusId, UA_QUALIFIEDNAME(NS_PACKML, "UnitSupportedModes"),
        unitSupportedModes);
}
/* Generated from Opc.Ua.PackML.NodeSet2.bsd with script open62541/tools/generate_datatypes.py
 * on host ALTAIR by user julian at 2022-03-09 04:41:55 */

#include "nodeset_packml_generated.h"

/* ProductionMaintenanceModeEnum */
#define ProductionMaintenanceModeEnum_members NULL

/* PackMLCountDataType */
static UA_DataTypeMember PackMLCountDataType_members[5] = {
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("ID") /* .memberName */
},
{
    UA_TYPES_STRING, /* .memberTypeIndex */
    offsetof(UA_PackMLCountDataType, name) - offsetof(UA_PackMLCountDataType, iD) - sizeof(UA_Int32), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Name") /* .memberName */
},
{
    UA_TYPES_EUINFORMATION, /* .memberTypeIndex */
    offsetof(UA_PackMLCountDataType, unit) - offsetof(UA_PackMLCountDataType, name) - sizeof(UA_String), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Unit") /* .memberName */
},
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    offsetof(UA_PackMLCountDataType, count) - offsetof(UA_PackMLCountDataType, unit) - sizeof(UA_EUInformation), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Count") /* .memberName */
},
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    offsetof(UA_PackMLCountDataType, accCount) - offsetof(UA_PackMLCountDataType, count) - sizeof(UA_Int32), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("AccCount") /* .memberName */
},};

/* PackMLDescriptorDataType */
static UA_DataTypeMember PackMLDescriptorDataType_members[4] = {
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("ID") /* .memberName */
},
{
    UA_TYPES_STRING, /* .memberTypeIndex */
    offsetof(UA_PackMLDescriptorDataType, name) - offsetof(UA_PackMLDescriptorDataType, iD) - sizeof(UA_Int32), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Name") /* .memberName */
},
{
    UA_TYPES_EUINFORMATION, /* .memberTypeIndex */
    offsetof(UA_PackMLDescriptorDataType, unit) - offsetof(UA_PackMLDescriptorDataType, name) - sizeof(UA_String), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Unit") /* .memberName */
},
{
    UA_TYPES_FLOAT, /* .memberTypeIndex */
    offsetof(UA_PackMLDescriptorDataType, value) - offsetof(UA_PackMLDescriptorDataType, unit) - sizeof(UA_EUInformation), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Value") /* .memberName */
},};

/* PackMLAlarmDataType */
static UA_DataTypeMember PackMLAlarmDataType_members[7] = {
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("ID") /* .memberName */
},
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    offsetof(UA_PackMLAlarmDataType, value) - offsetof(UA_PackMLAlarmDataType, iD) - sizeof(UA_Int32), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Value") /* .memberName */
},
{
    UA_TYPES_STRING, /* .memberTypeIndex */
    offsetof(UA_PackMLAlarmDataType, message) - offsetof(UA_PackMLAlarmDataType, value) - sizeof(UA_Int32), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Message") /* .memberName */
},
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    offsetof(UA_PackMLAlarmDataType, category) - offsetof(UA_PackMLAlarmDataType, message) - sizeof(UA_String), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Category") /* .memberName */
},
{
    UA_TYPES_DATETIME, /* .memberTypeIndex */
    offsetof(UA_PackMLAlarmDataType, dateTime) - offsetof(UA_PackMLAlarmDataType, category) - sizeof(UA_Int32), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("DateTime") /* .memberName */
},
{
    UA_TYPES_DATETIME, /* .memberTypeIndex */
    offsetof(UA_PackMLAlarmDataType, ackDateTime) - offsetof(UA_PackMLAlarmDataType, dateTime) - sizeof(UA_DateTime), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("AckDateTime") /* .memberName */
},
{
    UA_TYPES_BOOLEAN, /* .memberTypeIndex */
    offsetof(UA_PackMLAlarmDataType, trigger) - offsetof(UA_PackMLAlarmDataType, ackDateTime) - sizeof(UA_DateTime), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Trigger") /* .memberName */
},};

/* PackMLIngredientsDataType */
static UA_DataTypeMember PackMLIngredientsDataType_members[2] = {
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("IngredientID") /* .memberName */
},
{
    UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE, /* .memberTypeIndex */
    offsetof(UA_PackMLIngredientsDataType, parameterSize) - offsetof(UA_PackMLIngredientsDataType, ingredientID) - sizeof(UA_Int32), /* .padding */
    false, /* .namespaceZero */
    true, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Parameter") /* .memberName */
},};

/* PackMLRemoteInterfaceDataType */
static UA_DataTypeMember PackMLRemoteInterfaceDataType_members[4] = {
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Number") /* .memberName */
},
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    offsetof(UA_PackMLRemoteInterfaceDataType, controlCmdNumber) - offsetof(UA_PackMLRemoteInterfaceDataType, number) - sizeof(UA_Int32), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("ControlCmdNumber") /* .memberName */
},
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    offsetof(UA_PackMLRemoteInterfaceDataType, cmdValue) - offsetof(UA_PackMLRemoteInterfaceDataType, controlCmdNumber) - sizeof(UA_Int32), /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("CmdValue") /* .memberName */
},
{
    UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE, /* .memberTypeIndex */
    offsetof(UA_PackMLRemoteInterfaceDataType, parameterSize) - offsetof(UA_PackMLRemoteInterfaceDataType, cmdValue) - sizeof(UA_Int32), /* .padding */
    false, /* .namespaceZero */
    true, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Parameter") /* .memberName */
},};

/* PackMLProductDataType */
static UA_DataTypeMember PackMLProductDataType_members[3] = {
{
    UA_TYPES_INT32, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("ProductID") /* .memberName */
},
{
    UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE, /* .memberTypeIndex */
    offsetof(UA_PackMLProductDataType, processVariablesSize) - offsetof(UA_PackMLProductDataType, productID) - sizeof(UA_Int32), /* .padding */
    false, /* .namespaceZero */
    true, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("ProcessVariables") /* .memberName */
},
{
    UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE, /* .memberTypeIndex */
    offsetof(UA_PackMLProductDataType, ingredientsSize) - offsetof(UA_PackMLProductDataType, processVariables) - sizeof(void *), /* .padding */
    false, /* .namespaceZero */
    true, /* .isArray */
    false  /* .isOptional */
    UA_TYPENAME("Ingredients") /* .memberName */
},};
const UA_DataType UA_NODESET_PACKML[UA_NODESET_PACKML_COUNT] = {
/* ProductionMaintenanceModeEnum */
{
    {4, UA_NODEIDTYPE_NUMERIC, {11LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {0}}, /* .binaryEncodingId */
    sizeof(UA_ProductionMaintenanceModeEnum), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    ProductionMaintenanceModeEnum_members  /* .members */
    UA_TYPENAME("ProductionMaintenanceModeEnum") /* .typeName */
},
/* PackMLCountDataType */
{
    {4, UA_NODEIDTYPE_NUMERIC, {14LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {69LU}}, /* .binaryEncodingId */
    sizeof(UA_PackMLCountDataType), /* .memSize */
    UA_NODESET_PACKML_PACKMLCOUNTDATATYPE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    false, /* .pointerFree */
    false, /* .overlayable */
    5, /* .membersSize */
    PackMLCountDataType_members  /* .members */
    UA_TYPENAME("PackMLCountDataType") /* .typeName */
},
/* PackMLDescriptorDataType */
{
    {4, UA_NODEIDTYPE_NUMERIC, {16LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {77LU}}, /* .binaryEncodingId */
    sizeof(UA_PackMLDescriptorDataType), /* .memSize */
    UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    false, /* .pointerFree */
    false, /* .overlayable */
    4, /* .membersSize */
    PackMLDescriptorDataType_members  /* .members */
    UA_TYPENAME("PackMLDescriptorDataType") /* .typeName */
},
/* PackMLAlarmDataType */
{
    {4, UA_NODEIDTYPE_NUMERIC, {15LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {74LU}}, /* .binaryEncodingId */
    sizeof(UA_PackMLAlarmDataType), /* .memSize */
    UA_NODESET_PACKML_PACKMLALARMDATATYPE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    false, /* .pointerFree */
    false, /* .overlayable */
    7, /* .membersSize */
    PackMLAlarmDataType_members  /* .members */
    UA_TYPENAME("PackMLAlarmDataType") /* .typeName */
},
/* PackMLIngredientsDataType */
{
    {4, UA_NODEIDTYPE_NUMERIC, {17LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {79LU}}, /* .binaryEncodingId */
    sizeof(UA_PackMLIngredientsDataType), /* .memSize */
    UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    false, /* .pointerFree */
    false, /* .overlayable */
    2, /* .membersSize */
    PackMLIngredientsDataType_members  /* .members */
    UA_TYPENAME("PackMLIngredientsDataType") /* .typeName */
},
/* PackMLRemoteInterfaceDataType */
{
    {4, UA_NODEIDTYPE_NUMERIC, {19LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {83LU}}, /* .binaryEncodingId */
    sizeof(UA_PackMLRemoteInterfaceDataType), /* .memSize */
    UA_NODESET_PACKML_PACKMLREMOTEINTERFACEDATATYPE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    false, /* .pointerFree */
    false, /* .overlayable */
    4, /* .membersSize */
    PackMLRemoteInterfaceDataType_members  /* .members */
    UA_TYPENAME("PackMLRemoteInterfaceDataType") /* .typeName */
},
/* PackMLProductDataType */
{
    {4, UA_NODEIDTYPE_NUMERIC, {18LU}}, /* .typeId */
    {4, UA_NODEIDTYPE_NUMERIC, {81LU}}, /* .binaryEncodingId */
    sizeof(UA_PackMLProductDataType), /* .memSize */
    UA_NODESET_PACKML_PACKMLPRODUCTDATATYPE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    false, /* .pointerFree */
    false, /* .overlayable */
    3, /* .membersSize */
    PackMLProductDataType_members  /* .members */
    UA_TYPENAME("PackMLProductDataType") /* .typeName */
},
};


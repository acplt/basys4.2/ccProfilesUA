/*
 ============================================================================
 Name        : profile_utilities.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Utility functions. Practical wrappers for OPC UA functions.
 ============================================================================
 */
#ifndef PROFILE_UTILITIES_H
#define PROFILE_UTILITIES_H

#include <C3_Profiles.h>

// open62541 includes
#ifdef UA_ENABLE_AMALGAMATION
#include <open62541.h>  //Not tested yet
#else
#include <open62541/plugin/log_stdout.h>
#include <open62541/server.h>
#include <open62541/server_config_default.h>
#include <open62541/types.h>
#include <open62541/util.h>
#endif

/*
 * Information model constants
 * The namespace NS_PROFILES is uses for the specification of the basys control component
 * profiles information model. To keep the node ids of the information model stable and
 * organized, some values and factors are defined here. These ids are defined by basys and
 * are unique within the basys information model for control component profiles. A
 * companion spec may be derived from this nodeset here.
 * //TODO export to UA XML files
 * //TODO create own Namespace for modelling rules, as they are meta information
 * and not necessary for the types itself
 *
 * The NS_APPLICATION namespace is used for all instances of control components. Their
 * node ids are arbitrary. So every UA Server or corresponding application has the right
 * to define the nodeIDs in his way. The nodeIds are unique within server scope.
 *
 * The NS_DSTYPES is a dummy namespace for domain specific definitons like operation
 * modes(skills) and the selection of used profiles in the domain or company. These may be
 * definied in other companion specs later.
 *
 * //TODO add and check for cross references to existing companion specs, like DI or
 * PACKML.
 */
#define NS_PROFILES_URI "basys40.de/cc/profiles/"
// Namespace indices
#define NS_APPLICATION 1  // Application instances
extern UA_UInt16 NS_PROFILES;  // Profile types and interfaces, set in profile_example.c
                        // during createControlComponentEnvironment
extern UA_UInt16 NS_DSTYPES;   // Domain specific types, set in profile_example.c during
                        // createControlComponentEnvironment

// NS_APPLICATION: Examples namespace
#define ID_DUMMYCCFOLDER                                                                 \
    UA_NODEID_NUMERIC(NS_APPLICATION, 100000)  // Folder for dummy instances

// NS_PROFILES: Profiles namespace
#define NS2OFFSET_CCOBJECTYPES 100000      // Start value for object types
#define NS2OFFSET_CCINTERFACETYPES 200000  // Start value for interface types
#define NS2OFFSET_CCVARIABLETYPES 300000   // Start value for variable types
#define NS2OFFSET_CCMODELLINGRULES 400000  // Start value for modelling rules
#define NS2FACTOR_PROFILES                                                               \
    1000  // Factor to differentiate si profile interface and object types
#define NS2FACTOR_FOLDER 100  // Factor to differentiate folders or subcomponent objects
// Node ids of variabel types
#define ID_CCSTATUSTYPE                                                                  \
    UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCVARIABLETYPES + NS2FACTOR_FOLDER)
#define ID_CCPROFILETYPE                                                                 \
    UA_NODEID_NUMERIC(NS_PROFILES, NS2OFFSET_CCVARIABLETYPES + 2 * NS2FACTOR_FOLDER)

// NS_DSTYPES: Domain specific types namespace
#define NS3OFFSET_DUMMYCCTYPES 10000  // Start value for example object types
#define NS3FACTOR_EXAMPLES 1000       // Factor to differentiate example object types

/* Create all optional nodes regardles of profile or facet */
#ifdef __cplusplus
extern bool CreateAllOptionalNodes;
extern C3_Profile requestedExampleProfile;
#else
/* Create all optional nodes regardles of profile or facet */
bool CreateAllOptionalNodes;
/* Option to define the desired profile, which is used to create optional child nodes*/
C3_Profile requestedExampleProfile;
#endif

/* Print a NodeId in logs */
#define UA_NODEID_WRAP(NODEID, INPUT)                                                    \
    {                                                                                    \
        UA_String nodeIdStr = UA_STRING_NULL;                                            \
        UA_NodeId_print(NODEID, &nodeIdStr);                                             \
        INPUT;                                                                           \
        UA_String_clear(&nodeIdStr);                                                     \
    }

/*
 * Specific node management functions to build up the information model
 */
UA_StatusCode
markMandatory(UA_Server *server, UA_NodeId id, bool mandatory,
              bool useCustomModellingRule);

UA_StatusCode
markInterface(UA_Server *server, UA_NodeId id, C3_SI_FACET profile);

UA_StatusCode
markFacetModellingRule(UA_Server *server, UA_NodeId id, FACET_GROUP facetGroupNo,
                       int *facetValues, int facetValuesSize);

UA_StatusCode
addStringProperty(UA_Server *server, char *browsename, char *displayname,
                  char *description, UA_NodeId id, UA_NodeId parentId, const char *value,
                  UA_Boolean writeable);

UA_StatusCode
addStringArrayProperty(UA_Server *server, char *browsename, char *displayname,
                       char *description, UA_NodeId id, UA_NodeId parentId,
                       const char **value, size_t arraySize, UA_Boolean writeable);

void
appendStringArray(UA_Server *server, UA_NodeId id, char **values, size_t valuesSize);

UA_StatusCode
addVariableCallback(UA_Server *server, UA_NodeId id, void *read, void *write);

UA_StatusCode
addFolder(UA_Server *server, char *browsename, char *displayname, char *description,
          UA_NodeId id, UA_NodeId parentId, UA_Boolean asComponent);

UA_StatusCode
addObjectType(UA_Server *server, char *browsename, char *displayname, char *description,
              UA_NodeId id, UA_NodeId parentId);

/*
 * Utility functions for browsing and writing values
 */
UA_StatusCode
getParent(UA_Server *server, const UA_NodeId *nodeId, UA_NodeId *parentId,
          UA_QualifiedName *parentBrowseName);

UA_StatusCode
getChildByBrowsename(UA_Server *server, const UA_NodeId parentId,
                     const UA_QualifiedName childBrowseName, UA_NodeId *childIdOut);

UA_StatusCode
serverWriteValueByBrowsename(UA_Server *server, const UA_NodeId startingNode,
                             const UA_QualifiedName browseName, UA_Variant value);

/*
 * Callback for all method calls
 */
static UA_StatusCode
methodCallback(UA_Server *server, const UA_NodeId *sessionId, void *sessionHandle,
               const UA_NodeId *methodId, void *methodContext, const UA_NodeId *objectId,
               void *objectContext, size_t inputSize, const UA_Variant *input,
               size_t outputSize, UA_Variant *output);

UA_StatusCode
addMethod(UA_Server *server, char *browsename, char *displayname, char *description,
          UA_NodeId id, UA_NodeId parentId, UA_MethodCallback callback);

void
setServerConfig(UA_Server *server, const char *applicationName,
                const char *applicationUri, UA_UInt16 port, UA_LogLevel loglevel);

#endif /* PROFILE_UTILITIES_H */

/*
 ============================================================================
 Name        : profile_examples.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Functions to add OPC UA nodes for example control components
 ============================================================================
 */
#ifndef PROFILE_EXAMPLES_H
#define PROFILE_EXAMPLES_H

#include <profile_utilities.h>

typedef enum {
    EXAMPLE_NONE,
    EXAMPLE_PUMP,       // Simple Pump, with one operation mode
    EXAMPLE_PICKPLACE,  // Pick & Place unit with some operation modes
} EXAMPLE;
static const char *EXAMPLESTRING[] = {"", "PUMP", "PICKPLACE"};
static const char *EXAMPLESTRING_CASE[] = {"", "Pump", "PickPlace"};
static const char *EXAMPLESTRING_LOWER[] = {"", "pump", "pickplace"};

/* Definition of working states for the operation modes */
static const char *EXAMPLE_NONE_DEFAULT_WORKSTATES[4] = {"READY", "Init",
                                                         "ExecuteNothing", "DeInit"};
static const char *EXAMPLE_PUMP_PUMP_WORKSTATES[7] = {
    "NotPumping",  "RampUpPumpSpeed", "Pumping",          "Pumping,25%",
    "Pumping,50%", "Pumping,75%",     "RampDownPumpSpeed"};

static const char *EXAMPLE_PICKPLACE_PICKPLACE_WORKSTATES[7] = {
    "NotMoving",         "GoToHomePosition", "GoToPickupPosition", "Grasp",
    "GoToPlacePosition", "OpenGripper",      "GoToHomePosition"};
static const char *EXAMPLE_PICKPLACE_GRASP_WORKSTATES[5] = {
    "Wait", "UnlockGripper", "Calibrate", "CloseTillPressureReached", "LockGripper"};
static const char *EXAMPLE_PICKPLACE_OPEN_WORKSTATES[4] = {
    "Wait", "UnlockGripper", "SetLowPressure", "LockGripper"};
static const char *EXAMPLE_PICKPLACE_CLOSE_WORKSTATES[4] = {
    "Wait", "UnlockGripper", "SetHighPressure", "LockGripper"};

/* Create a single control component dummy with specified profile and example */
void
createControlComponentExample(UA_Server *server, C3_Profile profile, EXAMPLE example,
                              UA_UInt16 componentNo);

/* Create combinations of different profiles and examples
 * // TODO Create all usefull combinations of profile values and make example a CLI
 * parameter
 */
void
createControlComponentsForAllProfiles(UA_Server *server, int example,
                                      bool createOptional);

#endif /* PROFILE_EXAMPLES_H */
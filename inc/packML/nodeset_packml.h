/* WARNING: This is a generated file.
 * Any manual changes will be overwritten. */

#ifndef NODESET_PACKML_H_
#define NODESET_PACKML_H_


#ifdef UA_ENABLE_AMALGAMATION
# include "open62541.h"
#else
# include <open62541/server.h>
#endif
#include "nodeset_packml_generated.h"



_UA_BEGIN_DECLS

extern UA_StatusCode nodeset_packml(UA_Server *server);

_UA_END_DECLS

#endif /* NODESET_PACKML_H_ */

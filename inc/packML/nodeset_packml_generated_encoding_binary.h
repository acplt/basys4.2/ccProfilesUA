/* Generated from Opc.Ua.PackML.NodeSet2.bsd with script open62541/tools/generate_datatypes.py
 * on host ALTAIR by user julian at 2022-03-09 04:41:55 */

#ifndef NODESET_PACKML_GENERATED_ENCODING_BINARY_H_
#define NODESET_PACKML_GENERATED_ENCODING_BINARY_H_

#ifdef UA_ENABLE_AMALGAMATION
# include "open62541.h"
#else
# include "ua_types_encoding_binary.h"
# include "nodeset_packml_generated.h"
#endif



/* ProductionMaintenanceModeEnum */
static UA_INLINE size_t
UA_ProductionMaintenanceModeEnum_calcSizeBinary(const UA_ProductionMaintenanceModeEnum *src) {
    return UA_calcSizeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PRODUCTIONMAINTENANCEMODEENUM]);
}
static UA_INLINE UA_StatusCode
UA_ProductionMaintenanceModeEnum_encodeBinary(const UA_ProductionMaintenanceModeEnum *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PRODUCTIONMAINTENANCEMODEENUM], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_ProductionMaintenanceModeEnum_decodeBinary(const UA_ByteString *src, size_t *offset, UA_ProductionMaintenanceModeEnum *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PRODUCTIONMAINTENANCEMODEENUM], NULL);
}

/* PackMLCountDataType */
static UA_INLINE size_t
UA_PackMLCountDataType_calcSizeBinary(const UA_PackMLCountDataType *src) {
    return UA_calcSizeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLCOUNTDATATYPE]);
}
static UA_INLINE UA_StatusCode
UA_PackMLCountDataType_encodeBinary(const UA_PackMLCountDataType *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLCOUNTDATATYPE], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_PackMLCountDataType_decodeBinary(const UA_ByteString *src, size_t *offset, UA_PackMLCountDataType *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLCOUNTDATATYPE], NULL);
}

/* PackMLDescriptorDataType */
static UA_INLINE size_t
UA_PackMLDescriptorDataType_calcSizeBinary(const UA_PackMLDescriptorDataType *src) {
    return UA_calcSizeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE]);
}
static UA_INLINE UA_StatusCode
UA_PackMLDescriptorDataType_encodeBinary(const UA_PackMLDescriptorDataType *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_PackMLDescriptorDataType_decodeBinary(const UA_ByteString *src, size_t *offset, UA_PackMLDescriptorDataType *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE], NULL);
}

/* PackMLAlarmDataType */
static UA_INLINE size_t
UA_PackMLAlarmDataType_calcSizeBinary(const UA_PackMLAlarmDataType *src) {
    return UA_calcSizeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLALARMDATATYPE]);
}
static UA_INLINE UA_StatusCode
UA_PackMLAlarmDataType_encodeBinary(const UA_PackMLAlarmDataType *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLALARMDATATYPE], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_PackMLAlarmDataType_decodeBinary(const UA_ByteString *src, size_t *offset, UA_PackMLAlarmDataType *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLALARMDATATYPE], NULL);
}

/* PackMLIngredientsDataType */
static UA_INLINE size_t
UA_PackMLIngredientsDataType_calcSizeBinary(const UA_PackMLIngredientsDataType *src) {
    return UA_calcSizeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE]);
}
static UA_INLINE UA_StatusCode
UA_PackMLIngredientsDataType_encodeBinary(const UA_PackMLIngredientsDataType *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_PackMLIngredientsDataType_decodeBinary(const UA_ByteString *src, size_t *offset, UA_PackMLIngredientsDataType *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE], NULL);
}

/* PackMLRemoteInterfaceDataType */
static UA_INLINE size_t
UA_PackMLRemoteInterfaceDataType_calcSizeBinary(const UA_PackMLRemoteInterfaceDataType *src) {
    return UA_calcSizeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLREMOTEINTERFACEDATATYPE]);
}
static UA_INLINE UA_StatusCode
UA_PackMLRemoteInterfaceDataType_encodeBinary(const UA_PackMLRemoteInterfaceDataType *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLREMOTEINTERFACEDATATYPE], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_PackMLRemoteInterfaceDataType_decodeBinary(const UA_ByteString *src, size_t *offset, UA_PackMLRemoteInterfaceDataType *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLREMOTEINTERFACEDATATYPE], NULL);
}

/* PackMLProductDataType */
static UA_INLINE size_t
UA_PackMLProductDataType_calcSizeBinary(const UA_PackMLProductDataType *src) {
    return UA_calcSizeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLPRODUCTDATATYPE]);
}
static UA_INLINE UA_StatusCode
UA_PackMLProductDataType_encodeBinary(const UA_PackMLProductDataType *src, UA_Byte **bufPos, const UA_Byte *bufEnd) {
    return UA_encodeBinary(src, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLPRODUCTDATATYPE], bufPos, &bufEnd, NULL, NULL);
}
static UA_INLINE UA_StatusCode
UA_PackMLProductDataType_decodeBinary(const UA_ByteString *src, size_t *offset, UA_PackMLProductDataType *dst) {
    return UA_decodeBinary(src, offset, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLPRODUCTDATATYPE], NULL);
}

#endif /* NODESET_PACKML_GENERATED_ENCODING_BINARY_H_ */

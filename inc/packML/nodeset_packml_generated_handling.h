/* Generated from Opc.Ua.PackML.NodeSet2.bsd with script open62541/tools/generate_datatypes.py
 * on host ALTAIR by user julian at 2022-03-09 04:41:55 */

#ifndef NODESET_PACKML_GENERATED_HANDLING_H_
#define NODESET_PACKML_GENERATED_HANDLING_H_

#include "nodeset_packml_generated.h"

_UA_BEGIN_DECLS

#if defined(__GNUC__) && __GNUC__ >= 4 && __GNUC_MINOR__ >= 6
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wmissing-field-initializers"
# pragma GCC diagnostic ignored "-Wmissing-braces"
#endif


/* ProductionMaintenanceModeEnum */
static UA_INLINE void
UA_ProductionMaintenanceModeEnum_init(UA_ProductionMaintenanceModeEnum *p) {
    memset(p, 0, sizeof(UA_ProductionMaintenanceModeEnum));
}

static UA_INLINE UA_ProductionMaintenanceModeEnum *
UA_ProductionMaintenanceModeEnum_new(void) {
    return (UA_ProductionMaintenanceModeEnum*)UA_new(&UA_NODESET_PACKML[UA_NODESET_PACKML_PRODUCTIONMAINTENANCEMODEENUM]);
}

static UA_INLINE UA_StatusCode
UA_ProductionMaintenanceModeEnum_copy(const UA_ProductionMaintenanceModeEnum *src, UA_ProductionMaintenanceModeEnum *dst) {
    return UA_copy(src, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PRODUCTIONMAINTENANCEMODEENUM]);
}

UA_DEPRECATED static UA_INLINE void
UA_ProductionMaintenanceModeEnum_deleteMembers(UA_ProductionMaintenanceModeEnum *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PRODUCTIONMAINTENANCEMODEENUM]);
}

static UA_INLINE void
UA_ProductionMaintenanceModeEnum_clear(UA_ProductionMaintenanceModeEnum *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PRODUCTIONMAINTENANCEMODEENUM]);
}

static UA_INLINE void
UA_ProductionMaintenanceModeEnum_delete(UA_ProductionMaintenanceModeEnum *p) {
    UA_delete(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PRODUCTIONMAINTENANCEMODEENUM]);
}

/* PackMLCountDataType */
static UA_INLINE void
UA_PackMLCountDataType_init(UA_PackMLCountDataType *p) {
    memset(p, 0, sizeof(UA_PackMLCountDataType));
}

static UA_INLINE UA_PackMLCountDataType *
UA_PackMLCountDataType_new(void) {
    return (UA_PackMLCountDataType*)UA_new(&UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLCOUNTDATATYPE]);
}

static UA_INLINE UA_StatusCode
UA_PackMLCountDataType_copy(const UA_PackMLCountDataType *src, UA_PackMLCountDataType *dst) {
    return UA_copy(src, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLCOUNTDATATYPE]);
}

UA_DEPRECATED static UA_INLINE void
UA_PackMLCountDataType_deleteMembers(UA_PackMLCountDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLCOUNTDATATYPE]);
}

static UA_INLINE void
UA_PackMLCountDataType_clear(UA_PackMLCountDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLCOUNTDATATYPE]);
}

static UA_INLINE void
UA_PackMLCountDataType_delete(UA_PackMLCountDataType *p) {
    UA_delete(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLCOUNTDATATYPE]);
}

/* PackMLDescriptorDataType */
static UA_INLINE void
UA_PackMLDescriptorDataType_init(UA_PackMLDescriptorDataType *p) {
    memset(p, 0, sizeof(UA_PackMLDescriptorDataType));
}

static UA_INLINE UA_PackMLDescriptorDataType *
UA_PackMLDescriptorDataType_new(void) {
    return (UA_PackMLDescriptorDataType*)UA_new(&UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE]);
}

static UA_INLINE UA_StatusCode
UA_PackMLDescriptorDataType_copy(const UA_PackMLDescriptorDataType *src, UA_PackMLDescriptorDataType *dst) {
    return UA_copy(src, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE]);
}

UA_DEPRECATED static UA_INLINE void
UA_PackMLDescriptorDataType_deleteMembers(UA_PackMLDescriptorDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE]);
}

static UA_INLINE void
UA_PackMLDescriptorDataType_clear(UA_PackMLDescriptorDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE]);
}

static UA_INLINE void
UA_PackMLDescriptorDataType_delete(UA_PackMLDescriptorDataType *p) {
    UA_delete(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE]);
}

/* PackMLAlarmDataType */
static UA_INLINE void
UA_PackMLAlarmDataType_init(UA_PackMLAlarmDataType *p) {
    memset(p, 0, sizeof(UA_PackMLAlarmDataType));
}

static UA_INLINE UA_PackMLAlarmDataType *
UA_PackMLAlarmDataType_new(void) {
    return (UA_PackMLAlarmDataType*)UA_new(&UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLALARMDATATYPE]);
}

static UA_INLINE UA_StatusCode
UA_PackMLAlarmDataType_copy(const UA_PackMLAlarmDataType *src, UA_PackMLAlarmDataType *dst) {
    return UA_copy(src, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLALARMDATATYPE]);
}

UA_DEPRECATED static UA_INLINE void
UA_PackMLAlarmDataType_deleteMembers(UA_PackMLAlarmDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLALARMDATATYPE]);
}

static UA_INLINE void
UA_PackMLAlarmDataType_clear(UA_PackMLAlarmDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLALARMDATATYPE]);
}

static UA_INLINE void
UA_PackMLAlarmDataType_delete(UA_PackMLAlarmDataType *p) {
    UA_delete(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLALARMDATATYPE]);
}

/* PackMLIngredientsDataType */
static UA_INLINE void
UA_PackMLIngredientsDataType_init(UA_PackMLIngredientsDataType *p) {
    memset(p, 0, sizeof(UA_PackMLIngredientsDataType));
}

static UA_INLINE UA_PackMLIngredientsDataType *
UA_PackMLIngredientsDataType_new(void) {
    return (UA_PackMLIngredientsDataType*)UA_new(&UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE]);
}

static UA_INLINE UA_StatusCode
UA_PackMLIngredientsDataType_copy(const UA_PackMLIngredientsDataType *src, UA_PackMLIngredientsDataType *dst) {
    return UA_copy(src, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE]);
}

UA_DEPRECATED static UA_INLINE void
UA_PackMLIngredientsDataType_deleteMembers(UA_PackMLIngredientsDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE]);
}

static UA_INLINE void
UA_PackMLIngredientsDataType_clear(UA_PackMLIngredientsDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE]);
}

static UA_INLINE void
UA_PackMLIngredientsDataType_delete(UA_PackMLIngredientsDataType *p) {
    UA_delete(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE]);
}

/* PackMLRemoteInterfaceDataType */
static UA_INLINE void
UA_PackMLRemoteInterfaceDataType_init(UA_PackMLRemoteInterfaceDataType *p) {
    memset(p, 0, sizeof(UA_PackMLRemoteInterfaceDataType));
}

static UA_INLINE UA_PackMLRemoteInterfaceDataType *
UA_PackMLRemoteInterfaceDataType_new(void) {
    return (UA_PackMLRemoteInterfaceDataType*)UA_new(&UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLREMOTEINTERFACEDATATYPE]);
}

static UA_INLINE UA_StatusCode
UA_PackMLRemoteInterfaceDataType_copy(const UA_PackMLRemoteInterfaceDataType *src, UA_PackMLRemoteInterfaceDataType *dst) {
    return UA_copy(src, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLREMOTEINTERFACEDATATYPE]);
}

UA_DEPRECATED static UA_INLINE void
UA_PackMLRemoteInterfaceDataType_deleteMembers(UA_PackMLRemoteInterfaceDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLREMOTEINTERFACEDATATYPE]);
}

static UA_INLINE void
UA_PackMLRemoteInterfaceDataType_clear(UA_PackMLRemoteInterfaceDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLREMOTEINTERFACEDATATYPE]);
}

static UA_INLINE void
UA_PackMLRemoteInterfaceDataType_delete(UA_PackMLRemoteInterfaceDataType *p) {
    UA_delete(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLREMOTEINTERFACEDATATYPE]);
}

/* PackMLProductDataType */
static UA_INLINE void
UA_PackMLProductDataType_init(UA_PackMLProductDataType *p) {
    memset(p, 0, sizeof(UA_PackMLProductDataType));
}

static UA_INLINE UA_PackMLProductDataType *
UA_PackMLProductDataType_new(void) {
    return (UA_PackMLProductDataType*)UA_new(&UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLPRODUCTDATATYPE]);
}

static UA_INLINE UA_StatusCode
UA_PackMLProductDataType_copy(const UA_PackMLProductDataType *src, UA_PackMLProductDataType *dst) {
    return UA_copy(src, dst, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLPRODUCTDATATYPE]);
}

UA_DEPRECATED static UA_INLINE void
UA_PackMLProductDataType_deleteMembers(UA_PackMLProductDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLPRODUCTDATATYPE]);
}

static UA_INLINE void
UA_PackMLProductDataType_clear(UA_PackMLProductDataType *p) {
    UA_clear(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLPRODUCTDATATYPE]);
}

static UA_INLINE void
UA_PackMLProductDataType_delete(UA_PackMLProductDataType *p) {
    UA_delete(p, &UA_NODESET_PACKML[UA_NODESET_PACKML_PACKMLPRODUCTDATATYPE]);
}

#if defined(__GNUC__) && __GNUC__ >= 4 && __GNUC_MINOR__ >= 6
# pragma GCC diagnostic pop
#endif

_UA_END_DECLS

#endif /* NODESET_PACKML_GENERATED_HANDLING_H_ */

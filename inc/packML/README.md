# HowTo Generate PackML Nodeset
1. Update nodeset files from https://github.com/OPCFoundation/UA-Nodeset via git submodule open62541:  
`git submodule update --recursive`
   * Better: use newest version from UA-Nodeset repository or pull submodule
   * Nodeset can be found in *open62541\deps\ua-nodeset\PackML*
2. Generate types bsd file if missing (Maybe fixed in future releases of PackML nodeset)
   * Import XML Nodeset file in UAModeler and export nodeset
3. Move final nodeset files (xml, bsd, csv) to nodeset folder *inc/packML/nodset*
4. Generate nodes:
   * From repository root:  
`python open62541/tools/nodeset_compiler/nodeset_compiler.py --types-array=UA_TYPES --types-array=UA_NODESET_PACKML --existing open62541/deps/ua-nodeset/Schema/Opc.Ua.NodeSet2.xml --xml inc/packML/nodeset/Opc.Ua.PackML.NodeSet2.xml src/packML/nodeset_packml`
   * Move header (*src/packML/nodeset_packml.h*) to *inc/packML*
5. Generate datatypes:
   * From repository root:  
`python open62541/tools/generate_datatypes.py -c inc/packML/nodeset/Opc.Ua.PackML.NodeSet2.csv -t inc/packML/nodeset/Opc.Ua.PackML.NodeSet2.bsd --import=UA_TYPES#open62541/deps/ua-nodeset/Schema/Opc.Ua.Types.bsd --namespaceMap=4:http://opcfoundation.org/UA/PackML/ src/packML/nodeset_packml`
   * Move header to *inc/packML*:
     * *src/packML/nodeset_packml_generated.h*
     * *src/packML/nodeset_packml_generated_encoding_binary.h*
     * *src/packML/nodeset_packml_generated_handling.h*
6. Generate nodeIds:
   * From repository root:  
`python open62541/tools/generate_nodeid_header.py inc/packML/nodeset/Opc.Ua.PackML.NodeSet2.csv inc/packML/nodeset_packml_ids NS4`
   * Replace long prefixes with *UA_NS4ID_*:
     * UA_NS4ID_ROOTFOLDER_TYPESFOLDER_OBJECTTYPESFOLDER_BASEOBJECTTYPE_STATEMACHINETYPE_FINITESTATEMACHINETYPE_PACKMLEXECUTESTATEMACHINETYPE_
     * UA_NS4ID_ROOTFOLDER_TYPESFOLDER_OBJECTTYPESFOLDER_BASEOBJECTTYPE_STATEMACHINETYPE_FINITESTATEMACHINETYPE_PACKMLMACHINESTATEMACHINETYPE_
     * UA_NS4ID_ROOTFOLDER_TYPESFOLDER_OBJECTTYPESFOLDER_BASEOBJECTTYPE_STATEMACHINETYPE_FINITESTATEMACHINETYPE_PACKMLBASESTATEMACHINETYPE_
   * Rename `UA_NS4ID_RESET 361` to `UA_NS4ID_RESET_COMPLETE 361` to avoid duplicates

## Links
* [Open62541 Tutorial](https://open62541.org/doc/1.1/nodeset_compiler.html)
* [OPC UA PackML Nodeset](https://github.com/OPCFoundation/UA-Nodeset/tree/v1.04/PackML)
* [OPC UA PackML Companion Spec](https://opcfoundation.org/developer-tools/specifications-opc-ua-information-models/opc-ua-packml-companion-specification)
* [PackML Automat Demo in Excel](http://omac.org/wp-content/uploads/2015/12/PackML_Demo_Rev8.xls)

## ToDo
* [ ] Update submodule ua-nodeset of open62541 and use nodeset from *open62541/deps/ua-nodeset/PackML/*
* [ ] Automatically build nodeset during build (using open62541 CMake functions?)

## Troubleshooting
* if UA_TYPES_IDTYPE can't be found, the UA NS0 Nodeset is not set to FULL. Make sure the cmake option UA_NAMESPACE_ZERO is set to FULL.
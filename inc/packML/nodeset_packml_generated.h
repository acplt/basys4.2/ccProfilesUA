/* Generated from Opc.Ua.PackML.NodeSet2.bsd with script open62541/tools/generate_datatypes.py * on host ALTAIR by user julian at 2022-03-09 04:41:55 */

#ifndef NODESET_PACKML_GENERATED_H_
#define NODESET_PACKML_GENERATED_H_

#ifdef UA_ENABLE_AMALGAMATION
#include "open62541.h"
#else
#include <open62541/types.h>
#include <open62541/types_generated.h>

#endif

_UA_BEGIN_DECLS


/**
 * Every type is assigned an index in an array containing the type descriptions.
 * These descriptions are used during type handling (copying, deletion,
 * binary encoding, ...). */
#define UA_NODESET_PACKML_COUNT 7
extern UA_EXPORT const UA_DataType UA_NODESET_PACKML[UA_NODESET_PACKML_COUNT];

/**
 * ProductionMaintenanceModeEnum
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 */
typedef enum {
    UA_PRODUCTIONMAINTENANCEMODEENUM_INVALID = 0,
    UA_PRODUCTIONMAINTENANCEMODEENUM_PRODUCE = 1,
    UA_PRODUCTIONMAINTENANCEMODEENUM_MAINTENANCE = 2,
    UA_PRODUCTIONMAINTENANCEMODEENUM_MANUAL = 3,
    __UA_PRODUCTIONMAINTENANCEMODEENUM_FORCE32BIT = 0x7fffffff
} UA_ProductionMaintenanceModeEnum;
UA_STATIC_ASSERT(sizeof(UA_ProductionMaintenanceModeEnum) == sizeof(UA_Int32), enum_must_be_32bit);

#define UA_NODESET_PACKML_PRODUCTIONMAINTENANCEMODEENUM 0

/**
 * PackMLCountDataType
 * ^^^^^^^^^^^^^^^^^^^
 */
typedef struct {
    UA_Int32 iD;
    UA_String name;
    UA_EUInformation unit;
    UA_Int32 count;
    UA_Int32 accCount;
} UA_PackMLCountDataType;

#define UA_NODESET_PACKML_PACKMLCOUNTDATATYPE 1

/**
 * PackMLDescriptorDataType
 * ^^^^^^^^^^^^^^^^^^^^^^^^
 */
typedef struct {
    UA_Int32 iD;
    UA_String name;
    UA_EUInformation unit;
    UA_Float value;
} UA_PackMLDescriptorDataType;

#define UA_NODESET_PACKML_PACKMLDESCRIPTORDATATYPE 2

/**
 * PackMLAlarmDataType
 * ^^^^^^^^^^^^^^^^^^^
 */
typedef struct {
    UA_Int32 iD;
    UA_Int32 value;
    UA_String message;
    UA_Int32 category;
    UA_DateTime dateTime;
    UA_DateTime ackDateTime;
    UA_Boolean trigger;
} UA_PackMLAlarmDataType;

#define UA_NODESET_PACKML_PACKMLALARMDATATYPE 3

/**
 * PackMLIngredientsDataType
 * ^^^^^^^^^^^^^^^^^^^^^^^^^
 */
typedef struct {
    UA_Int32 ingredientID;
    size_t parameterSize;
    UA_PackMLDescriptorDataType *parameter;
} UA_PackMLIngredientsDataType;

#define UA_NODESET_PACKML_PACKMLINGREDIENTSDATATYPE 4

/**
 * PackMLRemoteInterfaceDataType
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 */
typedef struct {
    UA_Int32 number;
    UA_Int32 controlCmdNumber;
    UA_Int32 cmdValue;
    size_t parameterSize;
    UA_PackMLDescriptorDataType *parameter;
} UA_PackMLRemoteInterfaceDataType;

#define UA_NODESET_PACKML_PACKMLREMOTEINTERFACEDATATYPE 5

/**
 * PackMLProductDataType
 * ^^^^^^^^^^^^^^^^^^^^^
 */
typedef struct {
    UA_Int32 productID;
    size_t processVariablesSize;
    UA_PackMLDescriptorDataType *processVariables;
    size_t ingredientsSize;
    UA_PackMLIngredientsDataType *ingredients;
} UA_PackMLProductDataType;

#define UA_NODESET_PACKML_PACKMLPRODUCTDATATYPE 6


_UA_END_DECLS

#endif /* NODESET_PACKML_GENERATED_H_ */

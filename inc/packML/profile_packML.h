/*
 ============================================================================
 Name        : profile_packML.h
 Author      : Gerrit Fachinger, Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : PackML frontend for the control component. Specifies functions
               to load the PackML nodeset and instanciate the stateMachines.
 ============================================================================
 */
#include <profile_utilities.h>

#include <nodeset_packml_ids.h>

#define NS_PACKML 4
#define NS_PACKML_URI "http://opcfoundation.org/UA/PackML/"
#define PACKML_BASEOBJECT_NAME "PackMLBaseObject"

static const UA_NodeId PACKML_OBJECTSFOLDER =
    (UA_NodeId){NS_PACKML, UA_NODEIDTYPE_NUMERIC, UA_NS4ID_PACKMLBASEOBJECTTYPE};

/* State display names and ids based on EXST value (= PACKML StateNumber -1):
 * https://reference.opcfoundation.org/v104/PackML/v100/docs/B.1.1/
 * Cleared and Run are macro states (value 18, 19)
 */
static const char *PACKML_STATE_NAME[] = {
    "Clearing",   "Stopped",   "Starting",   "Idle",         "Suspended",
    "Execute",    "Stopping",  "Aborting",   "Aborted",      "Holding",
    "Held",       "Unholding", "Suspending", "Unsuspending", "Resetting",
    "Completing", "Complete",  "Cleared",    "Run"};
static const UA_UInt32 PACKML_STATE_ID[] = {
    UA_NS4ID_CLEARING,   UA_NS4ID_STOPPED,
    UA_NS4ID_STARTING,   UA_NS4ID_IDLE,
    UA_NS4ID_SUSPENDED,  UA_NS4ID_EXECUTE,
    UA_NS4ID_STOPPING,   UA_NS4ID_ABORTING,
    UA_NS4ID_ABORTED,    UA_NS4ID_HOLDING,
    UA_NS4ID_HELD,       UA_NS4ID_UNHOLDING,
    UA_NS4ID_SUSPENDING, UA_NS4ID_UNSUSPENDING,
    UA_NS4ID_RESETTING,  UA_NS4ID_COMPLETING,
    UA_NS4ID_COMPLETE,   UA_NS4ID_PACKMLBASESTATEMACHINETYPE_CLEARED,
    UA_NS4ID_RUNNING};

static const UA_UInt32 PACKML_ESMETHOD_ID[] = {
    UA_NS4ID_START,   UA_NS4ID_STOP,     UA_NS4ID_RESET, UA_NS4ID_RESET_COMPLETE,
    UA_NS4ID_ABORT,   UA_NS4ID_CLEAR,    UA_NS4ID_HOLD,  UA_NS4ID_UNHOLD,
    UA_NS4ID_SUSPEND, UA_NS4ID_UNSUSPEND};
#define PACKML_ESMETHOD_ID_SIZE 10

/* Loads the PackML Information model from xml nodeset file */
void
createProfile_PACKML(UA_Server *server);

/* Configures a PackMLBaseObject after its instanciation */
void
configurePackMLBaseObject(UA_Server *server, const UA_NodeId id, C3_Profile profile);

/* Clears all custom data and frees its memory before shutdown */
void
clearProfilePackML(UA_Server *server);

/*
 ============================================================================
 Name        : profile_interfaces.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Functions to add OPC UA Interfaces
 ============================================================================
 */
#ifndef PROFILE_INTERFACES_H
#define PROFILE_INTERFACES_H

#include <profile_utilities.h>

/******************************************************************************
 * Type creation //TODO create information model as xml file --> export via open62541?
 * TypeTree with BrowseNames:
 * Objects
 * - (O) Server
 *      - (O) ServerCapabilities
 *          - (Folder) ModellingRules
 *              - (Folder) SI
 *                  - (O) 0
 *                  - (O) 1
 *                  - ...
 *              - ...
 * BaseInterfaceType
 * - (OT) DummyCCType
 * 		- (V) C3_Profile // from CCProfileType
 * 		- (V) STATUS // from CCStatusType
 * 		- (OT) CMDType
 * 			- (V) CMD
 * 			- (V) CMDLAST
 * 		- (OT) OperationsType
 * 			- (Folder) OPERATIONS
 * 				- (M) Operation1
 * 				- ...
 * 		- (OT) ServicesType //TODO add structure
 *      - ...
 * BaseDataVariableType
 * 	- (VT) CCProfileType
 * 		- (V) SI
 * 		- (V) OC
 * 		- (V) EM
 * 		- (V) ES
 * 		- (V) OM
 * 	- (VT) CCStatusType
 * 		- (V) EXST
 * 		- (V) EXMODE
 * 		- (V) OPMODE
 * 		- (V) WORKST
 * 		- ...
 ******************************************************************************/

/*
 * Create Interface Type Hierarchy
 * Objects ... ModellingRules
 * BaseInterfaceType
 * 	- CCType
 *  	- CMDType
 *  	- OperationsType
 *  	- ServicesType
 * BaseDataVariableType
 * 	- CCStatusType
 */
void
createControlComponentInterface(UA_Server *server);
#endif /* PROFILE_INTERFACES_H */
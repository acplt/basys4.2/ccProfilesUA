/*
 ============================================================================
 Name        : cc_instanciation.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Functions to add OPC UA nodes for control components
 ============================================================================
 */
#ifndef CC_INSTANCIATION_H
#define CC_INSTANCIATION_H

#include <profile_utilities.h>

#include <C3_ControlComponent.h>

/* Create all types, interfaces and parent folder for examples */
void
createControlComponentEnvironment(UA_Server *server,
                                  const char *profilesNamespaceUri,
                                  const char *typeNamespaceUri);

/* Create a single control component type from a instanciated C3_CC */
void
createControlComponentType(UA_Server *server, C3_CC *cc, UA_NodeId id);

/* Create a single control component from a instanciated C3_CC */
void
createControlComponent(UA_Server *server, C3_CC *cc, UA_NodeId typeId, UA_NodeId id);

/* Free ressources of profile instances and types before server shutdown */
void
clearControlComponentEnvironment(UA_Server *server);

#endif /* CC_INSTANCIATION_H */
/*
 ============================================================================
 Name        : cc_exampleOpMode.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Control component operation mode implementation for dummy examples.
 ============================================================================
 */
#ifndef CC_EXAMPLEOPMODE_H
#define CC_EXAMPLEOPMODE_H

#include <C3_ControlComponent.h>

#include <cc_utilities.h>

// Adds the dummy operation mode to a control component
bool
cc_exampleOpMode_add(C3_CC *cc, char *name, const char **workStates, size_t workStatesSize,
                     int stateDuration);

#endif /* CC_EXAMPLEOPMODE_H */

/*
 ============================================================================
 Name        : cc_connector.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Functions to simulate a dummy control component. Handles the
               orders from the command input or method callbacks.
 ============================================================================
 */
#ifndef CC_CONNECTOR_H
#define CC_CONNECTOR_H

#include <cc_utilities.h>

/* Executes a command (consisting of sender, order and paramter) on a control component
 * identifed by its node id.
 */
UA_StatusCode
cc_executeCommand(UA_Server *server, const UA_NodeId *ccId, const UA_String *sender,
                  const UA_String *order, const UA_String *parameter);

/* Callbacks to connect the command input or operations to the control component command
 * input.
 */
void
cc_commandInputCallback(UA_Server *server, const UA_NodeId *sessionId,
                        void *sessionContext, const UA_NodeId *nodeId, void *nodeContext,
                        const UA_NumericRange *range, const UA_DataValue *data);

UA_StatusCode
cc_operationCallback(UA_Server *server, const UA_NodeId *sessionId, void *sessionHandle,
                     const UA_NodeId *methodId, void *methodContext,
                     const UA_NodeId *objectId, void *objectContext, size_t inputSize,
                     const UA_Variant *input, size_t outputSize, UA_Variant *output);

/* Callbacks to read status values from the control component */
#define DEFINE_CCVARIABLECALLBACK_HEADER(variableName)                                   \
    void cc_VariableCallback_##variableName(                                             \
        UA_Server *server, const UA_NodeId *sessionId, void *sessionContext,             \
        const UA_NodeId *nodeid, void *nodeContext, const UA_NumericRange *range,        \
        const UA_DataValue *data);

DEFINE_CCVARIABLECALLBACK_HEADER(CMDLAST)
DEFINE_CCVARIABLECALLBACK_HEADER(OCCST)
DEFINE_CCVARIABLECALLBACK_HEADER(OCCUPIER)
DEFINE_CCVARIABLECALLBACK_HEADER(OCCLAST)
DEFINE_CCVARIABLECALLBACK_HEADER(EXMODE)
DEFINE_CCVARIABLECALLBACK_HEADER(EXST)
DEFINE_CCVARIABLECALLBACK_HEADER(OPMODE)
DEFINE_CCVARIABLECALLBACK_HEADER(WORKST)
DEFINE_CCVARIABLECALLBACK_HEADER(ER)
DEFINE_CCVARIABLECALLBACK_HEADER(ERLAST)

DEFINE_CCVARIABLECALLBACK_HEADER(PROFILE)
DEFINE_CCVARIABLECALLBACK_HEADER(SI)
DEFINE_CCVARIABLECALLBACK_HEADER(OC)
DEFINE_CCVARIABLECALLBACK_HEADER(EM)
DEFINE_CCVARIABLECALLBACK_HEADER(ES)
DEFINE_CCVARIABLECALLBACK_HEADER(OM)

/* Utility functions to run the control components cyclic execution as an UA_Server job
 * (repeatedCallback)*/
void
cc_executionLoop(UA_Server *server, void *data);

/* Create and delete context for the execution loop (array of cc pointers)*/
void *
cc_executionLoopContext_new(UA_Server *server);
void
cc_executionLoopContext_delete(void *context);

#endif /* CC_CONNECTOR_H */
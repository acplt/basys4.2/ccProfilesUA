/*
 ============================================================================
 Name        : cc_utilites.h
 Author      : Julian Grothoff
 Version     : 0.1
 Copyright   : PLT
 Description : Utility functions for the dummy control component and the
               corresponding state machines.
 ============================================================================
 */
#ifndef CC_UTILITIES_H
#define CC_UTILITIES_H

#include <profile_utilities.h>

#include <C3_ControlComponent.h>

// Default value, if an order leads to no change of a state, because target state is
// already active
static UA_StatusCode CC_TARGETSTATEACTIVE = UA_STATUSCODE_GOODNODATA;
// Default value, if an order leads to no change of a state, because it is not supported
// in the current state
static UA_StatusCode CC_ORDERIGNORED = UA_STATUSCODE_GOODDATAIGNORED;

// Translates an C3_Result to an UA_StatusCode
UA_StatusCode
c3ResultToUAStatusCode(C3_Result result);

// Copies an UA_String to a nullterminated char*
char *
uaStringToCharArray(const UA_String *uaStr);

#endif /* CC_UTILITIES_H */
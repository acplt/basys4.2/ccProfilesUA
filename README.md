# BaSys 4.2 OPC UA Control Component Profile Server and Compliance Test Tool

This project implements example basys control components with different profiles in an open62541 based OPC UA server.
The project uses the [control component C implementation (C3 library)](https://git.rwth-aachen.de/acplt/basys4.2/c3/) as backend.

The goal is to generate a dummy for each profile or facet combination as a reference implementation.
Moreover, this serves as a test fixture for a [profile compliance tool](#profile-compliance-testing), which is developed as test within this project.

## Download

Latest builds on master:

* [Linux 64bit](https://git.rwth-aachen.de/acplt/basys4.2/ccProfilesUA/-/jobs/artifacts/master/download?job=build%3Alinux64)
* [Linux 64bit without PackML](https://git.rwth-aachen.de/acplt/basys4.2/ccProfilesUA/-/jobs/artifacts/master/download?job=build%3Alinux64%3AnoPackML)
* [Windows 64bit](https://git.rwth-aachen.de/acplt/basys4.2/ccProfilesUA/-/jobs/artifacts/master/download?job=build%3Awin64)

 [![pipeline status](https://git.rwth-aachen.de/acplt/basys4.2/ccProfilesUA/badges/master/pipeline.svg)](https://git.rwth-aachen.de/acplt/basys4.2/ccProfilesUA/-/commits/master)

## Usage

To start an OPC UA server with some examples on localhost and default port 4840 just execute **profile_server**.

Available command line arguments are:

* `[--profile <name>|<csv>]`
* `[--port <port>]`
* `[--optional]`
* `[--help]`

Example: `profile_server --profile BASYS --port 16664 --optional`

---

## Profiles

BaSys control components may be implemented according to profiles.
They are officialy documented in the [basyx wiki](https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponentProfiles).
Each profile is defined by values of different facet groups listed in the table below.
The [service interface facet group](https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponentProfiles#SI_Facets) is special, because it is orthogonal to the other profiles. Hence, every other facet looks a bit different, when a different SI facet is realised.
A rough implementation status of the examples and tests are marked in the table with -, 0 and +.

| Facet Group | Name              | Values                                  | CMD | OPERATIONS | PACKML   |
| ----------- | ----------------- | --------------------------------------- | --- | ---------- | -------- |
| SI          | Service Interface | CMD, OPERATIONS, SERVICES, VARIABLES    | +   | +          | +        |
| OC          | Occupation        | OCCUPIED, PRIO, LOCAL                   | +   | +          | -        |
| EM          | Execution Modes   | AUTO, SEMIAUTO, MANUAL, SIMULATE        | +   | +          | -        |
| ES          | Execution States  | BASYS, PACKML, ISA88, MTP, OPCUA, NE160 | 0   | 0          | 0        |
| OM          | Operation Modes   | MULTI, PARALLEL, CONTI                  | +   | 0          | -        |

For each facet group and for available profiles a correspondig enumeration, respectively a bitmask, is implemented in `c3/inc/C3_Profiles.h` header file.
Some features may be optional, but if used they should be realized in the same way. Therefore the label `Optional` is used in tests.
Another keyword is `Behaviour` to define tests, that activate operations and check their response and state changes.
<!--TODO change optional to advanced instead?-->
<!--TODO `behaviour`: maybe add static/syntax keyword for normal tests to filter with a positiv filter-->

### Common Facet Group Values

All facet group values can be determined with an positiv integer or zero, respectively enums are implemented.
If it is usefull to combine different facets of one facet group, their facet group values are represented as bitmasks.
Otherwise normal enums are used.

| Bit    | Value | Shortname | Example SI | Description                                                             |
| ------ | ----- | --------- | ---------- | ----------------------------------------------------------------------- |
| 1      | 0     | Unknown   | UNKNOWN    | No statement or not compliant to facets                                 |
| 1      | 1     | None      | NONE       | Facet gropu is not used or implemented                                  |
| 2 - n  | > 1   | Any       | ANY        | One ore more facets. All 2-n bits are linked by or operator (Value > 1) |
| 2      | 2     | Default   | CMD        | The default facet is used (specific to facet group)                     |
| 3      | 4     | First     | OPERATIONS | Alternative facets (specific to facet group)                            |
| 4 .. n | 8 ..n | Second    | SERVICES   | Alternative facets (specific to facet group)                            |

### Service Interface Facets

Different examples with different service interface (SI) facets are available.
The following facets are available at the moment:

* **CMD**: A string variable called CMD (CommandInput) is used to activate service operations.
* **OPERATIONS**: A list of OPC UA methods in a folder represent the operations of the offered services.
* **PACKML**:

#### Possible Upcomming SI Facets

* **SERVICES**: Each service represents an own object, with OPC UA methods as operations and additional state variables.
* **VARIABLES**: A list of variables to start the different operations of the offered services.
* **MTP**: Modulte Type Package conform service interface. Possibly a specific version of the VARIABLES profile.

---

## Profile Compliance Testing

This project includes the google test framework (<https://github.com/google/googletest>) for unit tests. These tests are designed to be fed with an external opcua server and nodeId of a control component.
Hence, they can be used to determine whether a control component has a compliant interface.
Moreover [filters](#compliance-test-filtering) can be applied to check for specific facets.

## Compliance Test Usage

To test the compliance you need to execute the **test_compliance** execuatable, which can be found in the bin folder after build process or in the [prebuild executables](#download).
The other test executables are not needed and are for unit tests and self checks of the profile_server example.

| Parameter                                         | Description                           | Used for                                                                                                          | example                                   |
| ------------------------------------------------- | ------------------------------------- | ----------------------------------------------------------------------------------------------------------------- | ----------------------------------------- |
| --selfcheck                                       | Use of internal server implemantation | Selfcheck of test_compliance with code from profile_server                                                        | --selfcheck                               |
| --server `opc.tcp://<host>:<port>`                | URL of OPC UA Server                  | To specify the internal or external server endpoint                                                               | --server opc.tcp://localhost:4840         |
| --nodes `ns=<index>;[i\|s\|g\|b]=<id>[, ...]`     | List of node ids[1]                   | Specification of control components (or types respectively interfaces) endpoint                                   | --nodes "ns=1;i=5,ns=2;s='StringNodeId'"  |
| --namespace `<URI>`                               | Namespace URI                         | Search for the namespace index of the profiles information model (to tranlate browse paths)                       | --namespace "basys40.de/cc/profiles"      |
| --profile `<profilename|SI,OC,EM,ES,OM,optional>` | Profile value[2]                      | Specification of the profile to test the nodes against, via profile name or numerical comma separated definition. | --profile BASYS                           |
|                                                   |                                       |                                                                                                                   |                                           |
| --timeout `<time>`                                | Timeout for behaviour tests (s)       | The behaviour test waits this time before checking results for each order                                         | --timeout 5                               |
| --user `<username>` `<password>`                  | Client authentification[3]            | The test client logs in with the first user name password given.                                                  | --user tester testersPassword             |
|                                                   |                                       |                                                                                                                   |                                           |
| --gtest_...                                       | Google test framework parameters      | Filtering profiles or single tests                                                                                | --gtest_list_tests --gtest_filter=\*ES.\* |

[1] *Note on node ids*: Currently only **numeric and string** ids are supported.
Single quotation marks are needed to ignore the semicolon in the node id string.
String node ids may enclose the string identifier with quotation marks, e.g.: --nodes 'ns=1;s="/TechUnits/ESE/ControlComponent1"' \
[2] *Note on common facet values*: The common values for each facet (UNKNOWN, NONE, ANY) are treated in the following way:

* UNKNOWN (0, default value): All tests - Tests for all facet values are executed. This may be used to determine which profile a control component implements or whether it implements multiple facets. You can [filter](#compliance-test-filtering) tests to specific facets or tests via gtest_filter.
* NONE (1): ANY tests - The control component implements no compliant facets. Only the tests (testname starting with ``ANY``) common to all facets in the facet group are executed.
* Other specific values (2-n): ANY tests & facet specific - The control component implements one or more specific facet. The tests common to all facets in the facet group (testname starting with ``ANY``) and the specific ones are executed.
* In future versions the test suite may try to evaluate which profile is used or try to read the ``SIPROFILE`` OPC UA variable of the control component and assume its value.

[3] *Note on user authentification*: A second authentification for the OC.*_Behaviour tests with SI OPERATIONS facet is necessary to test against wrong authentification. If no second user is given the anonymous user is used. If no user is given at all the default values from open62541 (user1:password, user2:password1) are used.

### Compliance Test Usage Examples

Some simple examples usage scenarios:

* To list all tests: `./test_compliance --nodes i=0 --gtest_list_tests`
* To check your [filter](#compliance-test-filtering) use: `./test_compliance --nodes i=0 --gtest_list_tests -gtest_filter=FooBlah`
* To run a self check with all tests: `./test_compliance --selfcheck --nodes 'ns=1;i=1'`
* To test one control component against BASYS profile: `./test_compliance --nodes 'ns=1;i=42' --server=opc.tcp://192.168.0.12:4840 --profile BASYS`

<!--TODO define config file with cmd options and list of nodes and --config flag -->
<!--TODO define example config for --selfcheck or load nodeId directly. -->

### GUI Tools for Compliance Testing

The google test framework has different [associated projects for GUIs or IDE integration](https://github.com/google/googletest#related-open-source-projects) of gtests. Feel free to use one with the test_compliance executable.

## Compliance Test Filtering

In general the underlying gtest framework exposes different hierarchical levels to identify tests.
Here three of these levels are used: the instanciation name, the test suite or fixture name and the test name itself.
They are combined in the following way: `nodeId/facetgroup.facet_keyword`
These levels can be used to filter the corresponding test needed for your compliance check:

| Level                | Used for    | Values                                                                                                         |
| -------------------- | ----------- | -------------------------------------------------------------------------------------------------------------- |
| Instanciation name   | Node id     | Node Id in standard syntax: `ns=<index>;[i|s|g|b]=<id>`. Set via [cmd line arguments](#compliance-test-usage). |
| Test suite / fixture | Facet group | [SI, OC, EM, ES, OM](#profiles)                                                                                |
| Test name            | Facet\*     | ANY or profile specific values.                                                                                |
|                      | Keyword     | _Optional,_Behaviour                                                                                          |

\*Some tests are common to all facets in a facet group. These tests start with the test name **ANY**.  

Examples of a full test names are:

* `ns=1;i=10001/ES.BASYS`
* `ns=2;s=MyControlComponent/SI.ANY`
* `ns=3;i=42/OC.OCCUPIED_Optional`

### Compliance Test Filter Examples

Execute for example: `./test_compliance --server=opc.tcp://192.168.0.100:16664 --nodes i=0 --gtest_filter=` with the following filters:

| gtest_filter=             | Description of selected tests                         |
| ------------------------- | ----------------------------------------------------- |
| `*/SI.ANY*`               | Service interface with all common tests.              |
| `*/SI.CMD*`               | Service interface with value CMD only                 |
| `*/EM.AUTO*:*/EM.MANUAL*` | Execution modes AUTO and MANUAL within EM facet group |
| `-*_Optional`             | Exclude checks for optional values.                   |

Filters may be concenatet with a **colon (:)**.
Negative filters are also possible with **minus (-)** to exclude tests.
See google test framework [documentation](https://github.com/google/googletest/blob/master/googletest/docs/advanced.md#running-a-subset-of-the-tests) for details.
